import userStore from "@/store/user_store/userStore";
import axios from "axios";
import * as wss from "@/api/wssocket";
import store from "@/store";
import * as enums from "@/api/enums";
import getToken from "@/router/getToken";

const init = ()=>{
    var token = localStorage.getItem("token");
    wss.connect('ws://localhost:8879/im',token);
    store.dispatch("load");
    wss.onConnect(() => {
        // 加载离线消息
        //  pullPrivateOfflineMessage(store.state.chatStore.privateMsgMaxId);
    });
    wss.onMessage((cmd, msgInfo) => {
        if (cmd == 2) {
            // 关闭ws
            wss.close(3000)
            // 异地登录，强制下线
            alert("您已在其他地方登陆，将被强制下线", "强制下线通知", {
                confirmButtonText: '确定',
                callback: action => {
                    location.href = "/";
                }
            });
        } else if (cmd == 3) {
            // 插入私聊消息
            handlePrivateMessage(msgInfo);
        }
    });
}

const handlePrivateMessage = (msg)=>{
    // 消息加载标志
    if (msg.type == enums.MESSAGE_TYPE.LOADDING) {
        store.commit("loadingPrivateMsg", JSON.parse(msg.content))
        return;
    }
    // 消息已读处理，清空已读数量
    if (msg.type == enums.MESSAGE_TYPE.READED) {
        store.commit("resetUnreadCount", {
            type: 'PRIVATE',
            targetId: msg.recvId
        })
        return;
    }
    // 消息回执处理,改消息状态为已读
    if (msg.type == enums.MESSAGE_TYPE.RECEIPT) {
        store.commit("readedMessage", { friendId: msg.sendId })
        return;
    }
    // 标记这条消息是不是自己发的
    msg.selfSend = msg.sendId == userStore.state.userId;
    // 好友id
    let friendId = msg.selfSend ? msg.recvId : msg.sendId;
    loadFriendInfo(friendId).then((friend) => {
        insertPrivateMessage(friend, msg);
    });
}

const loadFriendInfo = (id)=>{
    return new Promise((resolve, reject) => {
        let friend = store.state.friendStore.friends.find((f) => f.id == id);
        if (friend) {
            resolve(friend);
        } else {
            var token = localStorage.getItem("token");
            axios.get(`/fr/friend/find/${id}`,{
                headers: {
                    Authorization:'Bearer '+token
                }
            }).then((friend) => {
                if (friend.data.code == 200){
                    store.commit("addFriend", friend);
                    resolve(friend)
                }
            })
        }
    });
}

//   //发送消息
const  insertPrivateMessage = (friend, msg) => {
    let chatInfo = {
      type: 'PRIVATE',
      targetId: friend.id,
      showName: friend.nickName,
      headImage: friend.headImage
    };
    // 打开会话
    store.commit("openChat", chatInfo);
    // 插入消息
    store.commit("insertMessage", msg);
}

const  pullPrivateOfflineMessage = (minId) =>{
    var token = localStorage.getItem("token");
    if (token){
        axios.get("/msg/message/private/pullOfflineMessage?minId=" + minId,{
            headers: {
                Authorization:'Bearer '+token
            }
        });
    }
}

//   /**
//    * 断开连接
//    */
//   onExit() {
//     this.$wsApi.close(3000);
//     sessionStorage.removeItem("token");
//     location.href = "/";
//   },

export default {
    init,
    handlePrivateMessage,
    insertPrivateMessage,
    pullPrivateOfflineMessage
}