import localforage from "localforage";

var localForage = localforage.createInstance({
    name: "chatsDB",
    driver: localforage.INDEXEDDB
});

export default localForage;