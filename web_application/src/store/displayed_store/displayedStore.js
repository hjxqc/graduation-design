import {createStore} from "vuex";
import axios from "axios";
import qs from 'qs'

export default createStore( {
    state:{
        allPost:[],
        current:1,
        size:10,
        post_is_null:false
    }, mutations:{
        /**
         * 获取所有帖子的数据并提示错误信息
         * @param state
         * @param erroTip
         */
        getAllPost(state,allPost){
            console.log(allPost);
            state.allPost.push.apply(state.allPost,allPost);
        },
        setCurrent(state){
            if (!state.post_is_null){
                state.current = state.current + 1;
                console.log("翻页");
            }
        }
    },actions:{
        getApprovedPost(context, errorTip) {
            const res = axios.get("/post_img/managerpost/all/approved/post", {
                params: {
                    "current": context.state.current,
                    "size": context.state.size
                }
            }).then((res) => {
                if (res.data.code == 200) {
                    if(res.data.data.length <= 0) {
                        context.state.post_is_null = true;
                        errorTip("已经到底啦!");
                    }
                    context.commit("getAllPost", res.data.data);
                } else {
                    errorTip("获取数据异常，服务器繁忙");
                }
            }).catch((erro) => {
                console.log(erro)
                errorTip("服务器繁忙");
            });
        },
        getApprovedPostByIds(context, userids ,errorTip) {
            const res = axios.get("/post_img/managerpost/all/approved/post/by/userids", {
                params:{
                    userids: userids,
                    "current": context.state.current,
                    "size": context.state.size
                },
                paramsSerializer: function (params) {
                    return qs.stringify(params, { arrayFormat: "repeat" });
                }
            }).then((res) => {
                console.log(res);
                if (res.data.code == 200) {
                    if(res.data.data.length <= 0) {
                        context.state.post_is_null = true;
                        alert("没有动态更新");
                    }
                    context.commit("getAllPost", res.data.data);
                } else {
                    alter("没有动态更新");
                }
            }).catch((erro) => {
                console.log(erro)
                alert("没有动态更新");
            });
        },
        clear(context){
            context.state.allPost = [];
            context.state.current = 1;
            context.state.post_is_null = false;
        }
    },modules:{

    }
})