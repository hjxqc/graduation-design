import axios from "axios";
import {TERMINAL_TYPE} from "@/api/enums";
import getToken from "@/router/getToken";

export default {
    state:{
        friends: [],
        activeFriend: null,
        timer:null
    },
    mutations:{
        setFriend(state,friends){
            state.friends = friends;
        },
        activeFriendById(state,id){
            for (let i = 0; i < state.friends.length; i++) {
                if (state.friends[i].id === id) {
                    state.activeFriend = state.friends[i];
                    return;
                }
            }
        },
        activeFriend(state,idx){
            state.activeFriend = state.friends[idx];
        },
        activeFriendByFr(state,friend){
            state.activeFriend = friend;
        },
        reomveFriend(state,idx) {
            if (state.friends[idx] == state.activeFriend)
                state.activeFriend = null;
            state.friends.splice(idx,1);
        },
        reomveFriendByFrId(state,id) {
            for (let i = 0; i < state.friends.length; i++) {
                if (state.friends[i].id == id){
                    if (state.friends[i] == state.activeFriend)
                        state.activeFriend = null;
                    state.friends.splice(i,1);
                }
            }
        },
        addFriend(state,friend){
            state.friends.push(friend);
        },
        refreshOnlineStatus(state){
            getToken.initToken();
            let userIds = [];
            //如果没有朋友则直接退出
            if (state.friends.length == 0)
                return;
            state.friends.forEach((f) => {userIds.push(f.id)});
            var token = localStorage.getItem("token");
            axios.get('/usr/user/terminal/online',{
                params: {
                    userIds: userIds.join(",")
                },
                headers: {
                    Authorization:'Bearer '+token
                }
            }).then((onlineTerminal) => {
                this.commit('setOnlineStatus',onlineTerminal.data.data);
            });

            //30s后重新拉取
            clearTimeout(state.timer);
            state.timer = setTimeout(()=>{
                this.commit("refreshOnlineStatus")
            },30000);
        },
        setOnlineStatus(state,onlineTerminals){
            state.friends.forEach((f) => {
                let userTerminal = onlineTerminals.find((o)=> f.id==o.userId);
                if(userTerminal){
                    f.online = true;
                    f.onlineTerminals = userTerminal.terminals;
                    f.onlineWeb = userTerminal.terminals.indexOf(TERMINAL_TYPE.WEB)>=0
                    f.onlineApp = userTerminal.terminals.indexOf(TERMINAL_TYPE.APP)>=0
                }else{
                    f.online = false;
                    f.onlineTerminals = [];
                    f.onlineWeb = false;
                    f.onlineApp = false;
                }
            });

            state.friends.sort((f1,f2)=>{
                if (f1.online&&!f2.online)
                    return -1;
                if (f2.online&&!f1.online)
                    return 1;
                return 0;
            });
        },
        clear(state){
            clearTimeout(state);
            state.friend = [];
            state.timer = null;
            state.activeFriend = [];
        }
    },
    actions:{
        loadFriend(context){
            var token = localStorage.getItem("token");
            return new Promise((resolve,reject) => {
                axios.get('/fr/friend/list',{
                    headers: {
                        Authorization:'Bearer '+token
                    }
                })
                    .then((friends) => {
                        if (friends.data.code == 200){
                            context.commit("setFriend",friends.data.data);
                            context.commit("refreshOnlineStatus");
                            resolve();
                        }
                    }).catch((res)=>{
                        reject();
                    });
            });
        }
    }
}