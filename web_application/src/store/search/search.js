import { createStore } from 'vuex';
import axios from "axios";
export default createStore({
    state: {
        all_post:[],
        all_user:[],
        all_post_is_null:false,
        all_user_is_null:false,
        user_current_page :1,
        post_current_page :1,
        page_size :10
    },
    getters: {
    },
    mutations: {
        set_post(state,post){
            state.all_post.push.apply(state.all_post,post);
        },
        set_user(state,user){
            state.all_user.push.apply(state.all_user,user);
        },
        //翻用户的页
        page_user(state){
            //如果当前页为空则不进行翻页
            if(!state.all_user_is_null)
                state.user_current_page = state.user_current_page + 1;
        },
        //翻帖子的页
        page_post(state){
            //如果当前页为空则不进行翻页
            if (!state.all_post_is_null)
                state.post_current_page = state.post_current_page + 1;
        }
    },
    actions: {
        clear(context){
            context.state.all_post = [];
            context.state.all_user = [];
            context.state.all_post_is_null = false;
            context.state.all_user_is_null = false;
            context.state.user_current_page = 1;
            context.state.post_current_page = 1;
        },
        getApprovedPostByPostName(context, postName) {
            axios.get("/post_img/managerpost/all/approved/post/by/postname", {
                params:{
                    "postname": postName,
                    "current": context.state.post_current_page,
                    "size": context.state.page_size
                }
            }).then((res) => {
                console.log(res);
                if (res.data.code == 200) {
                    if (res.data.data.length <= 0){
                        context.state.all_post_is_null = true;
                    }else{
                        //表示这一页有值了
                        //表示可以进行翻页了
                        context.state.all_post_is_null = false;
                        context.commit('set_post',res.data.data);
                    }
                } else {
                    alert("没有相关帖子");
                }
            }).catch((erro) => {
                console.log(erro)
                alert("没有相关帖子");
            });
        },
        getAllUserByName(context,username){
            var token = localStorage.getItem("token");
            axios.get("/usr/user/findByName", {
                params:{
                    "name": username,
                    "current": context.state.user_current_page,
                    "size": context.state.page_size
                },
                headers: {
                    Authorization:'Bearer '+token
                }
            }).then((res) => {
                console.log(res);
                if (res.data.code == 200) {
                    if (res.data.data.length <= 0){
                        context.state.all_user_is_null = true;
                    }else{
                        //表示这一页有值了
                        //表示可以进行翻页了
                        context.state.all_user_is_null = false;
                        context.commit('set_user',res.data.data);
                    }
                } else {
                    alert("没有相关帖子");
                }
            }).catch((erro) => {
                console.log(erro)
                alert("没有相关帖子");
            });
        },
        change_friend_status(context,id){
            //改变状态
            context.state.all_user.forEach((user)=>{if (user.userVO.id == id) user.isFriend = !user.isFriend})
        }
    }
})
