import {createStore} from "vuex";
import axios from "axios";

export default createStore( {
    state:{
        userId: "",
        name: "",
        mobile:"",
        headImageThumb:"",
        mail:"",
        is_login: false
    }, mutations:{
        init_info(state,user_info){
            state.userId = user_info.userid;
            state.name = user_info.name;
            state.mobile = user_info.mobile;
            state.mail = user_info.mail;
            state.headImageThumb = user_info.headImage;
            state.is_login = true;
        }
    },actions:{
        get_user_info(context,token){
            axios.get("/login/permission/user/info",{
                headers: {
                    Authorization:'Bearer '+token
                }
            }).then(res=>{
               if(res.data.code == 200){
                   context.commit("init_info", {
                       userid:res.data.data.userId,
                       name:res.data.data.name,
                       mobile:res.data.data.mobile,
                       mail:res.data.data.mail,
                       headImage:res.data.data.headImage
                   });
               }
            }).catch(error=>{
                console.log(error);
            })
        }
    },modules:{

    }
})