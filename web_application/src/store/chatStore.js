import {
    MESSAGE_TYPE,
    MESSAGE_STATUS
} from "../api/enums.js"
import userStore from "@/store/user_store/userStore";
import webdb from "@/api/webdb";

export default {
    state: {
        activeChat: null,
        privateMsgMaxId: 0,
        loadingPrivateMsg: false,
        chats: []
    },
    getters: {
        /**
         * 找到与某个用户的会话的位置
         * @param state
         * @returns {(function(*): (string|undefined))|*}
         */
        findChatIdx: (state) => (chat) => {
            for (let idx in state.chats) {
                if (state.chats[idx].type == chat.type &&
                    state.chats[idx].targetId === chat.targetId) {
                    chat = state.chats[idx];
                    return idx
                }
            }
        },
        /**
         * 根据消息，找到这个消息是在与哪个用户的会话中
         * @param state
         * @returns {function(*): null}
         */
        findChat: (state) => (msgInfo) => {
            // 获取对方id或群id
            let type = msgInfo.groupId ? 'GROUP' : 'PRIVATE';
            let targetId = msgInfo.groupId ? msgInfo.groupId : msgInfo.selfSend ? msgInfo.recvId : msgInfo.sendId;
            let chat = null;
            for (let idx in state.chats) {
                if (state.chats[idx].type == type &&
                    state.chats[idx].targetId === targetId) {
                    chat = state.chats[idx];
                    break;
                }
            }
            return chat;
        },
        /**
         * 找到对应的消息
         * @param state
         * @returns {(function(*, *): (null|*|undefined))|*}
         */
        findMessage: (state) => (chat, msgInfo) => {
            if (!chat) {
                return null;
            }
            for (let idx in chat.messages) {
                // 通过id判断
                if (msgInfo.id && chat.messages[idx].id == msgInfo.id) {
                    return chat.messages[idx];
                }
                // 正在发送中的消息可能没有id,通过发送时间判断
                if (msgInfo.selfSend && chat.messages[idx].selfSend &&
                    chat.messages[idx].sendTime == msgInfo.sendTime) {
                    return chat.messages[idx];
                }
            }
        }
    },
    mutations: {
        //初始化消息
        initChats(state, chatsData) {
            state.chats = chatsData.chats || [];
            state.privateMsgMaxId = chatsData.privateMsgMaxId || 0;
            state.groupMsgMaxId = chatsData.groupMsgMaxId || 0;
            // 防止图片一直处在加载中状态
            state.chats.forEach((chat) => {
                chat.messages.forEach((msg) => {
                    if (msg.loadStatus == "loading") {
                        msg.loadStatus = "fail"
                    }
                })
            })
        },
        openChat(state,chatInfo){
            let chat = null;
            //将指定的消息放置到顶部
            for (let idx in state.chats) {
                if (state.chats[idx].type == chatInfo.type &&
                    state.chats[idx].targetId === chatInfo.targetId) {
                    chat = state.chats[idx];
                    // 放置头部
                    this.commit("moveTop", idx)
                    break;
                }
            }
            // 创建会话
            //创建每个用户的会话数据
            if (chat == null) {
                chat = {
                    targetId: chatInfo.targetId,
                    type: chatInfo.type,
                    showName: chatInfo.showName,
                    headImage: chatInfo.headImage,
                    lastContent: "",
                    lastSendTime: new Date().getTime(),
                    unreadCount: 0,
                    messages: [],
                    atMe: false,
                    atAll: false
                };
                state.chats.unshift(chat);
            }
        },
        //设置真在活跃的消息
        activeChat(state, idx) {
            state.activeChat = state.chats[idx];
        },
        //重新设置对应好友会话的未读消息的数量
        resetUnreadCount(state, chatInfo) {
            for (let idx in state.chats) {
                if (state.chats[idx].type == chatInfo.type &&
                    state.chats[idx].targetId == chatInfo.targetId) {
                    state.chats[idx].unreadCount = 0;
                    state.chats[idx].atMe = false;
                    state.chats[idx].atAll = false;
                }
            }
            this.commit("saveToStorage");
        },
        /**
         * 将指定的消息设置为已读的状态
         * @param state
         * @param pos 消息的好友id，最大的消息id
         */
        readedMessage(state, pos) {
            console.log(pos);
            for (let idx in state.chats) {
                if (state.chats[idx].type == 'PRIVATE' &&
                    state.chats[idx].targetId == pos.friendId) {
                    state.chats[idx].messages.forEach((m) => {
                            if ( m.id <= pos.maxId && m.status != MESSAGE_STATUS.RECALL) {
                                m.status = MESSAGE_STATUS.READED
                            }
                        // if (m.selfSend && m.status != MESSAGE_STATUS.RECALL) {
                        //     // pos.maxId为空表示整个会话已读
                        //     if (!pos.maxId || m.id <= pos.maxId) {
                        //         m.status = MESSAGE_STATUS.READED
                        //     }
                        // }
                    })
                }
            }
            this.commit("saveToStorage");
        },
        /**
         * 删除对应下标的和某个用户会话的所有消息
         * @param state
         * @param idx
         */
        removeChat(state, idx) {
            if (state.chats[idx] == state.activeChat) {
                state.activeChat = null;
            }
            state.chats.splice(idx, 1);
            this.commit("saveToStorage");
        },
        /**
         * 将和某个用户对话的所有消息移动到消息的顶部
         * @param state
         * @param idx
         */
        moveTop(state, idx) {
            // 加载中不移动，很耗性能
            if (state.loadingPrivateMsg || state.loadingGroupMsg) {
                return;
            }
            if (idx > 0) {
                let chat = state.chats[idx];
                state.chats.splice(idx, 1);
                state.chats.unshift(chat);
                this.commit("saveToStorage");
            }
        },
        /**
         * 删除和某个用户进行私聊的所有消息
         * @param state
         * @param friendId
         */
        removePrivateChat(state, friendId) {
            for (let idx in state.chats) {
                if (state.chats[idx].type == 'PRIVATE' &&
                    state.chats[idx].targetId == friendId) {
                    this.commit("removeChat", idx);
                }
            }
        },
        insertMessage(state, msgInfo) {
            console.log(msgInfo)
            let type =  'PRIVATE';
            // 记录消息的最大id
            if (msgInfo.id && type == "PRIVATE" && msgInfo.id > state.privateMsgMaxId) {
                state.privateMsgMaxId = msgInfo.id;
            }
            // 如果是已存在消息，则覆盖旧的消息数据
            let chat = this.getters.findChat(msgInfo); //找会话的窗口
            if (!chat)
                return;
            let message = this.getters.findMessage(chat, msgInfo); //覆盖已存在的消息
            if (message) {
                //进行对象的复制
                Object.assign(message, msgInfo);
                this.commit("saveToStorage");
                return;
            }
            // 插入新的数据，如果消息的类型是文本消息或者撤回消息则chat.lastContent = msgInfo.content;
             if (msgInfo.type == MESSAGE_TYPE.TEXT || msgInfo.type == MESSAGE_TYPE.RECALL) {
                chat.lastContent = msgInfo.content;
            }
             //设置发送时间
            chat.lastSendTime = msgInfo.sendTime;
             //设置发送人的姓名
            chat.sendNickName = msgInfo.sendNickName;
            // 未读加1
            if (!msgInfo.selfSend && msgInfo.status != MESSAGE_STATUS.READED && msgInfo.type != MESSAGE_TYPE.TIP_TEXT) {
                chat.unreadCount++;
            }
            // 间隔大于10分钟插入时间显示
            if (!chat.lastTimeTip || (chat.lastTimeTip < msgInfo.sendTime - 600 * 1000)) {
                chat.messages.push({
                    sendTime: msgInfo.sendTime,
                    type: MESSAGE_TYPE.TIP_TIME,
                });
                chat.lastTimeTip = msgInfo.sendTime;
            }
            // 新的消息
            chat.messages.push(msgInfo);
            this.commit("saveToStorage");
        },
        /**
         * 更新已经存在的消息
         * @param state
         * @param msgInfo
         */
        updateMessage(state, msgInfo) {
            // 获取对方id或群id
            let chat = this.getters.findChat(msgInfo);
            let message = this.getters.findMessage(chat, msgInfo);
            if (message) {
                // 属性拷贝
                Object.assign(message, msgInfo);
                this.commit("saveToStorage");
            }
        },
        /**
         * 删除已经存在的消息
         * @param state
         * @param msgInfo
         */
        deleteMessage(state, msgInfo) {
            let chat = this.getters.findChat(msgInfo);
            for (let idx in chat.messages) {
                // 已经发送成功的，根据id删除
                if (chat.messages[idx].id && chat.messages[idx].id == msgInfo.id) {
                    chat.messages.splice(idx, 1);
                    break;
                }
                // 正在发送中的消息可能没有id，根据发送时间删除
                if (msgInfo.selfSend && chat.messages[idx].selfSend &&
                    chat.messages[idx].sendTime == msgInfo.sendTime) {
                    chat.messages.splice(idx, 1);
                    break;
                }
            }
            this.commit("saveToStorage");
        },
        /**
         * 加载消息，如果不在加载中则对消息进行排序
         * @param state
         * @param loadding
         */
        loadingPrivateMsg(state, loadding) {
            state.loadingPrivateMsg = loadding;
            if (!state.loadingPrivateMsg) {
                this.commit("sort")
            }
        },
        /**
         * 通过消息的发送时间进行排序
         * @param state
         */
        sort(state) {
            state.chats.sort((c1, c2) => c2.lastSendTime - c1.lastSendTime);
        },
        /**
         * 将与所有人的对话，存储在localstorage里面
         * @param state
         */
        saveToStorage(state) {
            let userId = userStore.state.userId;
            let key = "chats-" + userId;
            let chatsData = {
                privateMsgMaxId: state.privateMsgMaxId,
                groupMsgMaxId: state.groupMsgMaxId,
                chats: state.chats
            }
            //将消息存到localstrage里面
            webdb.setItem(key,JSON.stringify(chatsData));
        },
        updateChatFromFriend(state, friend) {
            for (let i in state.chats) {
                let chat = state.chats[i];
                if (chat.type == 'PRIVATE' && chat.targetId == friend.id) {
                    chat.headImage = friend.headImageThumb;
                    chat.showName = friend.nickName;
                    break;
                }
            }
            this.commit("saveToStorage");
        },
    },
    actions: {
        /**
         * 装载消息，初始化消息
         * @param context
         * @returns {Promise<unknown>}
         */
        loadChat(context) {
            return new Promise((resolve, reject) => {
                let userId = userStore.state.userId;
                let key = "chats-" + userId;

                let item = localStorage.getItem(key)
                if (item) {
                    let chatsData = JSON.parse(item);
                    context.commit("initChats", chatsData);
                }
                resolve();
            })
        }
    }
}