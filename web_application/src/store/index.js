import { createStore } from 'vuex'
import friendStore from "@/store/friendStore";
import chatStore from "@/store/chatStore";
export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
    load(context) {
      this.dispatch("loadFriend");
      this.dispatch("loadChat");
    },
    unload(context){
      context.commit("clear");
    }
  },
  modules: {
    friendStore,
    chatStore
  },
  strict: process.env.NODE_ENV !== 'production'
})
