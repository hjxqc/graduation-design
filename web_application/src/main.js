import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import ElementPlus from "element-plus";
import * as socketApi from "@/api/wssocket";
import * as  enums from './api/enums.js';
import * as  date from './api/date.js';
import "element-plus/dist/index.css"
import webdb from "@/api/webdb";


const app = createApp(App);

app
    .use(ElementPlus)
    .use(router)
    .use(store)
    .use(store);

app.config.globalProperties.$wsApi = socketApi;  //便于在模板部分可直接用
app.config.globalProperties.$date = date;;  //便于在模板部分可直接用
app.config.globalProperties.$enums = enums;  //便于在模板部分可直接用
app.config.globalProperties.$webdb = webdb;  //便于在模板部分可直接用

app.mount('#app')


