import axios from "axios";

const sendMessage = (url,data,config) =>{
    return  new Promise((resolve,reject)=>{
        axios.post(url,data,config).then((res)=>{
            resolve(res);
        }).catch((error)=>{
            reject(error);
        });
    });
}


export default {
    sendMessage
}