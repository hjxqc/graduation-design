import axios from "axios";

const username_password_login = (username,password)=>{
    return  new Promise((resolve, reject) => {
        axios.post('/login/oauth/token',{
                grant_type: 'password',
                username: username ,
                password: password ,
                terminal: 0
            }
            ,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                auth: {
                    username: 'permission_authentication',
                    password: 'permission_authentication'
                }
            }).then(res => {
            resolve(res);
        }).catch(error=>{
            reject(error)
        });
    })


}

const mobile_login = (mobile,code)=>{
    return  new Promise((resolve, reject) => {
        axios.post('/login/oauth/token', {
                grant_type: 'mobile',
                mobile: mobile,
                code: code,
                terminal: 0
            }
            ,
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                auth: {
                    username: 'permission_authentication',
                    password: 'permission_authentication'
                }
            }).then(res => {
            console.log(res);
            resolve(res);
        }).catch(error => {
            reject(error)
        });
    });
}

const get_mobile_code = (mobile)=>{
    return new Promise((resolve, reject) => {
       axios.get('/login/permission/mobile/code/'+mobile).then(res=>{
           resolve(res);
       }).catch(error=>{
           reject(error);
       })
    });
}

const user_register = (url,data) => {
    return new Promise((resolve,reject)=>{
        axios.post(url,data,{
            headers: {
                'Content-Type': 'application/json'
            }})
            .then((res)=>{resolve(res)})
            .catch((error)=>{reject(error)});
    });
}

export default
{
    username_password_login,
    mobile_login,
    get_mobile_code,
    user_register
}