import axios from "axios";
const getApprovedPost = (userid , postid)=>{
    return new Promise((resolve, reject)=>{
        axios.get('/post_img/managerpost/approved/post/'+postid+'/'+userid).then(res=>{
            resolve(res)
        }).catch(error=>{
            reject(error)
        });
    });

};

const comment = (uri,userid,targetid,comment_content) => {
    const data = new FormData();
    data.set("content",comment_content);
    return new Promise((resolve, reject) => {
        axios.post('/thu_comm_col/comment'+uri+'comment/'+userid+'/'+targetid,data).then((res)=>{
            resolve(res);
        }).catch(error=>{
            reject(error);
        })
    });
}

const get_comprehensive = (postid,userid)=>{
    return new Promise((resolve, reject) => {
        axios.get('/thu_comm_col/comprehensive/post/comprehensive/'+postid+'/'+userid).then(res=>{
            resolve(res);
        }).catch(error=>{
            reject(error);
        })
    });
};

const cancel_thumsup = (uri,userid,target)=>{
    return new Promise((resolve, reject) => {
        axios.post('/thu_comm_col/thumbsup'+uri+'cancel/'+userid+'/'+target).then(res=>{
            resolve(res);
        }).catch((error)=>{
            reject(error);
        })
    })
}

const thumbsup = (uri,userid,targetid)=>{
    return new Promise((resolve, reject) => {
       axios.post('/thu_comm_col/thumbsup'+uri+userid+'/'+targetid).then(res=>{
           resolve(res);
       }).catch(error=>{
           reject(error);
       });
    });
}

const collection = (userid,postid)=>{
    return new Promise((resolve, reject) => {
        axios.post('/thu_comm_col/collection/post/collection/'+userid+'/'+postid).then(res=>{
            resolve(res);
        }).catch(error=>{
            reject(error);
        })
    })
}

const cancel_collection = (userid,postid)=>{
    return new Promise((resolve, reject) => {
        axios.post('/thu_comm_col/collection/collection/remove/'+userid+'/'+postid).then(res=>{
            resolve(res);
        }).catch(error=>{
            reject(error);
        })
    })
}

export default {
    getApprovedPost,comment,get_comprehensive,thumbsup,cancel_thumsup,collection,cancel_collection
}