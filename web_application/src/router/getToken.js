import axios from "axios";

const refreshToken=() =>{
    var token = localStorage.getItem("token");
    var refresh_token = localStorage.getItem("refresh_token");

    if (token === null || refresh_token === null) {
        window.location.href = "/";
    }

    //刷新令牌
    axios.post('/login/oauth/token', {
            grant_type: 'refresh_token',
            refresh_token: refresh_token,
            terminal: 0
        }
        ,
        {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            auth: {
                username: 'permission_authentication',
                password: 'permission_authentication'
            }
        }).then(res => {
        if (res.status == 200){
            localStorage.setItem("token",res.data.access_token);
            localStorage.setItem("refresh_token",res.data.refresh_token);
        }else {
            window.location.href = "/";
        }
    }).catch(() => {
        window.location.href = "/";
    });
}

const check_token = (token)=>{
    return new Promise((resolve, reject) => {
        axios.post("/login/oauth/check_token",{
                token: token
            },
            {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                auth: {
                    username: 'permission_authentication',
                    password: 'permission_authentication'
                }
            }).then(res=>{
            resolve(res);
        }).catch(error=>{
            reject(error);
        })
    })
}

const initToken = ()=>{
    // //判断token是否有过期,如果过期自动刷新
    var token = localStorage.getItem("token");
    var refresh_token = localStorage.getItem("refresh_token");
    if(token && refresh_token) {
       return  new Promise((resolve,reject)=>{
            check_token(token).then(res => {
                if (res.status != 200) {
                    console.log("刷新token");
                    refreshToken();
                }
            }).catch(() => {
                refreshToken();
            });
            resolve();
        });
    }else{
        return null;
    }
}

export default {
    initToken
}