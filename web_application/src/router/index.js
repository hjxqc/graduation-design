import { createRouter, createWebHistory } from 'vue-router'
import axios from "axios";
import userStore from "@/store/user_store/userStore";
import imInfo from "@/api/imInfo";
import getToken from "@/router/getToken";

const routes = [
  {
    path: "/",
    redirect: "/index",
    meta:{
      intercept: false
    }
  },
  {
    name:"index",
    path:"/index",
    component:()=> import("@/pages/index.vue"),
    redirect: "/displayed",
    meta:{
      intercept: false
    },
    children: [
      {
        name:"inform",
        path:"/inform",
        component:()=> import("@/pages/inform/inform.vue"),
        meta:{
          intercept: true
        }
      },
      {
        name:"search",
        path:"/search",
        component:()=> import("@/pages/search/search.vue"),
        meta:{
          intercept: false
        }
      },
      {
        name:"displayed",
        path:"/displayed",
        component:()=> import("@/pages/displayed_page/displayed.vue"),
        meta:{
          intercept: false
        }
      },
      {
        name:"dynamic",
        path:"/dynamic",
        component:()=> import("@/pages/displayed_page/dynamic.vue"),
        meta:{
          intercept: true
        }
      },
      {
        name:"push",
        path: "/push",
        component: () => import("@/pages/publishing/posts.vue"),
        meta: {
          intercept: true
        }
      },{
        name: "user_detail",
        path: "/user/detail",
        redirect: "/user/post",
        component:() => import('@/pages/user_detail/user_detail.vue'),
        meta: {
          intercept: true
        },
        children:[
          {
            name: "user_post",
            path: "/user/post",
            component: () => import('@/pages/user_detail/user_post.vue'),
            meta: {
              intercept: true
            }
          },
          {
            name: "user_upimg",
            path: "/user/upimg",
            component: () => import('@/pages/user_detail/user_upimg.vue'),
            meta: {
              intercept: true
            }
          },
          {
            name: "user_collection",
            path: "/user/collection",
            component: () => import('@/pages/user_detail/user_collection.vue'),
            meta: {
              intercept: true
            }
          },
          {
            name: "user_thumbup",
            path: "/user/thumbup",
            component: () => import('@/pages/user_detail/user_thumbup.vue'),
            meta: {
              intercept: true
            }
          }
        ]
      }
    ]
  },
  {
    name:"detail_page",
    path: "/detail/page/:postid",
    meta: {
      intercept: true
    },
    component: () => import("@/pages/displayed_page/detail_page/page_detail.vue")
  },
  {
    name:"user_login",
    path: "/user/login",
    component: () => import("@/pages/user_login/user_login.vue"),
    meta: {
      intercept: false
    }
  },
  {
    name:"user_register",
    path: "/user/register",
    component: () => import("@/pages/user_login/userRegister.vue"),
    meta: {
      intercept: false
    }
  },
  {
    name: "im_friend",
    path: "/im",
    component: () => import("@/pages/im_chat/im_friend/IMFriend.vue"),
    meta: {
      intercept: true
    }
  },
  {
    name: "im_chat",
    path: "/im/chat",
    component: () => import("@/pages/im_chat/im_chat/IMChat.vue"),
    meta: {
      intercept: false
    }
  },
  {
    name: "app",
    path: "/app",
    component: () => import("@/App.vue"),
    redirect: "/displayed",
    meta: {
      intercept: false
    }
  },
  {
    name: "admin",
    path: "/admin",
    component: () => import("@/pages/admin/admin_index.vue"),
    meta: {
      intercept: false
    },
    children: [
      {
        name: "admin_content_examine",
        path: "/admin/content/examine",
        component: () => import("@/pages/admin/content_examine/content_examine_admin.vue"),
        meta: {
          intercept: false
        }
      },
      {
        name: "admin_user",
        path: "/admin/user",
        component: () => import("@/pages/admin/user_admin/user_admin.vue"),
        meta: {
          intercept: false
        }
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
});
router.beforeEach((to, from, next)=>{
  // //判断token是否有过期,如果过期自动刷新
  var promise = getToken.initToken();
  if (promise){
    promise.then(()=>{
      userStore.dispatch("get_user_info",token);
      imInfo.init();
    });
  }
  var token = localStorage.getItem("token");
  var refresh_token = localStorage.getItem("refresh_token");

  if (to.path === '/' || to.path === '/index')
    next();

  if(!to.meta.intercept)
    next();

    if(
        token == null || token == undefined || token === ""||
        refresh_token == null || refresh_token == null || refresh_token == null
      )
      next("/user/login");
    else
      next();
})

export default router
