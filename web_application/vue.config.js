const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 关闭eslint校验
  lintOnSave: false,
  devServer:{
    proxy:{
      "/post_img": { //代理接口的前缀
        "target": "http://localhost:5072",
        "changeOrigin": true, // 如果设置为true,那么本地会虚拟一个服务器接收你的请求并代你发送该请求，这样就不会有跨域问题（只适合开发环境）
        "pathRewrite": { //重写路径 比如'/apis/aaa/ccc'重写为'/aaa/ccc'
          '^/post_img': ''
        }
      },
      "/thu_comm_col":{
        "target" : "http://localhost:5073",
        "changeOrigin" : true,
        "pathRewrite" :{
          '^/thu_comm_col' : ''
        }
      },
      "/login":{
        "target" : "http://localhost:5074/auth",
        "changeOrigin" : true,
        "pathRewrite" :{
          '^/login' : ''
        }
      },
      "/fr":{
        "target" : "http://localhost:8888",
        "changeOrigin" : true,
        "pathRewrite" :{
          '^/fr' : ''
        }
      },
      "/msg":{
        "target" : "http://localhost:8888",
        "changeOrigin" : true,
        "pathRewrite" :{
          '^/msg' : ''
        }
      },
      "/usr":{
        "target" : "http://localhost:8888",
        "changeOrigin" : true,
        "pathRewrite" :{
          '^/usr' : ''
        }
      }
    }
  }
})
