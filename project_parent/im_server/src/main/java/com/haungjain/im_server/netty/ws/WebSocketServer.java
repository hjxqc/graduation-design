package com.haungjain.im_server.netty.ws;

import com.haungjain.im_server.netty.IMChannelHandler;
import com.haungjain.im_server.netty.IMServer;
import com.haungjain.im_server.netty.ws.endecode.MessageProtocolDecode;
import com.haungjain.im_server.netty.ws.endecode.MessageProtocolEncode;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "websocket" , value="enable" , havingValue = "true",matchIfMissing = true)
public class WebSocketServer implements IMServer {

    @Value("${websocket.port}")
    private int port;

    private volatile boolean ready = false;

    private EventLoopGroup bossGroup;

    private EventLoopGroup workGroup;

    public boolean isReady() {
        return ready;
    }

    @Override
    public void start() {
        ServerBootstrap bootstrap = new ServerBootstrap();
        bossGroup = new NioEventLoopGroup();
        workGroup = new NioEventLoopGroup();
        //设置主从线程模型
        bootstrap.group(bossGroup,workGroup)
                //设置 服务端Nio通信类型
                .channel(NioServerSocketChannel.class)
                //设置ChannelPipline,也就是业务职责链，由处理器Handler串联而成，由从线程处理
                .childHandler(new ChannelInitializer<Channel>() {
                    // 添加处理器Handler，通常包含消息解码，业务处理，也可以是日志，权限，过滤等
                    @Override
                    protected void initChannel(Channel ch) throws Exception {
                        //获取责任链
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new IdleStateHandler(120,0,0, TimeUnit.SECONDS));
                        pipeline.addLast("http-codec", new HttpServerCodec());
                        pipeline.addLast("aggregator",new HttpObjectAggregator(65535));
                        pipeline.addLast("http-chunked",new ChunkedWriteHandler());
                        pipeline.addLast(new WebSocketServerProtocolHandler("/im"));
                        pipeline.addLast("encode",new MessageProtocolEncode());
                        pipeline.addLast("decode",new MessageProtocolDecode());
                        pipeline.addLast("handler",new IMChannelHandler());
                    }
                })
                // bootstarp 还可以设置tcp参数，根据需要可以分别设置主线程池和从线程池参数，来优化服务器
                // 其中主线程池使用option来设置，从线程池使用childOption方法设置
                // backlog表示主线程池在套接口排队的最大数量，队列由未连接队列（三次握手完成的）和已连接队列
                .option(ChannelOption.SO_BACKLOG,5)
                .childOption(ChannelOption.SO_KEEPALIVE,true);

        try {
            //绑定端口，启动select线程，轮询监听channel事件，监听到事件之后就会交给线程池处理
            bootstrap.bind(port).sync().channel();
            //就绪标识
            this.ready = true;
            log.info("websocket server  初始化完成，端口：{}",port);
            //端口服务关闭

        } catch (InterruptedException e){
            log.info("websocket server 初始化异常",e);
        }

    }

    @Override
    public void stop() {
        if (bossGroup !=null && !bossGroup.isShuttingDown() && !bossGroup.isShutdown())
            bossGroup.shutdownGracefully();

        if (workGroup !=null && !workGroup.isShuttingDown() && !workGroup.isShutdown())
            workGroup.shutdownGracefully();

        this.ready = false;

        log.info("websocket server 停止");
    }


}
