package com.haungjain.im_server.netty.processor;

import cn.hutool.core.bean.BeanUtil;
import com.haungjain.im_common.contant.IMConstant;
import com.haungjain.im_common.contant.IMRedisKey;
import com.haungjain.im_common.enums.IMCmdType;
import com.haungjain.im_common.model.IMHeartbeatInfo;
import com.haungjain.im_common.model.IMSendInfo;
import com.haungjain.im_server.constant.ChannelAttrkey;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.AttributeKey;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@RequiredArgsConstructor
public class HeartbeatProcessor extends AbstractMessageProcessor<IMHeartbeatInfo>{

    private final RedisTemplate<String,Object> redisTemplate;

    @Override
    public void process(ChannelHandlerContext ctx, IMHeartbeatInfo data) {
        //响应ws
        IMSendInfo sendInfo = new IMSendInfo();
        sendInfo.setCmd(IMCmdType.HEART_BEAT.code());
        ctx.channel().writeAndFlush(sendInfo);

        //设置属性
        AttributeKey<Long> heartBeatAttr = AttributeKey.valueOf(ChannelAttrkey.HEARTBEAT_TIMES);
        Long heartbeatTime = ctx.channel().attr(heartBeatAttr).get();
        //增加心跳次数
        ctx.channel().attr(heartBeatAttr).set(++heartbeatTime);
        //如果每次心跳到10的倍数就会进行一次在线的续命
        if (heartbeatTime % 10 == 0){
            //每十次进行一次续命
            AttributeKey<String> userIdAttr = AttributeKey.valueOf(ChannelAttrkey.USER_ID);
            String userId = ctx.channel().attr(userIdAttr).get();
            AttributeKey<Integer> terminalAttr = AttributeKey.valueOf(ChannelAttrkey.TERMINAL_TYPE);
            Integer terminal = ctx.channel().attr(terminalAttr).get();
            String key = String.join(":", IMRedisKey.IM_USER_SERVER_ID, userId, terminal.toString());
            redisTemplate.expire(key, IMConstant.ONLINE_TIMEOUT_SECOND, TimeUnit.SECONDS);//重新设置过期时间
        }
    }

    @Override
    public IMHeartbeatInfo transForm(Object o) {
        HashMap map = (HashMap) o;
        return BeanUtil.fillBeanWithMap(map,new IMHeartbeatInfo(),false);
    }
}
