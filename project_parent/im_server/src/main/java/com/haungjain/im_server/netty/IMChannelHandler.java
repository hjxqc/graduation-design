package com.haungjain.im_server.netty;

import com.haungjain.im_common.contant.IMRedisKey;
import com.haungjain.im_common.enums.IMCmdType;
import com.haungjain.im_common.model.IMSendInfo;
import com.haungjain.im_server.constant.ChannelAttrkey;
import com.haungjain.im_server.netty.processor.AbstractMessageProcessor;
import com.haungjain.im_server.netty.processor.ProcessorFactory;
import com.haungjain.im_server.util.SpringContextHolder;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.AttributeKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

@Slf4j
public class IMChannelHandler extends SimpleChannelInboundHandler<IMSendInfo> {

    /**
     * 读取到消息后进行处理
     * @param channelHandlerContext 上下文
     * @param imSendInfo 发送的消息
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, IMSendInfo imSendInfo) throws Exception {
        //处理器进行处理
        AbstractMessageProcessor processor = ProcessorFactory.createProcessor(IMCmdType.fromCode(imSendInfo.getCmd()));
        processor.process(channelHandlerContext,processor.transForm(imSendInfo.getData()));
    }

    /**
     * 出现异常的处理 打印错误报告
     *
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error(cause.getMessage());
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        log.info(ctx.channel().id().asLongText() + "连接");
    }

    /**
     * 删除channel,并在redis中设置为下线
     * @param ctx
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        AttributeKey<String> userIdAttr = AttributeKey.valueOf(ChannelAttrkey.USER_ID);
        String userId = ctx.channel().attr(userIdAttr).get();
        log.info("userId:{}",userId);
        AttributeKey<Integer> terminalAttr = AttributeKey.valueOf(ChannelAttrkey.TERMINAL_TYPE);
        Integer terminal = ctx.channel().attr(terminalAttr).get();
        log.info("terminal:{}",terminal);
        ChannelHandlerContext context = UserChannelCtxMap.getChannelCtx(userId, terminal);
        log.info("ctx:{}",context);
        // 判断一下，避免异地登录导致的误删
        if (context != null && ctx.channel().id().equals(context.channel().id())) {
            // 移除channel
            UserChannelCtxMap.removeChannleCtx(userId, terminal);
            // 用户下线
            RedisTemplate<String, Object> redisTemplate = SpringContextHolder.getBean("redisTemplate");
            String key = String.join(":", IMRedisKey.IM_USER_SERVER_ID, userId.toString(), terminal.toString());
            redisTemplate.delete(key);
            log.info("断开连接,userId:{},终端类型:{}", userId, terminal);
        }
    }

    /**
     * 处理心跳时间
     * @param ctx
     * @param evt
     * @throws Exception
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleState state = ((IdleStateEvent) evt).state();
            if (state == IdleState.READER_IDLE) {
                // 在规定时间内没有收到客户端的上行数据, 主动断开连接
                AttributeKey<String> attr = AttributeKey.valueOf("USER_ID");
                String userId = ctx.channel().attr(attr).get();
                AttributeKey<Integer> terminalAttr = AttributeKey.valueOf(ChannelAttrkey.TERMINAL_TYPE);
                Integer terminal = ctx.channel().attr(terminalAttr).get();
                log.info("心跳超时，即将断开连接,用户id:{},终端类型:{} ", userId, terminal);
                ctx.channel().close();
            }
        } else {
            super.userEventTriggered(ctx, evt);
        }

    }
}
