package com.haungjain.im_server.netty.processor;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.haungjain.im_common.contant.IMConstant;
import com.haungjain.im_common.contant.IMRedisKey;
import com.haungjain.im_common.enums.IMCmdType;
import com.haungjain.im_common.model.IMLoginInfo;
import com.haungjain.im_common.model.IMSendInfo;
import com.haungjain.im_common.model.IMSessionInfo;
import com.haungjain.im_common.util.JwtUtil;
import com.haungjain.im_server.constant.ChannelAttrkey;
import com.haungjain.im_server.netty.IMServerGroup;
import com.haungjain.im_server.netty.UserChannelCtxMap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.AttributeKey;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@RequiredArgsConstructor
public class LoginProcessor extends AbstractMessageProcessor<IMLoginInfo>{
    private final RedisTemplate<String,Object> redisTemplate;

    @Value("${jwt.accessToken.secret}")
    private String accessTokenSecret;

    @Override
    public void process(ChannelHandlerContext ctx, IMLoginInfo loginInfo) {
        if (!JwtUtil.checkSign(loginInfo.getAccessToken(),accessTokenSecret)){
            ctx.channel().close();
            log.warn("用户token校验不通过，强制下线，token:{}",loginInfo.getAccessToken());
        }

        String strInfo = JwtUtil.getInfo(loginInfo.getAccessToken());
        IMSessionInfo imSessionInfo = JSONObject.parseObject(strInfo, IMSessionInfo.class);
        String userId = imSessionInfo.getUserId();
        Integer terminal = imSessionInfo.getTerminal();
        log.info("用户登录：usrid{}",userId);
        ChannelHandlerContext channelCtx = UserChannelCtxMap.getChannelCtx(userId, terminal);
        if (channelCtx != null && !ctx.channel().id().equals(channelCtx.channel().id())){
            //不允许多地登录，强制下线
            IMSendInfo<Object> sendInfo = new IMSendInfo<>();
            sendInfo.setCmd(IMCmdType.FORCE_LOGUT.code());
            sendInfo.setData("您已在其他地方登录，将被强制下站");
            channelCtx.channel().writeAndFlush(sendInfo);
            log.info("异地登录，强制下线，userId:{}",userId);
        }
        //绑定用户channel
        UserChannelCtxMap.addChannelCtx(userId,terminal,ctx);
        //设置用户id地址
        AttributeKey<String> userIdAttr = AttributeKey.valueOf(ChannelAttrkey.USER_ID);
        ctx.channel().attr(userIdAttr).set(userId);
        //设置用户中终端
        AttributeKey<Integer> terminalAttr = AttributeKey.valueOf(ChannelAttrkey.TERMINAL_TYPE);
        ctx.channel().attr(terminalAttr).set(terminal);
        //初始化心跳次数
        AttributeKey<Long> heartBeatAttr = AttributeKey.valueOf(ChannelAttrkey.HEARTBEAT_TIMES);
        ctx.channel().attr(heartBeatAttr).set(0L);
        //在redis中记录每个user的channelId，15秒没心跳则自动过期
        String key = String.join(":", IMRedisKey.IM_USER_SERVER_ID, userId, terminal.toString());
        //当用户登录时将 IMServerGroup.serverId根据key上传redis中
        redisTemplate.opsForValue().set(key, IMServerGroup.serverId, IMConstant.ONLINE_TIMEOUT_SECOND, TimeUnit.SECONDS);
        //响应ws
        IMSendInfo<Object> sendInfo = new IMSendInfo<>();
        sendInfo.setCmd(IMCmdType.LOGIN.code());
        ctx.channel().writeAndFlush(sendInfo);
    }

    public IMLoginInfo transForm(Object o){
        HashMap hashMap = (HashMap)o;
        return BeanUtil.fillBeanWithMap(hashMap,new IMLoginInfo(),false);
    }
}
