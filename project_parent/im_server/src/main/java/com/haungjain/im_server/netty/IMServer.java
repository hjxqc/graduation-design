package com.haungjain.im_server.netty;

public interface IMServer {

    boolean isReady();

    void start();

    void stop();
}
