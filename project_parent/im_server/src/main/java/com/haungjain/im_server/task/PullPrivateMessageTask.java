package com.haungjain.im_server.task;

import com.alibaba.fastjson.JSONObject;
import com.haungjain.im_common.contant.IMRedisKey;
import com.haungjain.im_common.enums.IMCmdType;
import com.haungjain.im_common.model.IMRecvInfo;
import com.haungjain.im_server.netty.IMServerGroup;
import com.haungjain.im_server.netty.processor.AbstractMessageProcessor;
import com.haungjain.im_server.netty.processor.ProcessorFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class PullPrivateMessageTask extends AbstractMessageResultTask{

    @Autowired
    private final RedisTemplate<String,Object> redisTemplate;

    @Override
    public void pullMessage() throws InterruptedException {
        //从redis中拉取消息
        String key = String.join(":", IMRedisKey.IM_MESSAGE_PRIVATE_QUEUE, IMServerGroup.serverId + "");
//        JSONObject jsonObject = (JSONObject) redisTemplate.opsForList().leftPop(key);
        String jsonString = JSONObject.toJSONString(redisTemplate.opsForList().leftPop(key));
        JSONObject jsonObject = JSONObject.parseObject(jsonString);
        log.info("消息：{}", jsonObject);
        log.info("key:{},pullMessage:{}",key,jsonObject);
        while (!ObjectUtils.isEmpty(jsonObject)){
            IMRecvInfo recvInfo = JSONObject.toJavaObject(jsonObject, IMRecvInfo.class);
            //将消息推送到用户手中
            AbstractMessageProcessor processor = ProcessorFactory.createProcessor(IMCmdType.PRIVATE_MESSAGE);
            processor.process(recvInfo);
            //下一条消息
            jsonString = JSONObject.toJSONString(redisTemplate.opsForList().leftPop(key));
            jsonObject = JSONObject.parseObject(jsonString);
        }
    }
}
