package com.haungjain.im_server.netty.processor;

import com.haungjain.im_common.enums.IMCmdType;
import com.haungjain.im_server.util.SpringContextHolder;

public class ProcessorFactory {

    public static AbstractMessageProcessor createProcessor(IMCmdType cmd){
        AbstractMessageProcessor processor = null;
        switch (cmd){
            case LOGIN:
                processor = SpringContextHolder.getBean(LoginProcessor.class);
                break;
            case HEART_BEAT:
                processor = SpringContextHolder.getBean(HeartbeatProcessor.class);
                break;
            case PRIVATE_MESSAGE:
                processor = SpringContextHolder.getBean(PrivateMessageProcessor.class);
                break;
            default:
                break;
        }
        return processor;
    }
}
