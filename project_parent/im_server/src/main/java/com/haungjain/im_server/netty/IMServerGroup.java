package com.haungjain.im_server.netty;

import com.haungjain.im_common.contant.IMRedisKey;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class IMServerGroup implements CommandLineRunner {

    public static volatile long serverId = 0;

    @Autowired
    RedisTemplate<String,Object> redisTemplate;

    private final List<IMServer> imServers;

    /**
     * 判断服务器是否就绪
     * @return
     */
    public boolean isReady() {
        for (IMServer imServer : imServers) {
            if (!imServer.isReady())
                return false;
        }

        return true;
    }

    @Override
    public void run(String... args) throws Exception {
        //初始化SERVER_ID
        String key = IMRedisKey.IM_MAX_SERVER_ID;
        //每次启动服务serverId会进行自增，说明上个服务器版本过期
        serverId = redisTemplate.opsForValue().increment(key);
        //启动服务
        for (IMServer imServer : imServers) {
            imServer.start();
        }
    }

    @PreDestroy
    public void destory() {
        for (IMServer imServer : imServers) {
            imServer.stop();
        }
    }
}
