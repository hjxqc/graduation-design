package com.haungjain.im_server.task;

import com.haungjain.im_common.util.ThreadPoolExecutorFactory;
import com.haungjain.im_server.netty.IMServerGroup;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;

@Slf4j
public abstract class AbstractMessageResultTask implements CommandLineRunner {

    /**
     * 线程池
     */
    private static final ExecutorService EXECUTOR_SERVICE = ThreadPoolExecutorFactory.getThreadPoolExecutor();

    @Autowired
    private IMServerGroup serverGroup;

    @Override
    public void run(String... args) throws Exception {
        //初始化定时器
        EXECUTOR_SERVICE.execute(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                try {
                    if (serverGroup.isReady()){
                        pullMessage();
                    }
                }catch (Exception e){
                    log.error("任务调度异常",e);
                }

                if (!EXECUTOR_SERVICE.isShutdown()){
                    Thread.sleep(100);
                    EXECUTOR_SERVICE.execute(this); //递归保证线程进行下去
                }
            }
        });
    }

    @PreDestroy
    public void destory(){
        log.info("{}线程任务关闭",this.getClass().getSimpleName());
        EXECUTOR_SERVICE.shutdown();;
    }

    public abstract void pullMessage() throws InterruptedException;
}
