package com.haungjain.im_server.constant;

public class ChannelAttrkey {

    private ChannelAttrkey(){}

    /**
     * 用户ID
     */
    public static final String USER_ID = "USER_ID";
    /**
     * 终端类型
     */
    public static final String TERMINAL_TYPE = "TERMINAL_TYPE";
    /**
     * 心跳次数
     */
    public static final String HEARTBEAT_TIMES = "HEARTBEAt_TIMES";
}
