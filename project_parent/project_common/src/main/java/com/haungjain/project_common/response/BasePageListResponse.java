package com.haungjain.project_common.response;

import com.haungjain.project_common.properties.exception.CommonProperties;
import lombok.Data;

import java.util.List;

@Data
public class BasePageListResponse<T> {
    private Integer Code;

    private String Message;

    private List<T> Data;

    private Integer page_data_sum;

    public BasePageListResponse(CommonProperties commonProperties,List<T> data,Integer page_data_sum){
        this.Code = commonProperties.getCode();
        this.Message = commonProperties.getMessage();
        this.Data = data;
        this.page_data_sum = page_data_sum;
    }
}
