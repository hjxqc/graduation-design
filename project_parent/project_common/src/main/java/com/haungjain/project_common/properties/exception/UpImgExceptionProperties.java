package com.haungjain.project_common.properties.exception;

public enum UpImgExceptionProperties implements ExceptionProperties{
    ;

    private Integer code;

    private String mesage;

    UpImgExceptionProperties(Integer code, String mesage) {
        this.code = code;
        this.mesage = mesage;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.mesage;
    }
}
