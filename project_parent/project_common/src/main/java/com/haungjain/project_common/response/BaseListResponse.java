package com.haungjain.project_common.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseListResponse <T>{
    private Integer Code;

    private String Message;

    private List<T> Data;
}
