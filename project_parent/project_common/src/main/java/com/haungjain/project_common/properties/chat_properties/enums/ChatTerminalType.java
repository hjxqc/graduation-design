package com.haungjain.project_common.properties.chat_properties.enums;

import lombok.AllArgsConstructor;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@AllArgsConstructor
public enum ChatTerminalType {

    /**
     *web
     */
    WEB(0,"web"),
    /**
     * app
     */
    APP(1,"app"),
    /**
     * pc
     */
    PC(2,"pc");


    private final Integer code;

    private final String desc;

    public static ChatTerminalType fromCode(Integer code){
        for (ChatTerminalType chatTerminalType: values()){
            if (chatTerminalType.code.equals(code)){
                return chatTerminalType;
            }
        }

        return null;
    }

    public static List<Integer> codes(){
        return Arrays.stream(values()).map(ChatTerminalType::getCode).collect(Collectors.toList());
    }

    public Integer getCode() {
        return code;
    }
}
