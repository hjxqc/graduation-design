package com.haungjain.project_common.response;

import com.haungjain.project_common.properties.exception.CommonProperties;

public class ResponseBuilder {
    public static BaseResponse ok(Object data) {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setCode(CommonProperties.OK.getCode());
        baseResponse.setMessage(CommonProperties.OK.getMessage());
        baseResponse.setData(data);
        return baseResponse;
    }

    public static BaseResponse fail(Object data) {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setCode(CommonProperties.FAIL.getCode());
        baseResponse.setMessage(CommonProperties.FAIL.getMessage());
        baseResponse.setData(data);
        return baseResponse;
    }
}
