package com.haungjain.project_common.exception;

import com.haungjain.project_common.properties.exception.ExceptionProperties;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BaseException extends Exception{
    private Integer code;

    private String message;

    @Override
    public String getMessage() {
        return this.message;
    }

    public BaseException(ExceptionProperties exceptionProperties){
        this.code = exceptionProperties.getCode();
        this.message = exceptionProperties.getMessage();
    }
}
