package com.haungjain.project_common.properties.chat_properties.contant;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

public class ChatContant {

    private ChatContant(){}

    /**
     * 在线状态过期时间
     */
    public static final Long ONLINE_TIMEOUT_SECOND = 600L;
    /**
     * 消息允许撤回时间
     */
    public static final Long ALLOW_RECALL_SECOND = 300l;

}
