package com.haungjain.project_common.properties.exception;

public enum CommonProperties implements ExceptionProperties{
    OK(200,"操作成功"),
    FAIL(500,"失败"),
    SERVE_BUSY(505,"服务器繁忙");


    private Integer code;

    private String message;

    CommonProperties(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
