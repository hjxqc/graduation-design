package com.haungjain.project_common.properties.exception;

import lombok.Data;

public enum PostExceptionProperties implements ExceptionProperties{
    THE_POST_DONT_EXIST(4004,"该帖子不存在");

    private Integer code;

    private String message;

    PostExceptionProperties(Integer code, String message) {
        code = code;
        message = message;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
