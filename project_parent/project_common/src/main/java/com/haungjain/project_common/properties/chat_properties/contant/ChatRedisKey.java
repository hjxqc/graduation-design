package com.haungjain.project_common.properties.chat_properties.contant;

public class ChatRedisKey {

    private ChatRedisKey(){}

    /**
     * im-server最大id，从0开始递增
     */
    public static final String CHAT_MAX_SERVER_ID = "chat:max_server_id";
    /**
     * 用户ID所连接的CHAT-server的ID
     */
    public static final String CHAT_USER_SERVER_ID = "chat:user:server_id";
    /**
     * 未读私聊消息队列
     */
    public static final String CHAT_MESSAGE_PRIVATE_QUEUE = "chat:message:private";
    /**
     * 未读群聊消息
     */
    public static final String CHAT_MESSAGE_GROUP_QUEUE = "chat:message:group";
    /**
     * 私聊消息发送结果队列
     */
    public static final String CHAT_RESULT_PRIVATE_QUEUE = "chat:result:private";
    /**
     * 群聊消息发送结果队列
     */
    public static final String  CHAT_RESULT_GROUP_QUEUE = "chat:result:group";

}
