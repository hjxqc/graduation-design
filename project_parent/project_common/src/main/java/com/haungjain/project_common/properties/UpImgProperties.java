package com.haungjain.project_common.properties;

/**
 * 上传照片相关的属性
 */
public interface UpImgProperties {
    public static Integer UP_IMG_STATUSE_PRIVATE = 0;

    public static Integer UP_IMG_STATUSE_PUBLIC = 1;
}
