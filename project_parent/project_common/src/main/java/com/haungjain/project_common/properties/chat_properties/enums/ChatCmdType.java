package com.haungjain.project_common.properties.chat_properties.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ChatCmdType {

    /**
     * 登录
     */
    LOGIN(0,"登录"),
    /**
     *心跳
     */
    HEART_BEAT(1,"心跳"),
    /**
     *强制下线
     */
    FORCE_LOGOUT(2,"强制下线"),
    /**
     * 私聊消息
     */
    PRIVATE_MESSAGE(3,"私聊消息"),
    /**
     *群发消息
     */
    GOURP_MESSAGE(4,"群发消息");

    private final Integer code;

    private final String desc;

    public static ChatCmdType fromCode(Integer code){
        for (ChatCmdType chatCmdType:values()){
            if(chatCmdType.getCode().equals(code)){
                return chatCmdType;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }
}
