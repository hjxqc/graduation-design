package com.haungjain.project_common.properties.exception;

public interface ExceptionProperties {
    public Integer getCode();

    public String getMessage();
}
