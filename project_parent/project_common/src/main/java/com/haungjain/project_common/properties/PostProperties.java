package com.haungjain.project_common.properties;

/**
 * 帖子相关的属性
 */
public interface PostProperties {
    static public Integer POST_StATUS_PRIVATE = 1;

    static public Integer POST_StATUS_PUBLIC = 0;

    static public Integer REVIEW_PROGRESS_PASS = 0;

    static public Integer REVIEW_PROGRESS_ONGOING = 1;
}
