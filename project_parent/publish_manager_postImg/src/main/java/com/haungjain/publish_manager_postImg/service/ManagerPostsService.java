package com.haungjain.publish_manager_postImg.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.CommonService.ImgServiceInf;
import com.haungjain.project_interface.posts_data.ApprovedPostMapper;
import com.haungjain.project_interface.publish__manager_postingService.ManagerPostsServiceInf;
import com.haungjain.project_interface.posts_data.user_data.PostUserMapper;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.publish_manager_postImg.Img;
import com.haungjain.project_pojos.publish_manager_postImg.request.PostRequest;
import com.haungjain.project_pojos.thumbsup_comments_collection.Comprehensive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ManagerPostsService extends ServiceImpl<ApprovedPostMapper, ApprovedPost> implements ManagerPostsServiceInf {

    @Autowired
    private ImgServiceInf imgServiceInf;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PostUserMapper postUserMapper;

    @Override
    public BaseListResponse<ApprovedPost> getAllApprovedPost(String userId, PostRequest postRequest) throws BaseException {
        if (postRequest == null)
            throw new BaseException(CommonProperties.FAIL);

        IPage<ApprovedPost> approvedPostIPage = new Page<>(postRequest.getCurrent(), postRequest.getSize());

        Page<ApprovedPost> allApprovedPage = baseMapper.getAllApprovedPost(approvedPostIPage, userId);

        List<ApprovedPost> allApprovedPost = allApprovedPage.getRecords();


        //如果没有帖子数据内容设置为空
        if (allApprovedPost.size() == 0)
            return new BaseListResponse<ApprovedPost>(CommonProperties.OK.getCode(), CommonProperties.OK.getMessage(), new ArrayList<>());

        for (ApprovedPost approvedPost : allApprovedPost) {
            String postsId = approvedPost.getPostId();

            String postCover = approvedPost.getPostCover();
            Img cover = imgServiceInf.getById(postCover);
            //设置封面
            approvedPost.setCover(cover);

            List<Img> PostAllImg = imgServiceInf.getAllApprovedPostImg(postsId);
            approvedPost.setPostsImgContent(PostAllImg);

            UserInformation userInformation = postUserMapper.selectById(approvedPost.getUserId());
            approvedPost.setUserInformation(userInformation);
        }

        /**
         * 查询所有点赞、评论、收藏
         */

        return new BaseListResponse<ApprovedPost>(CommonProperties.OK.getCode(), CommonProperties.OK.getMessage(), allApprovedPost);
    }

    @Override
    public BaseListResponse<ApprovedPost> getAllApprovedPost(List<String> userId, PostRequest postRequest) throws BaseException {
        if (postRequest == null)
            throw new BaseException(CommonProperties.FAIL);

        IPage<ApprovedPost> approvedPostIPage = new Page<>(postRequest.getCurrent(), postRequest.getSize());

        Page<ApprovedPost> allApprovedPage = baseMapper.getAllApprovedPostByIds(approvedPostIPage, userId);

        List<ApprovedPost> allApprovedPost = allApprovedPage.getRecords();


        //如果没有帖子数据内容设置为空
        if (allApprovedPost.size() == 0)
            return new BaseListResponse<ApprovedPost>(CommonProperties.OK.getCode(), CommonProperties.OK.getMessage(), new ArrayList<>());

        for (ApprovedPost approvedPost : allApprovedPost) {
            String postsId = approvedPost.getPostId();

            String postCover = approvedPost.getPostCover();
            Img cover = imgServiceInf.getById(postCover);
            //设置封面
            approvedPost.setCover(cover);

            List<Img> PostAllImg = imgServiceInf.getAllApprovedPostImg(postsId);
            approvedPost.setPostsImgContent(PostAllImg);

            UserInformation userInformation = postUserMapper.selectById(approvedPost.getUserId());
            approvedPost.setUserInformation(userInformation);
        }

        /**
         * 查询所有点赞、评论、收藏
         */

        return new BaseListResponse<ApprovedPost>(CommonProperties.OK.getCode(), CommonProperties.OK.getMessage(), allApprovedPost);
    }

    @Override
    public BaseListResponse<ApprovedPost> searchAllApprovedPostByNanme(String post_name, PostRequest postRequest) throws BaseException {
        if (postRequest == null)
            throw new BaseException(CommonProperties.FAIL);

        IPage<ApprovedPost> approvedPostIPage = new Page<>(postRequest.getCurrent(), postRequest.getSize());

        Page<ApprovedPost> allApprovedPage = baseMapper.getAllApprovedPostByName(approvedPostIPage, post_name);

        List<ApprovedPost> allApprovedPost = allApprovedPage.getRecords();


        //如果没有帖子数据内容设置为空
        if (allApprovedPost.size() == 0)
            return new BaseListResponse<ApprovedPost>(CommonProperties.OK.getCode(), CommonProperties.OK.getMessage(), new ArrayList<>());

        for (ApprovedPost approvedPost : allApprovedPost) {
            String postsId = approvedPost.getPostId();

            String postCover = approvedPost.getPostCover();
            Img cover = imgServiceInf.getById(postCover);
            //设置封面
            approvedPost.setCover(cover);

            List<Img> PostAllImg = imgServiceInf.getAllApprovedPostImg(postsId);
            approvedPost.setPostsImgContent(PostAllImg);

            UserInformation userInformation = postUserMapper.selectById(approvedPost.getUserId());
            approvedPost.setUserInformation(userInformation);
        }

        /**
         * 查询所有点赞、评论、收藏
         */

        return new BaseListResponse<ApprovedPost>(CommonProperties.OK.getCode(), CommonProperties.OK.getMessage(), allApprovedPost);
    }

    @Override
    @Transactional
    public BaseResponse UpdatePosts(String approvedPostid, ApprovedPost approvedPost, MultipartFile[] multipartFiles) {
        //更新帖子
        ApprovedPost oPost = getById(approvedPostid);
        if (oPost == null)
            return ResponseBuilder.fail(null);

        //更新图片的内容
        imgServiceInf.SaveOrUpdateApprovedPostImg(approvedPostid, multipartFiles);

        //更新帖子点赞的内容

        if (approvedPost.getPostContent() != null)
            oPost.setPostContent(approvedPost.getPostContent());

        if (approvedPost.getPostStatus() != null && (approvedPost.getPostStatus() == "1" || approvedPost.getPostStatus() == "0"))
            oPost.setPostStatus(oPost.getPostStatus());
        //设置更新时间
        oPost.setPostUpdateDate(new Date());
        saveOrUpdate(oPost);

        return ResponseBuilder.ok(null);
    }

    /**
     * 删除帖子
     *
     * @param approvedPostId
     * @return
     * @throws BaseException
     */
    @Override
    @Transactional
    public BaseResponse RemovePosts(String approvedPostId) throws BaseException {
        //删除图片内容
        try {
            imgServiceInf.RemoveImg(approvedPostId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(CommonProperties.FAIL);
        }
        //删除帖子的信息
        boolean falg = removeById(approvedPostId);
        if (falg)
            return ResponseBuilder.ok(null);
        else
            return ResponseBuilder.fail(null);
    }

    /**
     * 通过帖子的id来获取到帖子
     * @param userid 设查询这个帖子的信息
     * @param postid
     * @return
     * @throws BaseException
     */
    @Override
    public BaseResponse<ApprovedPost> getApprovedPost(String userid,String postid) throws BaseException {

        ApprovedPost approvedPost = baseMapper.selectById(postid);

        String postsId = approvedPost.getPostId();

        String postCover = approvedPost.getPostCover();
        Img cover = imgServiceInf.getById(postCover);
        //设置封面
        approvedPost.setCover(cover);

        List<Img> PostAllImg = imgServiceInf.getAllApprovedPostImg(postsId);
        approvedPost.setPostsImgContent(PostAllImg);

        UserInformation userInformation = postUserMapper.selectById(approvedPost.getUserId());

        approvedPost.setUserInformation(userInformation);
        //发送请求得到这个帖子的评论，点赞和收藏
        ResponseEntity<Comprehensive> response = restTemplate.getForEntity("http://localhost:5073/comprehensive/post/comprehensive/" + postsId + "/" + userid, Comprehensive.class);
        Comprehensive comprehensive = response.getBody();
        approvedPost.setComprehensive((Comprehensive) comprehensive);

        return ResponseBuilder.ok(approvedPost);
    }
}
