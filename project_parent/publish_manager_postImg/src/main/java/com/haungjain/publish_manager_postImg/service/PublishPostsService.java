package com.haungjain.publish_manager_postImg.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.properties.PostProperties;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BasePageListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.CommonService.ImgServiceInf;
import com.haungjain.project_interface.posts_data.ApprovedPostMapper;
import com.haungjain.project_interface.posts_data.PublishedPostsMapper;
import com.haungjain.project_interface.publish__manager_postingService.PublishPostsServiceInf;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.publish_manager_postImg.Img;
import com.haungjain.project_pojos.publish_manager_postImg.PublishedPosts;
import com.haungjain.project_utils.uuid.uuidUtil;
import com.haungjain.project_pojos.publish_manager_postImg.pojo.SearchParam;
import lombok.RequiredArgsConstructor;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PublishPostsService extends ServiceImpl<PublishedPostsMapper,PublishedPosts> implements PublishPostsServiceInf {

    @Autowired
    ImgServiceInf imgService;

    @Override
    @Transactional
    public BaseResponse publishPosts(PublishedPosts publishedPosts, MultipartFile[] multipartFiles,MultipartFile cover) throws BaseException {
        String coverid = "";
        if(multipartFiles==null || cover==null || multipartFiles.length==0){
            return ResponseBuilder.fail(null);
        }
        //保存封面
        if(cover!=null)
        {
            List<Img> imgs = imgService.SaveImage(new MultipartFile[]{cover});
            coverid=imgs.get(0).getImgId();
        }else if(multipartFiles!=null&&multipartFiles.length>0) {
            //使用图片内容的第一张做封面
            List<Img> imgs = imgService.SaveImage(new MultipartFile[]{multipartFiles[0]});
            coverid=imgs.get(0).getImgId();
        }else
            return ResponseBuilder.fail(null);

        //设置帖子的id
        publishedPosts.setPostsId(uuidUtil.getUUid());
        //设置帖子的封面
        publishedPosts.setPostCover(coverid);
        //正在审核中
        publishedPosts.setReviewProgress(PostProperties.REVIEW_PROGRESS_ONGOING);
        //设置时间
        publishedPosts.setPostPublishDate(new Date());
        publishedPosts.setPostUpdateDate(new Date());
        //将发布的帖子和用户进行绑定
        if (publishedPosts.getUserId() == null || publishedPosts.getUserId() == "")
            return ResponseBuilder.fail(null);
//        publishedPosts.setUserId("5ffbf723-ab7e-11ee-828e-0242ac11000a");
        //将帖子添加到数据库中
        saveOrUpdate(publishedPosts);
        //如果发布的帖子有图片的内容
        if(multipartFiles.length!=0)
            imgService.SaveImage(publishedPosts.getPostsId(),multipartFiles);

        return ResponseBuilder.ok(null);
    }

    @Override
    public BasePageListResponse<PublishedPosts> getAllPublishPost(SearchParam searchParam){
        if (searchParam.getCurrent() == null || searchParam.getPage_size() == null)
            return new BasePageListResponse<PublishedPosts>(CommonProperties.FAIL,null,0);
        QueryWrapper<PublishedPosts> queryWrapper = new QueryWrapper<>();
        if (searchParam.getSearch_key() != null) {
            queryWrapper.lambda().and(q->q.like(PublishedPosts::getPostName, searchParam.getSearch_key())
                    .or()
                    .like(PublishedPosts::getPostContent, searchParam.getSearch_key()));

        }

        if (searchParam.getPost_status() != null){
            queryWrapper.and(q->q.lambda().eq(PublishedPosts::getPostStatus,searchParam.getPost_status()));
        }

        if (searchParam.getStart_time() !=null && searchParam.getEnd_time() != null)
            queryWrapper.and(q->q.lambda().between(PublishedPosts::getPostPublishDate,searchParam.getStart_time(),searchParam.getEnd_time()));

        queryWrapper.last("limit "+(searchParam.getCurrent() - 1)*searchParam.getPage_size()+','+searchParam.getPage_size());

        List<PublishedPosts> list = this.list(queryWrapper);

        int sum = this.count();

        return new BasePageListResponse<PublishedPosts>(CommonProperties.OK,list,sum);
    }

    @Override
    public BaseResponse<PublishedPosts> getPublishPostById(String postId){
        PublishedPosts posts = this.getById(postId);

        String postCover = posts.getPostCover();
        Img cover = imgService.getById(postCover);
        //设置封面
        posts.setCoverImg(cover);

        List<Img> PostAllImg = imgService.getAllApprovedPostImg(postId);
        posts.setPostsImgContent(PostAllImg);

        return ResponseBuilder.ok(posts);
    }

    @Autowired
    private ApprovedPostMapper approvedPostMapper;

    @Transactional
    public BaseResponse publishPost(String postId) throws InvocationTargetException, IllegalAccessException {
        PublishedPosts post = this.getById(postId);
        ApprovedPost approvedPost = new ApprovedPost();
        BeanUtils.copyProperties(approvedPost,post);
        approvedPost.setPostId(post.getPostsId());
        post.setReviewProgress(0);
        this.saveOrUpdate(post);
        approvedPostMapper.insert(approvedPost);
        return ResponseBuilder.ok(null);
    }
}
