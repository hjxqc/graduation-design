package com.haungjain.publish_manager_postImg.controller;

import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BasePageListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.publish_manager_postImg.PublishedPosts;
import com.haungjain.project_pojos.publish_manager_postImg.pojo.SearchParam;
import com.haungjain.publish_manager_postImg.service.PublishPostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@RestController
@RequestMapping("/publishedpost")
public class PublishPostsController {

    @Autowired
    PublishPostsService publishPostsService;

    @PostMapping("/post/publish")
    public BaseResponse publishPost(PublishedPosts publishedPosts, MultipartFile[] multipartFiles,MultipartFile cover) throws BaseException {
        return publishPostsService.publishPosts(publishedPosts,multipartFiles,cover);
    }

    @GetMapping("/post/publish/list")
    public BasePageListResponse<PublishedPosts> getAllPublishPost(@RequestParam(required = false) String search_key,
                                                                  @RequestParam(required = false) String post_status,
                                                                  @RequestParam(required = false) String start_time,
                                                                  @RequestParam(required = false) String end_time,
                                                                  @RequestParam Integer current,
                                                                  @RequestParam Integer page_size) throws ParseException {
        Date sta_date = null;
        Date end_date = null;
        if (start_time != null && end_time != null){
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); // 设置时区为UTC
            sta_date = dateFormat.parse(start_time);
            end_date = dateFormat.parse(end_time);
        }
        return publishPostsService.getAllPublishPost(new SearchParam(search_key,post_status,sta_date,end_date,current,page_size));
    }

    @GetMapping("/publish/post/one/{postid}")
    public BaseResponse<PublishedPosts> getPublishPost(@PathVariable("postid") String postid){
        return publishPostsService.getPublishPostById(postid);
    }

    @PostMapping("/publishedpost/{postid}")
    public BaseResponse publishedPost(@PathVariable("postid") String postid) throws InvocationTargetException, IllegalAccessException {
        return publishPostsService.publishPost(postid);
    }
}
