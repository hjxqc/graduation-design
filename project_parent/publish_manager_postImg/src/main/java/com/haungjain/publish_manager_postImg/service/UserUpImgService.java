package com.haungjain.publish_manager_postImg.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.CommonService.ImgServiceInf;
import com.haungjain.project_interface.posts_data.UserUpImgMapper;
import com.haungjain.project_interface.publish__manager_postingService.UserUpImgServiceInf;
import com.haungjain.project_pojos.publish_manager_postImg.Img;
import com.haungjain.project_pojos.publish_manager_postImg.Location;
import com.haungjain.project_pojos.publish_manager_postImg.UserUpImg;
import com.haungjain.project_utils.uuid.uuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

@Service
public class UserUpImgService extends ServiceImpl<UserUpImgMapper, UserUpImg> implements UserUpImgServiceInf {

    @Autowired
    private ImgServiceInf imgServiceInf;

    /**
     * 支持批量或者单个的上传图片。
     * 如果在批量上传图片时设置状态的时候上传的所有图片都是这个状态，无法支持对单个图片进行调整
     * 但是如果只上传一个图片则就可以对单个图片进行调整
     * @param userId
     * @param multipartFiles
     * @param location
     * @param upImgStatus
     * @return
     * @throws BaseException
     */
    @Override
    @Transactional
    public BaseResponse upImg(String userId , MultipartFile[] multipartFiles , Location location , String upImgStatus) throws BaseException {
        //将图片保存到服务器中并且，返回数据库中的数据
        List<Img> imgs = imgServiceInf.SaveImage(multipartFiles);

        //将图片保存到用户中去
        for (Img img : imgs) {
            UserUpImg userUpImg = new UserUpImg();
            userUpImg.setImgId(img.getImgId());
            userUpImg.setUpImgStatus(upImgStatus);
            //json类型数据， //将图片放入指定位置（暂定）
            userUpImg.setUpImgid(uuidUtil.getUUid());
            userUpImg.setUpDate(new Date());
            userUpImg.setUserid(userId);
            saveOrUpdate(userUpImg);
        }
        return new BaseResponse(CommonProperties.OK.getCode(), CommonProperties.OK.getMessage(), null);
    }

    @Override
    @Transactional
    public BaseResponse RemoveUpImg(String userid ,String[] upImgIds,Location location) {

        //删除数据库中图片的信息、和图片中的图片的信息
        List<String> imgIds = baseMapper.getAllImgIdByUpImgIds(upImgIds);
        //删除图片信息
        boolean removeImgStatus = imgServiceInf.RemoveImg(imgIds);
        if(!removeImgStatus)
            return ResponseBuilder.fail(null);

        //删除用户上传照片的数据库表中的信息
        boolean removeUserUpImgStatus = baseMapper.removeUserUpImg(userid, upImgIds);
        if (!removeUserUpImgStatus)
            return ResponseBuilder.fail(null);

        return new BaseResponse(CommonProperties.OK.getCode(), CommonProperties.OK.getMessage(), null);
    }

    @Override
    public BaseResponse UpdateUpImg(String userid, String upImgId, String upImgStatus, Location location) {

        UserUpImg upImg = getById(upImgId);
        if(upImg == null)
            return ResponseBuilder.fail(null);

        if(!upImg.getUserid().equals(userid))
            return ResponseBuilder.fail(null);

        if(upImgStatus == null || (!upImgStatus.equals("1") && !upImgStatus.equals("0")))
            return ResponseBuilder.fail(null);
        upImg.setUpImgStatus(upImgStatus);
        upImg.setUpDate(new Date());
        //进行更新
        saveOrUpdate(upImg);
        //进行图片位置的更新
        return ResponseBuilder.ok(null);
    }

    @Override
    public BaseListResponse<UserUpImg> getAllUserUp(String userid) {
        QueryWrapper<UserUpImg> userUpImgQueryWrapper = new QueryWrapper<>();
        userUpImgQueryWrapper.eq("userid",userid);
        List<UserUpImg> userUpImgs = baseMapper.selectList(userUpImgQueryWrapper);
        for (UserUpImg userUpImg : userUpImgs) {
            String imgId = userUpImg.getImgId();
            Img img = imgServiceInf.getById(imgId);
            userUpImg.setImg(img);
        }

        BaseListResponse<UserUpImg> userUpImgBaseListResponse = new BaseListResponse<>();
        userUpImgBaseListResponse.setCode(CommonProperties.OK.getCode());
        userUpImgBaseListResponse.setMessage(CommonProperties.OK.getMessage());
        userUpImgBaseListResponse.setData(userUpImgs);
        return userUpImgBaseListResponse;
    }
}
