package com.haungjain.publish_manager_postImg.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_interface.publish__manager_postingService.ManagerPostsServiceInf;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.publish_manager_postImg.request.PostRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/managerpost")
public class ManagerPostsController {

    @Autowired
    ManagerPostsServiceInf managerPostsServiceInf;

    @GetMapping("/all/approved/post")
    public BaseListResponse<ApprovedPost> getAllApprovedPost(
            @RequestParam(value = "userid", required = false) String userId
            ,@RequestParam("current") Integer current
            ,@RequestParam("size") Integer size) throws BaseException {
        return managerPostsServiceInf.getAllApprovedPost(userId,new PostRequest(current,size));
    }

    @GetMapping("/all/approved/post/by/userids")
    public BaseListResponse<ApprovedPost> getAllApprovedPostByIds( @RequestParam(value = "userids") List<String> userIds
            ,@RequestParam("current") Integer current
            ,@RequestParam("size") Integer size) throws BaseException {
        return managerPostsServiceInf.getAllApprovedPost(userIds,new PostRequest(current,size));
    }

    @GetMapping("/all/approved/post/by/postname")
    public BaseListResponse<ApprovedPost> getAllApprovedPostByIds( @RequestParam(value = "postname") String postname
            ,@RequestParam("current") Integer current
            ,@RequestParam("size") Integer size) throws BaseException {
        return managerPostsServiceInf.searchAllApprovedPostByNanme(postname,new PostRequest(current,size));
    }

    @GetMapping("/approved/post/{postid}/{userid}")
    public BaseResponse<ApprovedPost> getApprovedPost(
            @PathVariable(value = "postid") String postid,
            @PathVariable(value = "userid") String userid) throws BaseException {
        return managerPostsServiceInf.getApprovedPost(userid,postid);
    }


    @PostMapping("/approved/post/updated/{postid}")
    public BaseResponse UpdatePosts(
            @PathVariable("postid") String approvedPostId
            ,ApprovedPost approvedPost,MultipartFile[] multipartFiles){
        return managerPostsServiceInf.UpdatePosts(approvedPostId,approvedPost,multipartFiles);
    }

    @PostMapping("/approved/post/deleted/{postsid}")
    public BaseResponse RemovePost(@PathVariable("postsid") String ApprovedPostId) throws BaseException {
        return managerPostsServiceInf.RemovePosts(ApprovedPostId);
    }
}
