package com.haungjain.publish_manager_postImg.controller;

import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_interface.publish__manager_postingService.UserUpImgServiceInf;
import com.haungjain.project_pojos.publish_manager_postImg.Location;
import com.haungjain.project_pojos.publish_manager_postImg.UserUpImg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/userupimg")
public class UserUpImgController {

    @Autowired
    private UserUpImgServiceInf userUpImgServiceInf;

    /**
     * 用户进行上传图片
     * @param userId
     * @param status
     * @param multipartFiles
     * @param location
     * @return
     * @throws BaseException
     */
    @PostMapping("/upimg/up/{userid}")
    public BaseResponse upImg(@PathVariable("userid") String userId , String status , MultipartFile[] multipartFiles , Location location) throws BaseException {
        return userUpImgServiceInf.upImg(userId,multipartFiles,location,status);
    }

    /**
     * 用户进行删除上转的图片
     * @param userid
     * @param upImgIds
     * @param location
     * @return
     */
    @PostMapping("/upimg/remove/{userid}")
    public BaseResponse RemoveUpImg(@PathVariable("userid") String userid , String[] upImgIds, Location location){
        return userUpImgServiceInf.RemoveUpImg(userid,upImgIds,location);
    }

    /**
     * 对图片上传的照片进行信息的更改
     * @param userid
     * @param upimgid
     * @param upImgStatus
     * @param location
     * @return
     */
    @PostMapping("/upimg/update/{userid}/{upimgid}")
    public BaseResponse UpdateUpImg(@PathVariable("userid") String userid,@PathVariable("upimgid") String upimgid , String upImgStatus , Location location){
        return userUpImgServiceInf.UpdateUpImg(userid,upimgid,upImgStatus,location);
    }

    @GetMapping("/upimg/all/{userid}")
    public BaseListResponse<UserUpImg> getAllUserUp(@PathVariable("userid") String userid){
        return userUpImgServiceInf.getAllUserUp(userid);
    }
}
