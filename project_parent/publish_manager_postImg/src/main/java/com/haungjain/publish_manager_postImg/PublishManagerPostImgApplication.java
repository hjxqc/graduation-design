package com.haungjain.publish_manager_postImg;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan(basePackages = "com.haungjain.project_interface.posts_data")
@ComponentScan(basePackages = {"com.haungjain.project_utils.ImgUtils","com.haungjain.project_utils.mybatisconf","com.haungjain.project_utils.network","com.haungjain.project_utils.uuid"})
@ComponentScan(basePackages = {"com.haungjain.publish_manager_postImg.controller","com.haungjain.publish_manager_postImg.service"})
@EnableTransactionManagement(proxyTargetClass = true)
public class PublishManagerPostImgApplication {
    public static void main(String[] args) {
        SpringApplication.run(PublishManagerPostImgApplication.class, args);
    }
}
