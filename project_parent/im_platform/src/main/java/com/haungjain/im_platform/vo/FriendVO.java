package com.haungjain.im_platform.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class FriendVO {

    @NotNull(message = "好友id不可为空")
    private String id;

    @NotNull(message = "好友昵称不可为空")
    private String nickName;


    private String headImage;
}
