package com.haungjain.im_platform.config.oauth2;

import com.haungjain.im_platform.config.oauth2.token_converter.CustomUseTokenConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
public class TokenConfig {
    //jwt密匙
    private static final String SIGNING_KEY = "permission_authentication";

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();

        //对称密匙来签署我们的令牌，资源服务器也将使用此密匙来保证验准确性
        jwtAccessTokenConverter.setSigningKey(SIGNING_KEY);
        CustomUseTokenConverter customUseTokenConverter = new CustomUseTokenConverter();
        DefaultAccessTokenConverter defaultAccessTokenConverter = new DefaultAccessTokenConverter();
        defaultAccessTokenConverter.setUserTokenConverter(customUseTokenConverter);
        jwtAccessTokenConverter.setAccessTokenConverter(defaultAccessTokenConverter);

        return jwtAccessTokenConverter;
    }

    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(jwtAccessTokenConverter());
    }
}
