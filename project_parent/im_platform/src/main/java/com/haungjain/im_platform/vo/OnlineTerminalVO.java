package com.haungjain.im_platform.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author: 谢绍许
 * @date: 2023-10-28 21:17:59
 * @version: 1.0
 */
@Data
@AllArgsConstructor
public class OnlineTerminalVO {

    private String userId;

    private List<Integer> terminals;

}
