package com.haungjain.im_platform.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.haungjain.im_platform.entity.Friend;
import com.haungjain.im_platform.vo.FriendVO;

import java.util.List;

public interface IFriendService extends IService<Friend> {

    /**
     * 判断用户2是否用户1的好友
     *
     * @param userId1 用户1的id
     * @param userId2 用户2的id
     * @return true/false
     */
    Boolean isFriend(String userId1, String userId2);


    /**
     * 查询用户的所有好友
     *
     * @param userId 用户id
     * @return 好友列表
     */
    List<Friend> findFriendByUserId(String userId);

    /**
     * 添加好友，互相建立好友关系
     *
     * @param friendId 好友的用户id
     */
    void addFriend(String friendId);

    /**
     * 删除好友，双方都会解除好友关系
     *
     * @param friendId 好友的用户id
     */
    void delFriend(String friendId);

    /**
     * 查询指定的某个好友信息
     *
     * @param friendId 好友的用户id
     * @return 好友信息
     */
    FriendVO findFriend(String friendId);

    void update(FriendVO vo);
}
