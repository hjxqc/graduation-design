package com.haungjain.im_platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haungjain.im_platform.entity.User;
import com.haungjain.im_platform.vo.OnlineTerminalVO;
import com.haungjain.im_platform.vo.UserSearchVo;
import com.haungjain.im_platform.vo.UserVO;
import com.haungjain.project_common.response.BasePageListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

public interface IUserService extends IService<User> {
    /**
     * 根据用户名查询用户
     *
     * @param username 用户名
     * @return 用户信息
     */
    User findUserByUserName(String username);

    /**
     * 根据用户昵id查询用户以及在线状态
     *
     * @param id 用户id
     * @return 用户信息
     */
    UserVO findUserById(String id);

    /**
     * 根据用户昵称查询用户，最多返回20条数据
     *
     * @param name    用户名或昵称
     * @param current
     * @param size
     * @return 用户列表
     */
    List<UserSearchVo> findUserByName(String name, Integer current, Integer size);

    /**
     * 获取用户在线的终端类型
     *
     * @param userIds 用户id，多个用‘,’分割
     * @return 在线用户终端
     */
    List<OnlineTerminalVO> getOnlineTerminals(String userIds);


    BasePageListResponse<AccountInformation> getAccountInformationByParams(Map<String, Object> map);

    BaseResponse<User> getUserInformationById(@PathVariable("userid") String userid);

    BaseResponse updateInformation(Map<String,Object> map);

    BaseResponse deleteInformation(Map<String, Object> map);
}
