package com.haungjain.im_platform.listener;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haungjain.im_client.annotation.IMListener;
import com.haungjain.im_client.listener.MessageListener;
import com.haungjain.im_common.enums.IMListenerType;
import com.haungjain.im_common.enums.IMSendCode;
import com.haungjain.im_common.model.IMSendResult;
import com.haungjain.im_platform.entity.PrivateMessage;
import com.haungjain.im_platform.enums.MessageStatus;
import com.haungjain.im_platform.service.IPrivateMessageService;
import com.haungjain.im_platform.vo.PrivateMessageVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.HashSet;
import java.util.List;

@Slf4j
@IMListener(type = IMListenerType.PRIVATE_MESSAGE)
public class PrivateMessageListener implements MessageListener<PrivateMessageVO> {

    @Lazy
    @Autowired
    private IPrivateMessageService privateMessageService;

    @Override
    public void process(List<IMSendResult<PrivateMessageVO>> results) {
        HashSet<Long> messageIds = new HashSet<>();
        for (IMSendResult<PrivateMessageVO> result : results) {
            PrivateMessageVO messageVO = result.getData();
            //更新消息状态，这里只处理成消息，失败的消息保持未读状态
            if(result.getCode() == IMSendCode.SUCCESS.code()){
                messageIds.add(messageVO.getId());
                log.info("消息送达，消息id：{}，发送者：{}，接收者：{}，终端：{}",messageVO.getId(),result.getSender().getId(),result.getReceiver().getId(),result.getReceiver().getTerminal());
            }
        }
        //批量修改状态
        if(CollUtil.isNotEmpty(messageIds)){
            UpdateWrapper<PrivateMessage> updateWrapper = new UpdateWrapper<>();
            updateWrapper.lambda().in(PrivateMessage::getId,messageIds)
                    .eq(PrivateMessage::getStatus, MessageStatus.UNSEND.code())
                    .set(PrivateMessage::getStatus,MessageStatus.SENDED.code());
            privateMessageService.update(updateWrapper);//更新发送成功但未送达的消息的状态变成未已读的状态
        }
    }
}
