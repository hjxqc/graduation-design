package com.haungjain.im_platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;

public interface AccountInformationMapper extends BaseMapper<AccountInformation> {
}
