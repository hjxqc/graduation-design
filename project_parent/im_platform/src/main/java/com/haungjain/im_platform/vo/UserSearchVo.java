package com.haungjain.im_platform.vo;

import lombok.Data;

@Data
public class UserSearchVo {
    UserVO userVO;

    //是否是朋友
    Boolean isFriend;
}
