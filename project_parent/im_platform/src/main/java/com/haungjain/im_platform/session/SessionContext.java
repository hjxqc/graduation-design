package com.haungjain.im_platform.session;

import com.alibaba.fastjson.JSONObject;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/*
 * @Description
 * @Author Blue
 * @Date 2022/10/21
 */
public class SessionContext {

    public static UserSession getSession(Authentication authentication) {
        // 从请求上下文里获取Request对象
        Object principal = authentication.getPrincipal();
        AccountInformation accountInformation = JSONObject.parseObject((String) principal, AccountInformation.class);
        UserSession userSession = new UserSession();
        userSession.setUserName(accountInformation.getUserName());
        userSession.setUserId(accountInformation.getUserId());
        userSession.setTerminal(accountInformation.getTerminal());
        return userSession;
    }

}
