package com.haungjain.im_platform.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.haungjain.im_platform.entity.PrivateMessage;
import com.haungjain.im_platform.mapper.PrivateMessageMapper;
import com.haungjain.im_platform.service.IPrivateMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrivateMessageUpdateService {

    @Autowired
    private PrivateMessageMapper privateMessageMapper;

    public void delAllMessageByUserId(String userId, String friendId){
        //删除选定用户所有的消息
        LambdaQueryWrapper<PrivateMessage> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(PrivateMessage::getSendId,userId);
        lambdaQueryWrapper.eq(PrivateMessage::getRecvId,friendId);
        privateMessageMapper.delete(lambdaQueryWrapper);
    }
}
