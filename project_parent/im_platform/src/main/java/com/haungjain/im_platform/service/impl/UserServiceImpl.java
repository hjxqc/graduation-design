package com.haungjain.im_platform.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.im_client.IMClient;
import com.haungjain.im_common.enums.IMTerminalType;
import com.haungjain.im_platform.entity.User;
import com.haungjain.im_platform.mapper.AccountInformationMapper;
import com.haungjain.im_platform.mapper.UserMapper;
import com.haungjain.im_platform.service.IFriendService;
import com.haungjain.im_platform.service.IUserService;
import com.haungjain.im_platform.session.SessionContext;
import com.haungjain.im_platform.session.UserSession;
import com.haungjain.im_platform.util.BeanUtils;
import com.haungjain.im_platform.vo.FriendVO;
import com.haungjain.im_platform.vo.OnlineTerminalVO;
import com.haungjain.im_platform.vo.UserSearchVo;
import com.haungjain.im_platform.vo.UserVO;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.haungjain.project_common.response.BasePageListResponse;

import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements IUserService {

    private final IMClient imClient;

    /**
     * 根据用户名查找用户信息
     * @param username 用户名
     * @return
     */
    @Override
    public User findUserByUserName(String username) {
        LambdaQueryWrapper<User> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(User::getUserName,username);
        return this.getOne(queryWrapper);
    }

    @Override
    public UserVO findUserById(String id) {
        User user = this.getById(id);
        UserVO userVO = BeanUtils.copyProperties(user, UserVO.class);
        userVO.setOnline(imClient.isOnline(id));
        return userVO;
    }

    private final IFriendService friendService;

    /**
     * 根据名字进行模糊查询匹配用户
     *
     * @param name    用户名或昵称
     * @param current
     * @param size
     * @return
     */
    @Override
    public List<UserSearchVo> findUserByName(String name, Integer current, Integer size) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        LambdaQueryWrapper<User> queryWrapper = Wrappers.lambdaQuery();
        String sqlstr = "limit " + (current - 1) * size + "," + size;
        queryWrapper.like(User::getUserName,name).last(sqlstr);
        List<User> users = this.list(queryWrapper);
        List<String> userIds = users.stream().map(User::getId).collect(Collectors.toList());
        List<String> onlineUserIds = imClient.getOnlineUser(userIds);
        return users.stream().map(u -> {
            UserVO vo = BeanUtils.copyProperties(u, UserVO.class);
            vo.setOnline(onlineUserIds.contains(u.getId()));
            Boolean isFriend = friendService.isFriend(session.getUserId(), vo.getId());
            UserSearchVo userSearchVo = new UserSearchVo();
            userSearchVo.setUserVO(vo);
            userSearchVo.setIsFriend(isFriend);
            return userSearchVo;
        }).collect(Collectors.toList());
    }

    @Override
    public List<OnlineTerminalVO> getOnlineTerminals(String userIds) {
        List<String> userIdList = Arrays.stream(userIds.split(",")).collect(Collectors.toList());
        //查询在线的终端
        Map<String, List<IMTerminalType>> onlineTerminal = imClient.getOnlineTerminal(userIdList);
        //组装vo
        LinkedList<OnlineTerminalVO> vos = new LinkedList<>();
        onlineTerminal.forEach((userId,types)->{
            List<Integer> terminals = types.stream().map(IMTerminalType::code).collect(Collectors.toList());
            vos.add(new OnlineTerminalVO(userId,terminals));
        });
        return vos;
    }

    @Autowired
    private AccountInformationMapper accountInformationMapper;

    @Override
    public BasePageListResponse<AccountInformation> getAccountInformationByParams(Map<String, Object> map){

        if (map.get("current") == null || map.get("page_size") == null)
            return new BasePageListResponse(CommonProperties.FAIL,null,0);

        LambdaQueryWrapper<AccountInformation> queryWrapper = new LambdaQueryWrapper<>();

        //按用户名模糊查询
        if (map.get("userName") !=null ){
            queryWrapper.and(q->q.like(AccountInformation::getUserName,map.get("userName")));
        }

        //根据手机号模糊查询
        if (map.get("mobile") != null){
            queryWrapper.and(q->q.like(AccountInformation::getMobile,map.get("mobile")));
        }

        if (map.get("mail") !=null){
            queryWrapper.and(q->q.like(AccountInformation::getMail,map.get("mail")));
        }

        if (
                map.get("accountNonExpired") !=null
                        || map.get("accountNonLocked") != null
                        || map.get("credentialsNonExpired") != null
                        || map.get("enabled") != null
        ){
            queryWrapper.and(q -> {
                q.eq(AccountInformation::isAccountNonExpired,map.get("accountNonExpired"))
                        .eq(AccountInformation::isAccountNonLocked,map.get("accountNonLocked"))
                        .eq(AccountInformation::isCredentialsNonExpired,map.get("credentialsNonExpired"))
                        .eq(AccountInformation::isEnabled,map.get("enabled"));
            });
        }

        if (map.get("type") != null){
            queryWrapper.and(q-> {
                List<User> users = this.list(new QueryWrapper<User>().lambda().eq(User::getType, map.get("type")));
                List<String> userids = users.stream().map(u -> u.getId()).collect(Collectors.toList());
                if (userids.size()>0)
                    q.in(AccountInformation::getUserId, userids);
            });
        }

        Integer current = (Integer) map.get("current");
        Integer page_size = (Integer) map.get("page_size");
        queryWrapper.last("limit " + (current - 1) * page_size + "," + page_size);

        Integer sum = accountInformationMapper.selectCount(new QueryWrapper<AccountInformation>());
        List<AccountInformation> accountInformations = accountInformationMapper.selectList(queryWrapper);

        return new BasePageListResponse<AccountInformation>(CommonProperties.OK,accountInformations,sum);
    }

    @Override
    public BaseResponse<User> getUserInformationById(@PathVariable("userid") String userid){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        User userInformation = this.getById(userid);

        if (userInformation == null)
            return ResponseBuilder.fail(null);

        return ResponseBuilder.ok(userInformation);
    }

    @Transactional
    @Override
    public BaseResponse updateInformation(Map<String,Object> map){
        String accountInformation = (String)map.get("user_account");
        String userInformation = (String)map.get("user_info");

        if (accountInformation == null || userInformation == null)
            return ResponseBuilder.fail(null);
        AccountInformation acc = JSON.parseObject(accountInformation,AccountInformation.class);
        accountInformationMapper.updateById(acc);
        User ui = JSON.parseObject(userInformation,User.class);
        this.updateById(ui);

        FriendVO friendVO = new FriendVO();
        friendVO.setNickName(ui.getUserName());
        friendVO.setHeadImage(ui.getHeadImage());
        friendService.update(friendVO);
        return ResponseBuilder.ok(null);
    }


    @Override
    public BaseResponse deleteInformation(Map<String, Object> map){
        String accountid = (String)map.get("accountid");
        String userInfoid = (String)map.get("userInfoid");

        if (accountid == null || userInfoid == null)
            return ResponseBuilder.fail(null);

        accountInformationMapper.deleteById(accountid);

        this.removeById(userInfoid);
        return ResponseBuilder.ok(null);
    }
}
