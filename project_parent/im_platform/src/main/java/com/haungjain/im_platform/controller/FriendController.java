package com.haungjain.im_platform.controller;

import com.haungjain.im_platform.entity.Friend;
import com.haungjain.im_platform.result.Result;
import com.haungjain.im_platform.result.ResultUtils;
import com.haungjain.im_platform.service.IFriendService;
import com.haungjain.im_platform.session.SessionContext;
import com.haungjain.im_platform.session.UserSession;
import com.haungjain.im_platform.vo.FriendVO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/friend")
@RestController
@RequiredArgsConstructor
public class FriendController {

    private final IFriendService friendService;

    @GetMapping("/list")
    public Result<List<FriendVO>> findFriends(){
        //找到用户的所有的好友
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        List<Friend> friends = friendService.findFriendByUserId(session.getUserId());
        List<FriendVO> vos = friends.stream().map(f -> {
            FriendVO friendVO = new FriendVO();
            friendVO.setId(f.getFriendId());
            friendVO.setHeadImage(f.getFriendHeadImage());
            friendVO.setNickName(f.getFriendNickName());
            return friendVO;
        }).collect(Collectors.toList());
        return ResultUtils.success(vos);
    }

    @PostMapping("/add")
    public Result addFriend(@NotEmpty(message = "好友id不能为空") @RequestParam("friendId") String frindId){
        friendService.addFriend(frindId);
        return ResultUtils.success();
    }

    @GetMapping("/find/{friendId}")
    public Result<FriendVO> findFriend(@NotEmpty(message = "好友id不可为空") @PathVariable("friendId") String friendId){
        return ResultUtils.success(friendService.findFriend(friendId));
    }

    @PostMapping("/delete/{friendId}")
    public Result delFriend(@NotEmpty(message = "好友id不可为空") @PathVariable("friendId") String friendId){
        friendService.delFriend(friendId);
        return ResultUtils.success();
    }
}
