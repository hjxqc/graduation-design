package com.haungjain.im_platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.im_platform.entity.PrivateMessage;

public interface PrivateMessageMapper extends BaseMapper<PrivateMessage> {

}
