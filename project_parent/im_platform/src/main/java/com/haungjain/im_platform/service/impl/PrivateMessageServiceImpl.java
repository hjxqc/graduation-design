package com.haungjain.im_platform.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.im_client.IMClient;
import com.haungjain.im_common.contant.IMConstant;
import com.haungjain.im_common.enums.IMTerminalType;
import com.haungjain.im_common.model.IMPrivateMessage;
import com.haungjain.im_common.model.IMUserInfo;
import com.haungjain.im_platform.dto.PrivateMessageDTO;
import com.haungjain.im_platform.entity.Friend;
import com.haungjain.im_platform.entity.PrivateMessage;
import com.haungjain.im_platform.enums.MessageStatus;
import com.haungjain.im_platform.enums.MessageType;
import com.haungjain.im_platform.enums.ResultCode;
import com.haungjain.im_platform.exception.GlobalException;
import com.haungjain.im_platform.mapper.PrivateMessageMapper;
import com.haungjain.im_platform.service.IFriendService;
import com.haungjain.im_platform.service.IPrivateMessageService;
import com.haungjain.im_platform.session.SessionContext;
import com.haungjain.im_platform.session.UserSession;
import com.haungjain.im_platform.util.BeanUtils;
import com.haungjain.im_platform.util.SensitiveFilterUtil;
import com.haungjain.im_platform.vo.PrivateMessageVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PrivateMessageServiceImpl extends ServiceImpl<PrivateMessageMapper, PrivateMessage> implements IPrivateMessageService {

    private final IFriendService friendService;

    private final IMClient imClient;

    private final SensitiveFilterUtil sensitiveFilterUtil;

    @Override
    public Long sendMessage(PrivateMessageDTO dto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        Boolean isFriend = friendService.isFriend(session.getUserId(), dto.getRecvId());
        if (Boolean.FALSE.equals(isFriend))
            throw new GlobalException(ResultCode.PROGRAM_ERROR,"您已不是对方好友无法发送消息");
        //保存消息
        PrivateMessage msg = BeanUtils.copyProperties(dto, PrivateMessage.class);
        msg.setSendId(session.getUserId());
        msg.setStatus(MessageStatus.UNSEND.code());
        msg.setSendTime(new Date());
        this.save(msg);
        //过滤消息内容
        String content = sensitiveFilterUtil.filter(dto.getContent());
        msg.setContent(content);
        //推送消息
        PrivateMessageVO messageInfo = BeanUtils.copyProperties(msg, PrivateMessageVO.class);
        IMPrivateMessage<PrivateMessageVO> sendMessage = new IMPrivateMessage<>();
        sendMessage.setSender(new IMUserInfo(session.getUserId(),session.getTerminal()));
        sendMessage.setRecvId(messageInfo.getRecvId());
        sendMessage.setSendToSelf(true);
        sendMessage.setData(messageInfo);
        sendMessage.setSendResult(true);
        imClient.sendPrivateMessage(sendMessage);
        log.info("发送私聊消息，发送id：{}，接收id：{}，内容：{}",session.getUserId(),dto.getRecvId(),dto.getContent());
        return msg.getId();
    }

    @Override
    public void recallMessage(Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        PrivateMessage msg = this.getById(id);
        if (Objects.isNull(msg))
            throw new GlobalException(ResultCode.PROGRAM_ERROR,"消息不存在");

        if (!msg.getSendId().equals(session.getUserId()))
            throw new GlobalException(ResultCode.PROGRAM_ERROR,"这条消息不是你发送的，无法撤回");

        if (System.currentTimeMillis() - msg.getSendTime().getTime() > IMConstant.ALLOW_RECALL_SECOND * 1000)
            throw new GlobalException(ResultCode.PROGRAM_ERROR,"消息已经发送了五分钟，无法撤回");
        //修改消息状态
        msg.setStatus(MessageStatus.RECALL.code());
        this.updateById(msg);
        //推送消息
        PrivateMessageVO msgInfo = BeanUtils.copyProperties(msg, PrivateMessageVO.class);
        msgInfo.setType(MessageType.RECALL.code());
        msgInfo.setSendTime(new Date());
        msgInfo.setContent("对方撤回了一条消息");

        IMPrivateMessage<PrivateMessageVO> sendMessage = new IMPrivateMessage<>();
        sendMessage.setSender(new IMUserInfo(session.getUserId(),session.getTerminal()));
        sendMessage.setRecvId(msgInfo.getRecvId());
        sendMessage.setSendToSelf(false);
        sendMessage.setData(msgInfo);
        sendMessage.setSendResult(false);
        imClient.sendPrivateMessage(sendMessage);

        //推送给自己的其他的终端
        msg.setContent("你撤回了一条消息");
        sendMessage.setSendToSelf(true);
        //接收者接收的终端为空说明只有自己能够接收到消息
        sendMessage.setRecvTerminals(Collections.emptyList());
        imClient.sendPrivateMessage(sendMessage);
        log.info("撤回私聊消息，发送id：{}，接收消息：{}，内容：{}",session.getUserId(),msg.getRecvId(),msg.getContent());
    }

    @Override
    public List<PrivateMessageVO> findHistoryMessage(String friendId, Long page, Long size) {
        page = page > 0 ? page : 1;
        size = size > 0 ? size : 10;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        String userId = session.getUserId();
        long stIdx = (page - 1) * size;
        QueryWrapper<PrivateMessage> wrapper = new QueryWrapper<>();
        wrapper.lambda().and(
                wrap -> wrap
                        .and(wp->wp.eq(PrivateMessage::getSendId,userId).eq(PrivateMessage::getRecvId,friendId))
                        .or(wp -> wp.eq(PrivateMessage::getRecvId,userId).eq(PrivateMessage::getSendId,friendId))
                )
                .ne(PrivateMessage::getStatus,MessageStatus.RECALL.code())
                .orderByDesc(PrivateMessage::getId) //降序
                .last("limit " + stIdx + "," + size);

        List<PrivateMessage> mesasges = this.list(wrapper);
        List<PrivateMessageVO> mesageInfos = mesasges.stream().map(m -> BeanUtils.copyProperties(m, PrivateMessageVO.class)).collect(Collectors.toList());
        log.info("拉取聊天记录，用户id：{},好友id：{}，数量：{}",userId,friendId,mesageInfos.size());
        return mesageInfos;
    }

    /**
     * 拉取消息，只能拉取最近1个月的消息，一次拉取100条
     *
     * @param minId 消息起始id
     * @return 聊天消息列表
     */
    @Override
    public List<PrivateMessageVO> loadMessage(Long minId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        List<Friend> friends = friendService.findFriendByUserId(session.getUserId());
        if (friends.isEmpty())
            return new ArrayList<>();

        List<String> friendIds = friends.stream().map(Friend::getFriendId).collect(Collectors.toList());
        //获取当前用户的消息
        LambdaQueryWrapper<PrivateMessage> queryWrapper = Wrappers.lambdaQuery();
        Date mindate = DateUtils.addMonths(new Date(), -1);
        //只能拉取最近一个月的
        queryWrapper.gt(PrivateMessage::getId,minId) //优化过的分页查询
                .ge(PrivateMessage::getSendTime,mindate)
                .ne(PrivateMessage::getStatus,MessageStatus.RECALL.code())
                .and(
                        wrap -> wrap
                                .and(wp -> wp.eq(PrivateMessage::getSendId,session.getUserId()).in(PrivateMessage::getRecvId,friendIds))
                                .or(wp -> wp.eq(PrivateMessage::getRecvId,session.getUserId()).in(PrivateMessage::getSendId,friendIds))
                )
                .orderByAsc(PrivateMessage::getId)
                .last("limit 100");

        List<PrivateMessage> privateMessages = this.list(queryWrapper);
        List<Long> ids = privateMessages.stream()
                .filter(m -> !m.getSendId().equals(session.getUserId()) && m.getStatus().equals(MessageStatus.  UNSEND.code()))
                .map(PrivateMessage::getId)
                .collect(Collectors.toList());

        if (!ids.isEmpty()){
            LambdaUpdateWrapper<PrivateMessage>  updateWrapper = Wrappers.lambdaUpdate();
            updateWrapper.in(PrivateMessage::getId,ids)
                    .set(PrivateMessage::getStatus,MessageStatus.SENDED.code());
            //更新消息状态
            this.update(queryWrapper);
        }
        log.info("拉取消息，用户id：{}，数量：{}",session.getUserId(),privateMessages.size());
        return privateMessages.stream().map(m -> BeanUtils.copyProperties(m,PrivateMessageVO.class)).collect(Collectors.toList());
    }

    @Override
    public void pullOfflineMessage(Long minId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        if (!imClient.isOnline(session.getUserId()))
            throw new GlobalException(ResultCode.PROGRAM_ERROR,"网络连接失败没无法拉取离线消息");

        //查询好友列表
        List<Friend> friends = friendService.findFriendByUserId(session.getUserId());
        if (friends.isEmpty())
            return;
        //开启加载中标志
        this.sendLoadingMessage(true);
        List<String> friendIds = friends.stream().map(Friend::getFriendId).collect(Collectors.toList());
        // 获取当前用户的消息
        LambdaQueryWrapper<PrivateMessage> queryWrapper = Wrappers.lambdaQuery();
        // 只能拉取最近1个月的1000条消息
        Date minDate = DateUtils.addMonths(new Date(), -1);
        queryWrapper.gt(PrivateMessage::getId, minId)
                .ge(PrivateMessage::getSendTime, minDate)
                .ne(PrivateMessage::getStatus, MessageStatus.RECALL.code())
                .and(wrap -> wrap
                        .and(wp -> wp.eq(PrivateMessage::getSendId, session.getUserId()).in(PrivateMessage::getRecvId, friendIds))
                        .or(wp -> wp.eq(PrivateMessage::getRecvId, session.getUserId()).in(PrivateMessage::getSendId, friendIds)))
                .orderByDesc(PrivateMessage::getId)
                .last("limit 1000");
        List<PrivateMessage> messages = this.list(queryWrapper);
        // 消息顺序从小到大
        CollectionUtil.reverse(messages);
        // 推送消息
        for(PrivateMessage m:messages ){
            PrivateMessageVO vo = BeanUtils.copyProperties(m, PrivateMessageVO.class);
            IMPrivateMessage<PrivateMessageVO> sendMessage = new IMPrivateMessage<>();
            sendMessage.setSender(new IMUserInfo(m.getSendId(), IMTerminalType.WEB.code()));
            sendMessage.setRecvId(session.getUserId());
            sendMessage.setRecvTerminals(Arrays.asList(session.getTerminal()));
            sendMessage.setSendToSelf(false);
            sendMessage.setData(vo);
            sendMessage.setSendResult(true);
            imClient.sendPrivateMessage(sendMessage);
        }
        // 关闭加载中标志
        this.sendLoadingMessage(false);
        log.info("拉取私聊消息，用户id:{},数量:{}", session.getUserId(), messages.size());
    }

    private void sendLoadingMessage(Boolean loadding){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        PrivateMessageVO msgInfo = new PrivateMessageVO();
        msgInfo.setType(MessageType.LOADDING.code());
        msgInfo.setContent(loadding.toString());
        IMPrivateMessage sendMessage = new IMPrivateMessage();
        sendMessage.setSender(new IMUserInfo(session.getUserId(),session.getTerminal()));
        sendMessage.setRecvId(session.getUserId());
        sendMessage.setRecvTerminals(Arrays.asList(session.getTerminal()));
        sendMessage.setData(msgInfo);
        sendMessage.setSendResult(false);
        sendMessage.setSendToSelf(false);
        imClient.sendPrivateMessage(sendMessage);
    }

    @Transactional
    @Override
    public void readedMessage(String friendId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        // 推送消息给自己，清空会话列表上的已读数量
        PrivateMessageVO msgInfo = new PrivateMessageVO();
        msgInfo.setType(MessageType.READED.code());
        msgInfo.setSendId(session.getUserId());
        msgInfo.setRecvId(friendId);
        IMPrivateMessage<PrivateMessageVO> sendMessage = new IMPrivateMessage<>();
        sendMessage.setData(msgInfo);
        sendMessage.setSender(new IMUserInfo(session.getUserId(), session.getTerminal()));
        sendMessage.setRecvId(session.getUserId());
        sendMessage.setSendToSelf(false);
        sendMessage.setSendResult(false);
        imClient.sendPrivateMessage(sendMessage);
        // 推送回执消息给对方，更新已读状态
        msgInfo = new PrivateMessageVO();
        msgInfo.setType(MessageType.RECEIPT.code());
        msgInfo.setSendId(session.getUserId());
        msgInfo.setRecvId(friendId);
        sendMessage = new IMPrivateMessage<>();
        sendMessage.setSender(new IMUserInfo(session.getUserId(), session.getTerminal()));
        sendMessage.setRecvId(friendId);
        sendMessage.setSendToSelf(false);
        sendMessage.setSendResult(false);
        sendMessage.setData(msgInfo);
        imClient.sendPrivateMessage(sendMessage);
        // 修改消息状态为已读
        LambdaUpdateWrapper<PrivateMessage> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.eq(PrivateMessage::getSendId, friendId)
                .eq(PrivateMessage::getRecvId, session.getUserId())
                .eq(PrivateMessage::getStatus, MessageStatus.SENDED.code())
                .set(PrivateMessage::getStatus, MessageStatus.READED.code());
        this.update(updateWrapper);
        log.info("消息已读，接收方id:{},发送方id:{}", session.getUserId(), friendId);
    }

    @Override
    public Long getMaxReadedId(String friendId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        LambdaQueryWrapper<PrivateMessage> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(PrivateMessage::getSendId,friendId)
                .eq(PrivateMessage::getRecvId,session.getUserId())
                .eq(PrivateMessage::getStatus,MessageStatus.READED.code())
                .orderByDesc(PrivateMessage::getId)
                .select(PrivateMessage::getId)
                .last("limit 1");
        PrivateMessage message = this.getOne(queryWrapper);
        if (Objects.isNull(message))
            return -1L;
        return message.getId();
    }
}
