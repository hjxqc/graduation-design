package com.haungjain.im_platform.controller;

import com.haungjain.im_platform.entity.User;
import com.haungjain.im_platform.result.Result;
import com.haungjain.im_platform.result.ResultUtils;
import com.haungjain.im_platform.service.IUserService;
import com.haungjain.im_platform.session.SessionContext;
import com.haungjain.im_platform.session.UserSession;
import com.haungjain.im_platform.util.BeanUtils;
import com.haungjain.im_platform.vo.OnlineTerminalVO;
import com.haungjain.im_platform.vo.UserSearchVo;
import com.haungjain.im_platform.vo.UserVO;
import com.haungjain.project_common.response.BasePageListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final IUserService userService;

    @GetMapping("/terminal/online")
    public Result<List<OnlineTerminalVO>> getOnlineTerminal(@NotEmpty @RequestParam("userIds") String userIds){
        return ResultUtils.success(userService.getOnlineTerminals(userIds));
    }

    @GetMapping("/self")
    private Result<UserVO> findSelfInfo(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        User user = userService.getById(session.getUserId());
        UserVO userVO = BeanUtils.copyProperties(user, UserVO.class);
        return ResultUtils.success(userVO);
    }

    @GetMapping("/find/{id}")
    private Result<UserVO> findById(@NotEmpty @PathVariable("id")String id){
        return ResultUtils.success(userService.findUserById(id));
    }

    @GetMapping("/findByName")
    public Result<List<UserSearchVo>> findByName(@RequestParam("name") String name,
                                                 @RequestParam("current") Integer current,
                                                 @RequestParam("size") Integer size){
        return ResultUtils.success(userService.findUserByName(name, current, size));
    }

    @PostMapping("/all/accountinformation")
    public BasePageListResponse<AccountInformation> getAccountInformationByParams(@RequestBody Map<String, Object> map){
        return userService.getAccountInformationByParams(map);
    }

    @GetMapping("/usernformation/{userid}")
    public BaseResponse<User> getUserInformationById(@PathVariable("userid") String userid){
        return userService.getUserInformationById(userid);
    }

    @PostMapping("/account/userinfo/update")
    public BaseResponse updateInformation(@RequestBody Map<String,Object> map){
        return userService.updateInformation(map);
    }

    @PostMapping("/account/userinfo/delete")
    public BaseResponse deleteInformation(@RequestBody Map<String,Object> map){
        return userService.deleteInformation(map);
    }
}
