package com.haungjain.im_platform.controller;

import com.haungjain.im_platform.dto.PrivateMessageDTO;
import com.haungjain.im_platform.result.Result;
import com.haungjain.im_platform.result.ResultUtils;
import com.haungjain.im_platform.service.IPrivateMessageService;
import com.haungjain.im_platform.vo.PrivateMessageVO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/message/private")
@RequiredArgsConstructor
public class PrivateMessageController {

    private final IPrivateMessageService privateMessageService;

    @PostMapping("/send")
    public Result<Long> sendMessage(@Valid @RequestBody PrivateMessageDTO dto){
        return ResultUtils.success(privateMessageService.sendMessage(dto));
    }

    @PostMapping("/recall/{id}")
    public Result<Long> recallMessage(@NotNull(message = "消息id不能为空") @PathVariable Long id){
        privateMessageService.recallMessage(id);
        return ResultUtils.success();
    }

    @GetMapping("/loadMessage")
    public Result<List<PrivateMessageVO>> loadMessage(@RequestParam Long minId){
        return ResultUtils.success(privateMessageService.loadMessage(minId));
    }

    @GetMapping("pullOfflineMessage")
    public Result pullOfflineMessage(@RequestParam Long minId){
        privateMessageService.pullOfflineMessage(minId);
        return ResultUtils.success();
    }

    @PostMapping("/readed")
    public Result readedMessage(@RequestParam String friendId){
        privateMessageService.readedMessage(friendId);
        return ResultUtils.success();
    }

    @GetMapping("/maxReadedId")
    public Result<Long> getMaxReadedId(@RequestParam String friendId){
        return ResultUtils.success(privateMessageService.getMaxReadedId(friendId));
    }

    @GetMapping("/history")
    public Result<List<PrivateMessageVO>> findHistoryMessage(@NotNull(message = "好友id不能为空") @RequestParam String friendId,
                                                             @NotNull(message = "页码不能为空") @RequestParam Long page,
                                                             @NotNull(message = "size不能为空") @RequestParam Long size){
        return ResultUtils.success(privateMessageService.findHistoryMessage(friendId,page,size));
    }
}
