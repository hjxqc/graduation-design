package com.haungjain.im_platform;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@Slf4j
@MapperScan(basePackages = {"com.haungjain.im_platform.mapper"})
@EnableAspectJAutoProxy(exposeProxy = true)
@EnableResourceServer
public class IMPlatformApp {
    public static void main(String[] args) {
        SpringApplication.run(IMPlatformApp.class,args);
    }
}
