package com.haungjain.im_platform.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.im_platform.entity.Friend;

public interface FriendMapper extends BaseMapper<Friend> {

}
