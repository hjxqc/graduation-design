package com.haungjain.im_platform.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class PrivateMessageDTO {
    @NotNull(message = "接收用户id不可为空")
    private String recvId;


    @Length(max = 1024, message = "内容长度不得大于1024")
    @NotEmpty(message = "发送内容不可为空")
    private String content;

    @NotNull(message = "消息类型不可为空")
    private Integer type;
}
