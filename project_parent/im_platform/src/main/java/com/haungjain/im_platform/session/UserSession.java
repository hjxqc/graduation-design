package com.haungjain.im_platform.session;

import com.haungjain.im_common.model.IMSessionInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@RequiredArgsConstructor
public class UserSession extends IMSessionInfo {

    /**
     * 用户名称
     */
    private String userName;
}
