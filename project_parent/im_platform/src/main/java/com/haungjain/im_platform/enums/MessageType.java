package com.haungjain.im_platform.enums;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum MessageType {
    /**
     * 文字
     */
    TEXT(0, "文字"),
    /**
     * 撤回
     */
    RECALL(10, "撤回"),
    /**
     * 已读
     */
    READED(11, "已读"),

    /**
     * 消息已读回执
     */
    RECEIPT(12, "消息已读回执"),
    /**
     * 时间提示
     */
    TIP_TIME(20,"时间提示"),
    /**
     * 文字提示
     */
    TIP_TEXT(21,"文字提示"),

    /**
     * 消息加载标记
     */
    LOADDING(30,"加载中"),
    /**
     * 同步candidate
     */
    RTC_CANDIDATE(107, "同步candidate");

    private final Integer code;

    private final String desc;


    public Integer code() {
        return this.code;
    }
}
