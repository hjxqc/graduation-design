package com.haungjain.im_platform.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.im_platform.contant.RedisKey;
import com.haungjain.im_platform.entity.Friend;
import com.haungjain.im_platform.entity.User;
import com.haungjain.im_platform.enums.ResultCode;
import com.haungjain.im_platform.exception.GlobalException;
import com.haungjain.im_platform.mapper.FriendMapper;
import com.haungjain.im_platform.mapper.UserMapper;
import com.haungjain.im_platform.service.IFriendService;
import com.haungjain.im_platform.session.SessionContext;
import com.haungjain.im_platform.session.UserSession;
import com.haungjain.im_platform.vo.FriendVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
@CacheConfig(cacheNames = RedisKey.IM_CACHE_FRIEND)
public class FriendServiceImpl extends ServiceImpl<FriendMapper,Friend> implements IFriendService{

    private final UserMapper userMapper;

    @Override
    public Boolean isFriend(String userId1, String userId2) {
        QueryWrapper<Friend> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Friend::getUserId,userId1)
                .eq(Friend::getFriendId,userId2);
        return this.count(queryWrapper) > 0;
    }

    /**
     * 找出所有的朋友，通过用户id
     * @param userId 用户id
     * @return
     */
    @Override
    public List<Friend> findFriendByUserId(String userId) {
        LambdaQueryWrapper<Friend> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(Friend::getUserId,userId);
        return this.list(queryWrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addFriend(String friendId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        if (friendId == session.getUserId()){
            throw new GlobalException(ResultCode.PROGRAM_ERROR,"不允许添加自己为好友");
        }
        //互相绑定好友
        FriendServiceImpl proxy = (FriendServiceImpl) AopContext.currentProxy();
        proxy.bindFriend(session.getUserId(),friendId);
        proxy.bindFriend(friendId,session.getUserId());
        log.info("添加好友，用户：{}，好友：{}",session.getUserId(),friendId);
    }

    /**
     * 单项绑定好友
     * @param userId
     * @param friendId
     */
    @CacheEvict(key = "#userId+':'+#friendId")
    public void bindFriend(String userId,String friendId){
        QueryWrapper<Friend> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Friend::getUserId,userId)
                .eq(Friend::getFriendId,friendId);
        //如果以前没有加上好久
        if(this.count(queryWrapper) == 0){
            Friend friend = new Friend();
            friend.setUserId(userId);
            friend.setFriendId(friendId);
            User friendInfo = userMapper.selectById(friendId);
            friend.setFriendNickName(friendInfo.getUserName());
            friend.setFriendHeadImage(friendInfo.getHeadImage());
            friend.setCreatedTime(new Date());
            this.save(friend);
        }
    }

    private final PrivateMessageUpdateService privateMessageUpdateService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delFriend(String friendId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        String userId = session.getUserId();
        //互相接触好友关系，走代理清理缓存
        FriendServiceImpl proxy = (FriendServiceImpl) AopContext.currentProxy();
        proxy.unbindFriend(userId,friendId);
        proxy.unbindFriend(friendId,userId);
        privateMessageUpdateService.delAllMessageByUserId(userId,friendId);
        privateMessageUpdateService.delAllMessageByUserId(friendId,userId);
        log.info("删除好友，用户id：{}，好友id:{}",userId,friendId);
    }

    @CacheEvict(key = "#userId+':'+#friendId")
    public void unbindFriend(String userId,String friendId){
        QueryWrapper<Friend> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Friend::getUserId,userId)
                .eq(Friend::getFriendId,friendId);
        List<Friend> friends = this.list(queryWrapper);
        //删除好友
        friends.forEach( friend -> {this.removeById(friend.getId());});
    }



    @Override
    public FriendVO findFriend(String friendId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        QueryWrapper<Friend> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Friend::getUserId,session.getUserId())
                .eq(Friend::getFriendId,friendId);
        Friend friend = this.getOne(queryWrapper);
        if (friend == null)
            throw new GlobalException(ResultCode.PROGRAM_ERROR,"对方不是你好友");
        FriendVO friendVO = new FriendVO();
        friendVO.setId(friend.getFriendId());
        friendVO.setHeadImage(friend.getFriendHeadImage());
        friendVO.setNickName(friend.getFriendNickName());
        return friendVO;
    }

    @Override
    public void update(FriendVO vo) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserSession session = SessionContext.getSession(authentication);
        QueryWrapper<Friend> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(Friend::getUserId, session.getUserId());

        List<Friend> fs = this.list(queryWrapper);

        //批量修改好友信息
        for (Friend f : fs) {
            f.setFriendHeadImage(vo.getHeadImage());
            f.setFriendNickName(vo.getNickName());
            this.updateById(f);
        }
    }
}
