package com.haungjain.im_platform.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UserVO {

    @NotNull(message = "用户id不能为空")
    private String id;

    @NotEmpty(message = "用户名不能为空")
    @Length(max = 64, message = "用户名不能大于64字符")
    private String userName;

    private Integer sex;

    private Integer type;

    @Length(max = 1024, message = "个性签名不能大于1024个字符")
    private String signature;

    private String headImage;


    private Boolean online;
}
