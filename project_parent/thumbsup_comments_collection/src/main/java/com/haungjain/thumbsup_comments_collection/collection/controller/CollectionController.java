package com.haungjain.thumbsup_comments_collection.collection.controller;

import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_interface.thumbsup_comments_collectionService.CollectionServiceInf;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.Collection;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.CollectionLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/collection")
public class CollectionController {

    @Autowired
    private CollectionServiceInf collectionServiceInf;

    @PostMapping("/post/collection/{userid}/{postid}")
    public BaseResponse collectionPost(@PathVariable("userid") String userid,@PathVariable("postid") String postid, CollectionLocation collectionLocation) throws BaseException {
        return collectionServiceInf.collectionPost(userid, postid, collectionLocation);
    }

    @PostMapping("/collection/remove/{userid}/{postid}")
    public BaseResponse cancelCollectionPost(@PathVariable("userid") String userid,@PathVariable("postid") String postid,CollectionLocation collectionLocation) throws BaseException {
        return collectionServiceInf.cancelCollectionPost(userid, postid, collectionLocation);
    }

    @GetMapping ("/user/collection/{userid}")
    public BaseListResponse<Collection> getUserAllCollection(@PathVariable("userid") String userid){
        return collectionServiceInf.getUserAllPostByUserId(userid);
    }
}
