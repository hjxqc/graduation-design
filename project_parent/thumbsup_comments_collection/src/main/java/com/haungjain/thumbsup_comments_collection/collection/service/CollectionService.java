package com.haungjain.thumbsup_comments_collection.collection.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.CommonService.ImgServiceInf;
import com.haungjain.project_interface.posts_data.ApprovedPostMapper;
import com.haungjain.project_interface.thumbsup_comments_collectionService.CollectionServiceInf;
import com.haungjain.project_interface.thumbsup_comments_collection_data.CollectionMapper;
import com.haungjain.project_interface.thumbsup_comments_collection_data.user_data.UserMapper;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.publish_manager_postImg.Img;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.Collection;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.CollectionLocation;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.CollectionStatus;
import com.haungjain.project_utils.ImgUtils.service.ImgService;
import com.haungjain.project_utils.uuid.uuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CollectionService extends ServiceImpl<CollectionMapper, Collection> implements CollectionServiceInf {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ApprovedPostMapper approvedPostMapper;

    @Autowired
    ImgServiceInf imgServiceInf;

    @Override
    public BaseListResponse<Collection> getUserAllPostByUserId(String userid){
        List<Collection> collections = baseMapper.seletctAllCollectByUserid(userid);
        List<Collection> res = collections.stream().map(item -> {
            ApprovedPost approvedPost = approvedPostMapper.selectById(item.getCollectionPostId());
            Img img = imgServiceInf.getById(approvedPost.getPostCover());
            approvedPost.setCover(img);
            item.setApprovedPost(approvedPost);
            UserInformation userInformation = userMapper.selectById(approvedPost.getUserId());
            approvedPost.setUserInformation(userInformation);
            return item;
        }).collect(Collectors.toList());
        return new BaseListResponse(CommonProperties.OK.getCode(),CommonProperties.OK.getMessage(),res);
    }

    @Override
    @Transactional
    public BaseResponse collectionPost(String userid, String postid, CollectionLocation collectionLocation) throws BaseException {
        try {
            UserInformation userInformation = userMapper.selectById(userid);
            ApprovedPost approvedPost = approvedPostMapper.selectById(postid);
            if(userInformation == null || approvedPost == null)
                return ResponseBuilder.fail(null);

            boolean collected = isCollected(userid, postid);
            if(collected)
                return ResponseBuilder.fail(null);

            Collection collection = new Collection();
            //设置主键
            collection.setCollectionPostId(uuidUtil.getUUid());
            collection.setCollectionUserId(userid);
            collection.setCollectionPostId(postid);
            //设置位置

            boolean falg = saveOrUpdate(collection);
            if(!falg)
                return ResponseBuilder.fail(null);
        }catch (Exception e){
            e.printStackTrace();
            throw new BaseException(CommonProperties.FAIL);
        }

        return ResponseBuilder.ok(null);
    }

    @Override
    @Transactional
    public BaseResponse cancelCollectionPost(String userid, String postid, CollectionLocation collectionLocation) throws BaseException {

        try {
            QueryWrapper<Collection> collectionQueryWrapper = new QueryWrapper<>();
            collectionQueryWrapper.eq("collection_post_id",postid);
            collectionQueryWrapper.eq("collection_user_id",userid);
            boolean flag = remove(collectionQueryWrapper);
            if(!flag)
                return ResponseBuilder.fail(null);
        }catch (Exception e){
            e.printStackTrace();
            throw new BaseException(CommonProperties.FAIL);
        }

        return ResponseBuilder.ok(null);
    }

    @Override
    public CollectionStatus postAllCollection(String postid, String userid){
        QueryWrapper<Collection> collectionQueryWrapper = new QueryWrapper<>();
        collectionQueryWrapper.eq("collection_post_id",postid);
        CollectionStatus collectionStatus = new CollectionStatus();
        collectionStatus.setCollectionNum(baseMapper.selectCount(collectionQueryWrapper));
        boolean collected = isCollected(userid, postid);
        if(!collected)
            collectionStatus.setCollectionColor("black");
        else
            collectionStatus.setCollectionColor("yellow");

        return collectionStatus;
    }

    public boolean isCollected(String userid,String postid){
        QueryWrapper<Collection> collectionQueryWrapper = new QueryWrapper<>();
        collectionQueryWrapper.eq("collection_user_id",userid);
        collectionQueryWrapper.eq("collection_post_id",postid);
        Integer integer = baseMapper.selectCount(collectionQueryWrapper);
        if(integer>0)
            return true;
        else
            return false;
    }
}
