package com.haungjain.thumbsup_comments_collection.thumbsup.controller;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.thumbsup_comments_collectionService.ThumbsupServiceInf;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/thumbsup")
public class ThumbsupController {

    @Autowired
    ThumbsupServiceInf thumbsupServiceInf;

    @PostMapping("/thumbsuppost/{userid}/{approvedpostsid}")
    public BaseResponse thumbsupPost(@PathVariable("userid") String userid, @PathVariable("approvedpostsid") String approvedPostsId){
        return thumbsupServiceInf.thumbsupPost(userid,approvedPostsId);
    }

    @PostMapping("/thumbsupcomment/{userid}/{commentid}")
    public BaseResponse thumbsupComment(@PathVariable("userid") String userid,@PathVariable("commentid") String commentId){
        return thumbsupServiceInf.thumbsupComment(userid,commentId);
    }

    @PostMapping("/thumbsupcomment/cancel/{userid}/{commentid}")
    public BaseResponse cancelThumbsupComment(@PathVariable("userid") String userid,@PathVariable("commentid") String commentId) throws BaseException {
        return thumbsupServiceInf.cancelThumbsupComment(userid,commentId);
    }

    @PostMapping("/thumbsuppost/cancel/{userid}/{approvedpostsid}")
    public BaseResponse cancelThumbsupPost(@PathVariable("userid") String userid ,@PathVariable("approvedpostsid") String approvedPostsId) throws BaseException {
        return thumbsupServiceInf.cancelThumbsupPost(userid,approvedPostsId);
    }

    @GetMapping("/thumbsup/judge/{originid}/{userid}")
    public BaseResponse judgeUserThumbsup(@PathVariable("originid") String originid ,@PathVariable("userid") String userid){
        return thumbsupServiceInf.judgeUserThumbsup(originid,userid);
    }

    @GetMapping("/all/thumbsup/{originid}")
    public BaseResponse getAllThumbsupByOriginId(@PathVariable("originid") String originid){
        return ResponseBuilder.ok(thumbsupServiceInf.AllThumThumbsup(originid));
    }

    @GetMapping("/user/all/thumbup/post/{userid}")
    public BaseListResponse<ApprovedPost> getAllThumupPostByUserId(@PathVariable("userid") String userid){
        return thumbsupServiceInf.getUserThumbupAllPost(userid);
    }
}
