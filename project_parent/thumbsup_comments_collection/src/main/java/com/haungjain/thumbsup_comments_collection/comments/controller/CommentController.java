package com.haungjain.thumbsup_comments_collection.comments.controller;

import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_interface.thumbsup_comments_collectionService.CommentsServiceInf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    CommentsServiceInf commentsServiceInf;

    @PostMapping("/post/comment/{userid}/{postid}")
    public BaseResponse commentPost(@PathVariable("userid") String userid ,@PathVariable("postid") String postid , String content) throws BaseException {
        return commentsServiceInf.commentPost(userid, postid, content);
    }

    @PostMapping("/comment/comment/{userid}/{commentid}")
    public BaseResponse commentComment(@PathVariable("userid") String userid ,@PathVariable("commentid") String commentid , String content) throws BaseException {
        return commentsServiceInf.commentComment(userid, commentid, content);
    }

    @PostMapping("/comment/remove/{userid}/{commentid}")
    public BaseResponse removeComment(@PathVariable("userid") String userid,@PathVariable("commentid") String commentid) throws BaseException {
        return commentsServiceInf.removeComment(userid, commentid);
    }
}
