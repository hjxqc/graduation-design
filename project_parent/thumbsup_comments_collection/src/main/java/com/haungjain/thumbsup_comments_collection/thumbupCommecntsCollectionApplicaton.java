package com.haungjain.thumbsup_comments_collection;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages = {"com.haungjain.project_utils.ImgUtils","com.haungjain.thumbsup_comments_collection"})
@MapperScan(basePackages = {"com.haungjain.project_interface.thumbsup_comments_collection_data","com.haungjain.project_interface.posts_data"})
@EnableTransactionManagement(proxyTargetClass = true)
public class thumbupCommecntsCollectionApplicaton {
    public static void main(String[] args) {
        SpringApplication.run(thumbupCommecntsCollectionApplicaton.class,args);
    }
}
