package com.haungjain.thumbsup_comments_collection.comments.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.posts_data.ApprovedPostMapper;
import com.haungjain.project_interface.thumbsup_comments_collectionService.CommentsServiceInf;
import com.haungjain.project_interface.thumbsup_comments_collectionService.ThumbsupServiceInf;
import com.haungjain.project_interface.thumbsup_comments_collection_data.CommentsMapper;
import com.haungjain.project_interface.thumbsup_comments_collection_data.user_data.UserMapper;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.CommentComment;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.CommentStatus;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.Comments;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.PostComment;
import com.haungjain.project_utils.uuid.uuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class CommentService extends ServiceImpl<CommentsMapper, Comments> implements CommentsServiceInf {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ApprovedPostMapper approvedPostMapper;

    @Autowired
    private ThumbsupServiceInf thumbsupServiceInf;

    @Override
    @Transactional
    public BaseResponse commentPost(String userid, String postid, String content) throws BaseException {

        UserInformation userInformation = userMapper.selectById(userid);
        ApprovedPost approvedPost = approvedPostMapper.selectById(postid);

        if(userInformation == null || approvedPost == null || content == null || content.equals(""))
            return ResponseBuilder.fail(null);

        Comments comments = new Comments();
        //设置主键
        comments.setCommentsId(uuidUtil.getUUid());
        //设置评论的对象
        comments.setCommentsTarget(postid);
        //设置评论时间
        comments.setCommentsDate(new Date());
        //设置内容
        comments.setCommentsContent(content);
        saveOrUpdate(comments);

        saveUserComment(userid,comments.getCommentsId());

        return ResponseBuilder.ok(null);
    }

    @Override
    @Transactional
    public BaseResponse commentComment(String userid, String commentid, String content) throws BaseException {
        UserInformation userInformation = userMapper.selectById(userid);
        Comments comments = getById(commentid);

        if(userInformation == null || comments == null || content == null || content.equals(""))
            return ResponseBuilder.fail(null);

        Comments commentsRes = new Comments();
        //设置主键
        commentsRes.setCommentsId(uuidUtil.getUUid());
        //设置评论的对象
        commentsRes.setCommentsTarget(commentid);
        //设置发布时间
        commentsRes.setCommentsDate(new Date());
        //设置评论的内容
        commentsRes.setCommentsContent(content);

        saveOrUpdate(commentsRes);

        saveUserComment(userid,commentsRes.getCommentsId());

        return ResponseBuilder.ok(null);
    }

    @Transactional
    public boolean saveUserComment(String userid,String commentid) throws BaseException {

        try{
            boolean flag = baseMapper.insertUserComments(userid, commentid);
            if (!flag)
                return false;
        }catch (Exception e){
            e.printStackTrace();
            throw new BaseException(CommonProperties.FAIL);
        }
        return true;
    }

    @Override
    @Transactional
    public BaseResponse removeComment(String userid, String commentid) throws BaseException {
        try {
            //删除子评论
            removeChildComment(userid,commentid);

            //删除评论
            removeById(commentid);
            baseMapper.deleteUserCommentByUseridAndCommentid(userid,commentid);
        }catch (Exception e){
            e.printStackTrace();
            throw new BaseException(CommonProperties.FAIL);
        }
        return ResponseBuilder.ok(null);
    }

    @Override
    public List<PostComment> postAllComment(String postid,String userid){

        //得到评论贴子的评论
        List<Comments> comments = baseMapper.selectCommentByParentCommnetId(postid);

        List<PostComment> postComments = new ArrayList<>();

        //再得到所有评论的评论
        //如果一个评论没有评论则是一个空的集合
        for (Comments comment : comments) {
            String commentsId = comment.getCommentsId();
            String CommentUserid = comment.getUserid();
            ArrayList<CommentComment> parentPostComments = new ArrayList<>();
            Integer commentAllThumsup = thumbsupServiceInf.commentAllThumsup(commentsId);
            boolean isNotThumbsup = thumbsupServiceInf.isNotThumbsup(userid, commentsId);
            UserInformation userInformation = userMapper.selectById(CommentUserid);
            getALLCommentComment(userid,userInformation.getName(),commentsId,parentPostComments);
            PostComment postComment = new PostComment();
            postComment.setComments(comment);
            postComment.setUserInformation(userInformation);
            CommentStatus commentStatus = new CommentStatus();
            //设置所有的点赞数
            commentStatus.setThumbsupNum(commentAllThumsup);

            if(!isNotThumbsup)
                commentStatus.setCommentColor("red");
            else
                commentStatus.setCommentColor("black");
            //设置评论的状态信息
            postComment.setCommentStatus(commentStatus);
            postComment.setCommentCommtent(parentPostComments);
            postComment.setAllThumbsup(commentAllThumsup);
            postComment.setCommtentNum(parentPostComments.size());
            postComments.add(postComment);
        }
        return postComments;
    }

    @Override
    public Integer postAllCommentNum(List<PostComment> postComments) {
       AtomicReference<Integer> res = new AtomicReference<>(0);

       postComments.forEach((item->{
           res.updateAndGet(v -> v + 1 + item.getCommtentNum());
       }));

        return res.get();
    }

    /**
     * 采用dfs算法计算出所有评论的评论
     *
     * @param parentid
     * @param parentComment
     */
    private void getALLCommentComment(String userid , String parentName , String parentid , List<CommentComment> parentComment){
        //查询父评论的所有子评论
        List<Comments> commentschlidren = baseMapper.selectCommentByParentCommnetId(parentid);

        //终止条件
        if(commentschlidren.size()==0)
            return;

        for (Comments comments : commentschlidren) {
            String commentsId = comments.getCommentsId();
            String commentUserid = comments.getUserid();
            //使用递归来找到所有这个评论的所有评论
            Integer commentAllThumsup = thumbsupServiceInf.commentAllThumsup(commentsId);
            boolean isNotThumbsup = thumbsupServiceInf.isNotThumbsup(userid, commentsId);
            UserInformation userInformation = userMapper.selectById(commentUserid);
            getALLCommentComment(userid,userInformation.getName(),commentsId,parentComment);
            CommentComment commentComment = new CommentComment();
            CommentStatus commentStatus = new CommentStatus();
            //设置所有的点赞数
            commentStatus.setThumbsupNum(commentAllThumsup);
            //设置点赞按钮的颜色
            if(!isNotThumbsup)
                commentStatus.setCommentColor("red");
            else
                commentStatus.setCommentColor("black");
            //设置评论的状态信息
            commentComment.setCommentStatus(commentStatus);
            //设置发布这条评论的用户信息
            commentComment.setUserInformation(userInformation);
            //设置发布这条评论的评论信息
            commentComment.setComments(comments);
            //设置这条评论回复的回复者的用户名
            commentComment.setToComment(parentName);
            //设置发布这条信息的用户名
            commentComment.setFromComment(userInformation.getName());
            parentComment.add(commentComment);
        }
    }

    //删除评论
    public void removeChildComment(String userid , String commentid){
        ArrayList<CommentComment> postComments = new ArrayList<>();
        UserInformation userInformation = userMapper.selectById(userid);
        getALLCommentComment(userid,userInformation.getName(),commentid,postComments);
        //删除子评论
        removeChildComment(userid , postComments);
        //删除父评论
        baseMapper.deleteById(commentid);
    }

    @Transactional
    public void removeChildComment(String userid , List<CommentComment> postComments){
        //终止条件
        if(postComments.size()==0)
            return;

        for (CommentComment comments : postComments) {
            //使用递归来找到所有这个评论的所有评论
            String commentsId = comments.getComments().getCommentsId();
            removeById(commentsId);
            baseMapper.deleteUserCommentByUseridAndCommentid(userid,commentsId);
        }
    }



}
