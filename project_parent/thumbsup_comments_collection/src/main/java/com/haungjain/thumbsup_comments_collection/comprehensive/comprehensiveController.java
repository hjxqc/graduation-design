package com.haungjain.thumbsup_comments_collection.comprehensive;

import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.thumbsup_comments_collectionService.CollectionServiceInf;
import com.haungjain.project_interface.thumbsup_comments_collectionService.CommentsServiceInf;
import com.haungjain.project_interface.thumbsup_comments_collectionService.ThumbsupServiceInf;
import com.haungjain.project_pojos.thumbsup_comments_collection.Comprehensive;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.PostComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/comprehensive")
    public class comprehensiveController {

    @Autowired
    private ThumbsupServiceInf thumbsupServiceInf;

    @Autowired
    private CommentsServiceInf commentsServiceInf;

    @Autowired
    private CollectionServiceInf collectionServiceInf;

    @GetMapping("/post/comprehensive/{postid}/{userid}")
    public Comprehensive comprehensivePost(@PathVariable("postid") String postid,@PathVariable("userid")String userid){
        Comprehensive comprehensive = new Comprehensive();
        comprehensive.setThumbsupStatus(thumbsupServiceInf.postAllThumbsup(postid,userid));
        comprehensive.setCollectionStatus(collectionServiceInf.postAllCollection(postid,userid));
        List<PostComment> postComments = commentsServiceInf.postAllComment(postid, userid);
        comprehensive.setPostComments(postComments);
        comprehensive.setCommentNum(commentsServiceInf.postAllCommentNum(postComments));
        return comprehensive;
    }
}
