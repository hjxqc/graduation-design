package com.haungjain.thumbsup_comments_collection.thumbsup.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.CommonService.ImgServiceInf;
import com.haungjain.project_interface.posts_data.ApprovedPostMapper;
import com.haungjain.project_interface.thumbsup_comments_collectionService.ThumbsupServiceInf;
import com.haungjain.project_interface.thumbsup_comments_collection_data.CommentsMapper;
import com.haungjain.project_interface.thumbsup_comments_collection_data.ThumbsupMapper;
import com.haungjain.project_interface.thumbsup_comments_collection_data.user_data.UserMapper;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.publish_manager_postImg.Img;
import com.haungjain.project_pojos.thumbsup_comments_collection.Thumbsup.ThumbsupStatus;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.Comments;
import com.haungjain.project_pojos.thumbsup_comments_collection.Thumbsup.ThumbsUp;
import com.haungjain.project_utils.uuid.uuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ThumbsupService extends ServiceImpl<ThumbsupMapper, ThumbsUp> implements ThumbsupServiceInf {

    @Autowired
    private ApprovedPostMapper approvedPostMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CommentsMapper commentsMapper;

    @Autowired
    private ImgServiceInf imgServiceInf;

    @Override
    public BaseListResponse<ApprovedPost> getUserThumbupAllPost(String userid){
        List<ApprovedPost> approvedPosts = baseMapper.selectAllThumupPostByUserId(userid);
        List<ApprovedPost> res = approvedPosts.stream().map(item -> {
            Img img = imgServiceInf.getById(item.getPostCover());
            item.setCover(img);
            UserInformation userInformation = userMapper.selectById(item.getUserId());
            item.setUserInformation(userInformation);
            return item;
        }).collect(Collectors.toList());
        return new BaseListResponse(CommonProperties.OK.getCode(),CommonProperties.OK.getMessage(),res);
    }

    @Override
    @Transactional
    public BaseResponse thumbsupPost(String userid , String approvedPostsId) {
        boolean falg = isNotThumbsup(userid, approvedPostsId);
        if(!falg)
            return ResponseBuilder.fail(null);
        ApprovedPost approvedPost = approvedPostMapper.selectById(approvedPostsId);
        UserInformation userInformation = userMapper.selectById(userid);
        //如果点赞了一个不存在的帖子
        if(approvedPost==null)
            return ResponseBuilder.fail(null);
        //如果用户不存在则
        if (userInformation==null)
            return ResponseBuilder.fail(null);

        ThumbsUp thumbsUp = new ThumbsUp();
        //设置主键
        thumbsUp.setThumbsUpId(uuidUtil.getUUid());
        //设置来源值
        thumbsUp.setThumbsUpOrigin(approvedPostsId);
        //设置点赞时间
        thumbsUp.setThumbsUpDate(new Date());
        //点赞车成功
        saveOrUpdate(thumbsUp);

        //加入用户点赞表中
        baseMapper.insertUserThumbsup(userid,thumbsUp.getThumbsUpId());

        return ResponseBuilder.ok(null);
    }

    @Override
    @Transactional
    public BaseResponse thumbsupComment(String userid , String commentId) {
        boolean falg = isNotThumbsup(userid, commentId);
        if(!falg)
            return ResponseBuilder.fail(null);
        Comments comments = commentsMapper.selectById(commentId);
        UserInformation userInformation = userMapper.selectById(userid);

        if(comments == null || userInformation == null)
            return ResponseBuilder.fail(null);

        ThumbsUp thumbsUp = new ThumbsUp();
        //设置主键
        thumbsUp.setThumbsUpId(uuidUtil.getUUid());
        //设置评论来源
        thumbsUp.setThumbsUpOrigin(commentId);
        //设置评论时间
        thumbsUp.setThumbsUpDate(new Date( ));
        saveOrUpdate(thumbsUp);

        //增加用户的点赞数
        baseMapper.insertUserThumbsup(userid,thumbsUp.getThumbsUpId());
        return ResponseBuilder.ok(null);
    }

    @Override
    public boolean isNotThumbsup(String userid, String origin){
        List<String> thumbsups = baseMapper.selectThumbsupByUseridAndOrigin(userid, origin);
        if(thumbsups.size()>0)
            return false;
        else
            return true;
    }

    /**
     * 删除这个用户对这个评论的点赞
     * @param userid
     * @param commentId
     * @return
     * @throws BaseException
     */
    @Override
    public BaseResponse cancelThumbsupComment(String userid, String commentId) throws BaseException {
        //查询这哦用户在评论点赞的id
        List<String> thumbsupIds = baseMapper.selectThumbsupByUseridAndOrigin(userid, commentId);

        //删除点赞
        for (String thumbsupId : thumbsupIds) {
            boolean flag = removeUserThumbsup(userid, thumbsupId);
            if (!flag)
                return ResponseBuilder.fail(null);
        }

        //删除成功
        return ResponseBuilder.ok(null);
    }

    /**
     * 删除这个用户对帖子的点赞
     * @param userid
     * @param approvedPostsId
     * @return
     * @throws BaseException
     */
    @Override
    public BaseResponse cancelThumbsupPost(String userid, String approvedPostsId) throws BaseException {
        //查询这哦用户在评论点赞的id
        List<String> thumbsupIds = baseMapper.selectThumbsupByUseridAndOrigin(userid, approvedPostsId);

        //删除点赞
        for (String thumbsupId : thumbsupIds) {
            boolean flag = removeUserThumbsup(userid, thumbsupId);
            if (!flag)
                return ResponseBuilder.fail(null);
        }

        //删除成功
        return ResponseBuilder.ok(null);
    }

    @Transactional
    public boolean removeUserThumbsup(String userid , String thumbsupId) throws BaseException {
        try {
            boolean falg = baseMapper.deleteUserThumbsup(userid,thumbsupId);
            if(!falg)
                return falg;

            //删除点赞的信息
            removeById(thumbsupId);
        }catch (Exception e){
            e.printStackTrace();
            throw new BaseException(CommonProperties.FAIL);
        }
        return true;
    }

    @Override
    public ThumbsupStatus postAllThumbsup(String postid, String userid){
        ThumbsupStatus thumbsupStatus = new ThumbsupStatus();
        thumbsupStatus.setThumbsupNum(AllThumThumbsup(postid));
        boolean notThumbsup = this.isNotThumbsup(userid, postid);
        if(notThumbsup)
            thumbsupStatus.setThumbsupColor("black");
        else
            thumbsupStatus.setThumbsupColor("red");
        return thumbsupStatus;
    }

    @Override
    public Integer commentAllThumsup(String commentid){
        return AllThumThumbsup(commentid);
    }

    @Override
    public Integer AllThumThumbsup(String originid){
        return baseMapper.selectAllThumsupNumByOriginid(originid);
    }

    @Override
    public BaseResponse judgeUserThumbsup(String originid, String userid){
        boolean isOnly = isNotThumbsup(userid, originid);
        if(!isOnly) {
            return ResponseBuilder.ok(isOnly);
        }else{
            return ResponseBuilder.fail(isOnly);
        }
    }
}
