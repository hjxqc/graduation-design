package com.haungjain.project_pojos.thumbsup_comments_collection.comment;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@TableName("comments")
public class Comments {
    @TableId(type = IdType.UUID,value = "commentsid")
    private String commentsId;

    @TableField(value = "comments_content")
    private String commentsContent;

    @TableField(value = "comments_target")
    private String commentsTarget;

    @TableField(value = "comments_date")
    private Date commentsDate;

    @TableField(exist = false)
    private String userid;

    @TableField(exist = false)
    private ApprovedPost approvedPost;
}
