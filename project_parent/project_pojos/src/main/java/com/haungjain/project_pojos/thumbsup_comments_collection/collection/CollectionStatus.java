package com.haungjain.project_pojos.thumbsup_comments_collection.collection;

import lombok.Data;

@Data
public class CollectionStatus {

    private String CollectionColor;

    private Integer CollectionNum;
}
