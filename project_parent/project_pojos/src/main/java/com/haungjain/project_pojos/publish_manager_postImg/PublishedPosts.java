package com.haungjain.project_pojos.publish_manager_postImg;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.Comments;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@ToString
@TableName(value = "published_posts")
public class PublishedPosts {
    @TableId(type = IdType.UUID,value = "postsid")
    private String postsId;

    @TableField(value = "post_name")
    private String postName;

    @TableField(value = "post_cover")
    private String postCover;

    @TableField(value = "post_content")
    private String postContent;

    @TableField(value = "post_status")
    private String postStatus;

    @TableField(value = "post_publish_date")
    private Date postPublishDate;

    @TableField(value = "post_update_date")
    private Date postUpdateDate;

    @TableField(value = "review_progress")
    private Integer reviewProgress;

    @TableField(value = "user_id")
    private String userId;

    @TableField(exist = false)
    private List<Img> postsImgContent;

    @TableField(exist = false)
    private Img coverImg;

    @TableField(exist = false)
    private List<Comments> postAllComments;

    @TableField(exist = false)
    private Long postCommentsNum;

    @TableField(exist = false)
    private Long thumbsUpNum;

    @TableField(exist = false)
    private Long collection;
}
