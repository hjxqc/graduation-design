package com.haungjain.project_pojos.publish_manager_postImg;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@TableName(value = "img")
public class Img {

    @TableId(type = IdType.UUID,value = "imgid")
    private String imgId;

    @TableField(value = "img_address")
    private String imgAddress;

    @TableField(value = "img_base_url")
    private String imgBaseUrl;

    @TableField(value = "img_up_date")
    private Date imgUpDate;
}
