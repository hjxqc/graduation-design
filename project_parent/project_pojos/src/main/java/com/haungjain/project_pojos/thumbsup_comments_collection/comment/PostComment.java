package com.haungjain.project_pojos.thumbsup_comments_collection.comment;

import com.haungjain.project_pojos.permission_authentication.UserInformation;
import lombok.Data;
import lombok.ToString;

import java.util.List;
@Data
@ToString
public class PostComment extends CommentParent{

    private Integer allThumbsup;

    private Integer commtentNum;

    private List<CommentComment> commentCommtent;
}
