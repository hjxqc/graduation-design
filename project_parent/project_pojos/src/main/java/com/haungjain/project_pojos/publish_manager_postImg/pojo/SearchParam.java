package com.haungjain.project_pojos.publish_manager_postImg.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class SearchParam {
    private String search_key;

    private String post_status;

    private Date start_time;

    private Date end_time;

    private Integer current;

    private Integer page_size;
}
