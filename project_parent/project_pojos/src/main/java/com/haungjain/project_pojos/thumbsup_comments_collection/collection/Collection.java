package com.haungjain.project_pojos.thumbsup_comments_collection.collection;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@TableName("collection")
public class Collection {

    @TableId(type = IdType.UUID,value = "collection_user_id")
    private String collectionUserId;

    @TableId(type = IdType.UUID,value = "collection_post_id")
    private String collectionPostId;

    @TableField(value = "collection_address")
    private String collection_address;

    @TableField(exist = false)
    private ApprovedPost approvedPost;
}
