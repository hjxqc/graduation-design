package com.haungjain.project_pojos.permission_authentication;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.haungjain.project_common.properties.chat_properties.enums.ChatCmdType;
import com.haungjain.project_pojos.publish_manager_postImg.UserUpImg;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.Collection;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.Comments;
import com.haungjain.project_pojos.thumbsup_comments_collection.Thumbsup.ThumbsUp;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@ToString
@TableName("user_information")
public class UserInformation {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.UUID,value = "user_id")
    private String userId;

    /**
     * 昵称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 性别
     */
    @TableField(value = "sex")
    private Integer sex;

    /**
     * 用户类型：1：普通用户，2：审核专用账户
     */
    @TableField(value = "type")
    private Integer type;
    /**
     * 个性签名
     */
    @TableField(value = "signature")
    private String signature;
    /**
     * 头像
     */
    @TableField("head_image")
    private String headImage;

    @TableField(value = "mobile")
    private String mobile;

    @TableField(value = "mail")
    private String mail;
    /**
     * 最后登录时间
     */
    @TableField("last_login_time")
    private Date lastLoginTime;

    /**
     * 创建时间
     */
    @TableField("created_time")
    private Date createdTime;

    @TableField(exist = false)
    private AccountInformation accountInformation;

    @TableField(exist = false)
    private List<UserUpImg> AllUserUpImg;

    @TableField(exist = false)
    private List<Comments> AllComments;

    @TableField(exist = false)
    private List<ThumbsUp> AllThumbsUp;

    @TableField(exist = false)
    private List<Collection> AllCollections;
}
