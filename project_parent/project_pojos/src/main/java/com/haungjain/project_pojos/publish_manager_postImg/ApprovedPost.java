package com.haungjain.project_pojos.publish_manager_postImg;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import com.haungjain.project_pojos.thumbsup_comments_collection.Comprehensive;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@ToString
@Data
@TableName(value = "approved_post")
public class ApprovedPost {
    @TableId(type = IdType.UUID,value = "post_id")
    private String postId;

    @TableField(value = "post_name")
    private String postName;

    @TableField(value = "post_cover")
    private String postCover;

    @TableField(value = "post_content")
    private String postContent;

    @TableField(value = "post_status")
    private String postStatus;

    @TableField(value = "post_publish_date")
    private Date postPublishDate;

    @TableField(value = "post_update_date")
    private Date postUpdateDate;

    @TableField(value = "user_id")
    private String userId;

    @TableField(exist = false)
    private List<Img> postsImgContent;

    @TableField(exist = false)
    private Img cover;
    /**
     * 这个类型里面包含了所有的点赞、收藏、评论
     */
    @TableField(exist = false)
    private Comprehensive comprehensive;

    @TableField(exist = false)
    private UserInformation userInformation;
}
