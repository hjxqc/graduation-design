package com.haungjain.project_pojos.thumbsup_comments_collection.comment;

import lombok.Data;

@Data
public class CommentComment extends CommentParent{
    String FromComment;

    String ToComment;
}
