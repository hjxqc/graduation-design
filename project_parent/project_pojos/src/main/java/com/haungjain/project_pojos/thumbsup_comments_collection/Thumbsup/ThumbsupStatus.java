package com.haungjain.project_pojos.thumbsup_comments_collection.Thumbsup;

import lombok.Data;

@Data
public class ThumbsupStatus {

    private String ThumbsupColor;

    private Integer ThumbsupNum;
}
