package com.haungjain.project_pojos.thumbsup_comments_collection;

import com.haungjain.project_pojos.thumbsup_comments_collection.Thumbsup.ThumbsupStatus;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.CollectionStatus;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.PostComment;
import lombok.Data;
import java.util.List;
@Data
public class Comprehensive {
    private ThumbsupStatus thumbsupStatus;

    private CollectionStatus collectionStatus;

    private Integer commentNum;

    private List<PostComment> postComments;
}
