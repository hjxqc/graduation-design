package com.haungjain.project_pojos.publish_manager_postImg.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostRequest {

    /**
     * 当前页数
     */
    private Integer current;

    /**
     * 每页记录数
     */
    private Integer size;
}
