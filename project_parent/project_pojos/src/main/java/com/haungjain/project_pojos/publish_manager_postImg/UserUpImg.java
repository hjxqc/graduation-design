package com.haungjain.project_pojos.publish_manager_postImg;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@TableName(value = "user_up_img")
public class UserUpImg {
    @TableId(type = IdType.UUID,value = "up_imgid")
    private String upImgid;

    @TableField(value = "up_date")
    private Date upDate;

    @TableField(value = "up_img_status")
    private String upImgStatus;

    @TableField(value = "imgid")
    private String imgId;

    @TableField(value = "userid")
    private String userid;

    @TableField(value = "up_address")
    private String upAddress;

    @TableField(exist = false)
    private Img img;

    @TableField(exist = false)
    private Location location;
}
