package com.haungjain.project_pojos.thumbsup_comments_collection.comment;

import com.haungjain.project_pojos.permission_authentication.UserInformation;
import lombok.Data;

@Data
public class CommentParent {
    protected Comments comments;

    protected UserInformation userInformation;

    protected CommentStatus commentStatus;
}
