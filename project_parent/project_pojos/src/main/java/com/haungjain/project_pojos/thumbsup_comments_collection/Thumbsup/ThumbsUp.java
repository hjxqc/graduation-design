package com.haungjain.project_pojos.thumbsup_comments_collection.Thumbsup;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@TableName("thumbs_up")
public class ThumbsUp {
    @TableId(type = IdType.UUID,value = "thumbs_up_id")
    private String thumbsUpId;

    @TableField(value = "thumbs_up_origin")
    private String thumbsUpOrigin;

    @TableField(value = "thumbs_up_date")
    private Date thumbsUpDate;

    @TableField(exist = false)
    private ApprovedPost approvedPost;
}
