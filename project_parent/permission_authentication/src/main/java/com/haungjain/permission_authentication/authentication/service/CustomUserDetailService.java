package com.haungjain.permission_authentication.authentication.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.haungjain.project_interface.permission_authentication_data.AccountInformationMapper;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CustomUserDetailService implements UserDetailsService {
    @Autowired
    private AccountInformationMapper accountInformationMapper;

    @Override
    public UserDetails loadUserByUsername(String principalJson) throws UsernameNotFoundException {
        QueryWrapper<AccountInformation> accountInformationQueryWrapper = new QueryWrapper<>();
        try{
            AccountInformation accountInfo = JSONObject.parseObject(principalJson, AccountInformation.class);
            //查询用户名信息
            accountInformationQueryWrapper.eq("user_name",accountInfo.getUserName());
        } catch (Exception e){
            log.warn(e.getMessage());
            accountInformationQueryWrapper.eq("user_name",principalJson);
        }

        AccountInformation accountInformation = accountInformationMapper.selectOne(accountInformationQueryWrapper);

        //1.根据请求用户名查询用户信息
        if(accountInformation == null){
            throw new UsernameNotFoundException("账号名密码错误");
        }
        return  new CustomUserDetail(accountInformation,AuthorityUtils.commaSeparatedStringToAuthorityList("ADMIN"));
    }
}
