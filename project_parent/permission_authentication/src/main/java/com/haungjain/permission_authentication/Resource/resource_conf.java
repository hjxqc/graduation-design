package com.haungjain.permission_authentication.Resource;

import com.haungjain.permission_authentication.authentication.sms_authentication.MobileCodeAuthenticationProvider;
import com.haungjain.permission_authentication.authentication.sms_authentication.MobileDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableResourceServer
public class resource_conf extends ResourceServerConfigurerAdapter {
    //配置当前资源服务器的id
    public static final String RESOURCE_ID="product-server";

    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(RESOURCE_ID)
                .tokenStore(tokenStore);
    }

    @Autowired
    MobileDetailService mobileDetailService;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/permission/mobile/code/{mobile}","/permission/user/register").permitAll().anyRequest().authenticated();

        MobileCodeAuthenticationProvider mobileCodeAuthenticationProvider = new MobileCodeAuthenticationProvider();

        mobileCodeAuthenticationProvider.setMobileDetailService(mobileDetailService);

        //将
        http.authenticationProvider(mobileCodeAuthenticationProvider);
    }
}
