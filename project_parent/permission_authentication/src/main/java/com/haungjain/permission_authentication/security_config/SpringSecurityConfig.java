package com.haungjain.permission_authentication.security_config;

import com.haungjain.permission_authentication.authentication.service.CustomUserDetailService;
import com.haungjain.permission_authentication.authentication.sms_authentication.MobileCodeAuthenticationProvider;
import com.haungjain.permission_authentication.authentication.sms_authentication.MobileDetailService;
import com.haungjain.project_interface.permission_authentication_data.AccountInformationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private CustomUserDetailService customUserDetailService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailService);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    MobileDetailService mobileDetailService;

    @Autowired
    private AccountInformationMapper accountInformationMapper;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .formLogin().disable();

        MobileCodeAuthenticationProvider mobileCodeAuthenticationProvider = new MobileCodeAuthenticationProvider();

        mobileCodeAuthenticationProvider.setMobileDetailService(mobileDetailService);

        mobileCodeAuthenticationProvider.setAccountInformationMapper(accountInformationMapper);

        //将
        http.authenticationProvider(mobileCodeAuthenticationProvider);
    }
}
