package com.haungjain.permission_authentication.Resource.mobile_inf;

import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.permission_authentication_data.AccountInformationMapper;
import com.haungjain.project_interface.permission_authentication_data.UserInformationMapper;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Service
public class UserRegisterService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountInformationMapper accountInformationMapper;

    @Autowired
    private UserInformationMapper userInformationMapper;

    @Transactional
    public BaseResponse register(Map<String,String> userInfo){
        String name = userInfo.get("name");
        String username = userInfo.get("username");
        Integer sex = Integer.parseInt(userInfo.get("sex"));
        String mobile = userInfo.get("mobile");
        String mail = userInfo.get("mail");
        String password = userInfo.get("password");

        if (name == null || username == null || sex == null || mobile == null || mail == null || password == null)
            return ResponseBuilder.fail(null);

        //获取用户名
        String userid = UUID.randomUUID().toString();
        AccountInformation accountInformation = new AccountInformation();
        UserInformation userInformation = new UserInformation();
        accountInformation.setUserId(userid);
        userInformation.setUserId(userid);
        accountInformation.setUserName(username);
        accountInformation.setMail(mail);
        accountInformation.setPassword(passwordEncoder.encode(password));
        accountInformation.setCredentialsNonExpired(true);
        accountInformation.setAccountNonLocked(true);
        accountInformation.setAccountNonExpired(true);
        accountInformation.setEnabled(true);

        userInformation.setName(name);
        userInformation.setSex(sex);
        userInformation.setMobile(mobile);
        userInformation.setMail(mail);
        userInformation.setType(1);
        userInformation.setCreatedTime(new Date());
        userInformation.setLastLoginTime(new Date());

        accountInformationMapper.insert(accountInformation);
        userInformationMapper.insert(userInformation);
        return ResponseBuilder.ok(null);
    }

    public UserInformation getUserInfo(String userid){
        return userInformationMapper.selectById(userid);
    }
}
