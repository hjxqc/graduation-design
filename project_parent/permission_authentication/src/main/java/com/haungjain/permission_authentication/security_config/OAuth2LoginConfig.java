package com.haungjain.permission_authentication.security_config;

import com.haungjain.permission_authentication.authentication.service.CustomUserDetailService;
import com.haungjain.permission_authentication.authentication.sms_authentication.MobileCodeTokenGranter;
import com.haungjain.permission_authentication.authentication.userTokenConverter.CustomUseTokenConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.ArrayList;
import java.util.Collections;

@Configuration
@EnableAuthorizationServer
public class OAuth2LoginConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private CustomUserDetailService customUserDetailService;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("permission_authentication")
                .secret(passwordEncoder.encode("permission_authentication"))
                //资源服务器id
                .resourceIds("product-server","chat-server")
                //授权类型
                .authorizedGrantTypes("password","refresh_token","mobile")
                //授权范围
                .scopes("all");
    }

    @Autowired
    private AuthenticationManager authenticationManager;



    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        //密码模式设置认证服务器
        endpoints.authenticationManager(authenticationManager);

        //刷新令牌获取令牌时需要
        endpoints.userDetailsService(customUserDetailService);

        //令牌管理策略
        endpoints.tokenStore(tokenStore)
                .accessTokenConverter(jwtAccessTokenConverter);//jwt令牌

        //获取所有默认的授权模式（授权码模式，密码模式，客户端模式，简化模式）等
        ArrayList<TokenGranter> tokenGranters = new ArrayList<>(Collections.singletonList(endpoints.getTokenGranter()));

        tokenGranters.add(new MobileCodeTokenGranter(authenticationManager,endpoints.getTokenServices(),
                endpoints.getClientDetailsService(),
                endpoints.getOAuth2RequestFactory()));



        CompositeTokenGranter compositeTokenGranter = new CompositeTokenGranter(tokenGranters);

        //配置短信验证
        endpoints
                .tokenGranter(compositeTokenGranter)
                .reuseRefreshTokens(true);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        // 所有人可访问 /oauth/token_key 后面要获取公钥, 默认拒绝访问
        security.tokenKeyAccess("permitAll()");
        // 认证后可访问 /oauth/check_token , 默认拒绝访问
        security.checkTokenAccess("permitAll()");
    }
}