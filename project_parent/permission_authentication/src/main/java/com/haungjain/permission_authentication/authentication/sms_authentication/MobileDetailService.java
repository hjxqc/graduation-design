package com.haungjain.permission_authentication.authentication.sms_authentication;


import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class MobileDetailService implements UserDetailsService {
    @Autowired
    RedisTemplate redisTemplate;

    @Override
    public UserDetails loadUserByUsername(String mobile) throws UsernameNotFoundException {
        String code = null;
        try{
            code = redisTemplate.opsForValue().get(mobile).toString();
            redisTemplate.delete(mobile);
        }catch (Exception e){
            e.printStackTrace();
            throw new UsernameNotFoundException("没有找到该验证码");
        }

        if (StringUtils.isEmpty(code) || code == null)
            throw new UsernameNotFoundException("没有找到该验证码");

        return new MobileDetail(mobile,code);
    }

    public UserDetails loadUserByMobileAndCode(String mobile,String code) throws UsernameNotFoundException {

        UserDetails userDetails = this.loadUserByUsername(mobile);

        String mobileCode = userDetails.getPassword();

        if(!mobileCode.equals(code)){
            throw new UsernameNotFoundException("验证码错误");
        }

        return userDetails;
    }
}
