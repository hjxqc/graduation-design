package com.haungjain.permission_authentication;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.haungjain.project_interface.permission_authentication_data"})
public class Permission_authentication {
    public static void main(String[] args) {
        SpringApplication.run(Permission_authentication.class,args);
    }
}
