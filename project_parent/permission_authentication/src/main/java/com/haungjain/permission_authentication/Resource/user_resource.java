package com.haungjain.permission_authentication.Resource;

import com.alibaba.fastjson2.JSONObject;
import com.haungjain.permission_authentication.Resource.mobile_inf.SendMobileCodeService;
import com.haungjain.permission_authentication.Resource.mobile_inf.UserRegisterService;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import org.apache.commons.lang.RandomStringUtils;
import org.codehaus.jackson.map.Serializers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/permission")
public class user_resource {

    @Autowired
    private SendMobileCodeService sendMobileCodeService;

    @Autowired
    private UserRegisterService userRegisterService;

    @RequestMapping("/user/info")
    public BaseResponse getUserInfo(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        AccountInformation accountInformation = JSONObject.parseObject((String) principal, AccountInformation.class);
        return ResponseBuilder.ok(userRegisterService.getUserInfo(accountInformation.getUserId()));
    }

    @GetMapping("/mobile/code/{mobile}")
    public BaseResponse sendMobileCode(@PathVariable("mobile") String mobile){

        //生成一个手机验证码
        String code = RandomStringUtils.randomNumeric(4);

        //将手机验证码发布到redis上
        sendMobileCodeService.sendCode(mobile,code);

        return ResponseBuilder.ok(code);
    }

    @PostMapping("/user/register")
    public BaseResponse registerUser(@RequestBody Map<String,String> userInfo){
        return userRegisterService.register(userInfo);
    }
}
