package com.haungjain.permission_authentication.authentication.userTokenConverter;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson2.JSONObject;
import com.haungjain.permission_authentication.authentication.service.CustomUserDetail;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class CustomUseTokenConverter implements UserAuthenticationConverter {
    private Collection<? extends GrantedAuthority> defaultAuthorities;
    private UserDetailsService userDetailsService;

    public CustomUseTokenConverter() {
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public void setDefaultAuthorities(String[] defaultAuthorities) {
        this.defaultAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils.arrayToCommaDelimitedString(defaultAuthorities));
    }

    public Map<String, ?> convertUserAuthentication(Authentication authentication) {

        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        Map<String, Object> response = new LinkedHashMap();
        response.put("user_name", authentication.getName());
        Object principal = authentication.getPrincipal();
        if (principal instanceof CustomUserDetail){
            CustomUserDetail customUserDetail = (CustomUserDetail) principal;
            response.put("user_id",customUserDetail.getUserId());
            response.put("mobile",customUserDetail.getMobile());
            response.put("mail",customUserDetail.getMail());
            response.put("accountNonExpired",customUserDetail.isAccountNonExpired());
            response.put("accountNonLocked",customUserDetail.isAccountNonLocked());
            response.put("credentialsNonExpired",customUserDetail.isCredentialsNonExpired());
            response.put("enabled",customUserDetail.isEnabled());
            if (customUserDetail.getTerminal() == null) {
                HttpServletRequest request = servletRequestAttributes.getRequest();
                Integer terminal = Integer.parseInt(request.getParameter("terminal"));
                response.put("terminal",terminal);
            }else{
                response.put("terminal",customUserDetail.getTerminal());
            }

        }

        if (authentication.getAuthorities() != null && !authentication.getAuthorities().isEmpty()) {
            response.put("authorities", AuthorityUtils.authorityListToSet(authentication.getAuthorities()));
        }

        return response;
    }

    public Authentication extractAuthentication(Map<String, ?> map) {
        if (map.containsKey("user_name")
                &&map.containsKey("user_id")
                &&map.containsKey("mobile")
                &&map.containsKey("mail")
                &&map.containsKey("accountNonExpired")
                &&map.containsKey("accountNonLocked")
                &&map.containsKey("credentialsNonExpired")
                &&map.containsKey("enabled")
                &&map.containsKey("terminal")
        ) {
            AccountInformation accountInformation = new AccountInformation();
            accountInformation.setUserId((String) map.get("user_id"));
            accountInformation.setUserName((String) map.get("user_name"));
            accountInformation.setMobile((String) map.get("mobile"));
            accountInformation.setMail((String) map.get("mail"));
            accountInformation.setAccountNonExpired((Boolean) map.get("accountNonExpired"));
            accountInformation.setAccountNonLocked((Boolean) map.get("accountNonLocked"));
            accountInformation.setCredentialsNonExpired((Boolean) map.get("credentialsNonExpired"));
            accountInformation.setEnabled((Boolean) map.get("enabled"));
            accountInformation.setTerminal((Integer) map.get("terminal"));
            Object principal = accountInformation;
            Collection<? extends GrantedAuthority> authorities = this.getAuthorities(map);
            if (this.userDetailsService != null) {
                UserDetails user = this.userDetailsService.loadUserByUsername((String)map.get("user_name"));
                authorities = user.getAuthorities();
                principal = user;
            }
            String principalString = JSONObject.toJSONString((AccountInformation)principal);
            return new UsernamePasswordAuthenticationToken(principalString, "N/A", authorities);
        } else {
            return null;
        }
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {
        if (!map.containsKey("authorities")) {
            return this.defaultAuthorities;
        } else {
            Object authorities = map.get("authorities");
            if (authorities instanceof String) {
                return AuthorityUtils.commaSeparatedStringToAuthorityList((String)authorities);
            } else if (authorities instanceof Collection) {
                return AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils.collectionToCommaDelimitedString((Collection)authorities));
            } else {
                throw new IllegalArgumentException("Authorities must be either a String or a Collection");
            }
        }
    }
}
