package com.haungjain.permission_authentication;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class mainController {

    @RequestMapping("/res")
    public String res(){
        return "这是资源";
    }

    @RequestMapping("/user/registered")
    public String registered(){
        return "registered";
    }
}
