package com.haungjain.permission_authentication.authentication.service;

import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class CustomUserDetail extends AccountInformation implements UserDetails {

    Collection<? extends GrantedAuthority> authorities;

    public CustomUserDetail(AccountInformation accountInformation , Collection<? extends GrantedAuthority> authorities) {
        super.setUserId(accountInformation.getUserId());
        super.setUserName(accountInformation.getUserName());
        super.setPassword(accountInformation.getPassword());
        super.setMail(accountInformation.getMail());
        super.setAccountNonExpired(accountInformation.isAccountNonExpired());
        super.setAccountNonLocked(accountInformation.isAccountNonLocked());
        super.setCredentialsNonExpired(accountInformation.isCredentialsNonExpired());
        super.setEnabled(accountInformation.isEnabled());
        super.setMobile(accountInformation.getMobile());
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUsername() {
        return super.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return super.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return super.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return super.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }
}
