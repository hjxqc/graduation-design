/*
 * Copyright 2004, 2005, 2006 Acegi Technology Pty Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.haungjain.permission_authentication.authentication.sms_authentication;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.haungjain.permission_authentication.authentication.service.CustomUserDetail;
import com.haungjain.project_interface.permission_authentication_data.AccountInformationMapper;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import lombok.Data;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.cache.NullUserCache;

import org.springframework.beans.factory.InitializingBean;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;

import org.springframework.util.Assert;

@Data
public class MobileCodeAuthenticationProvider implements
        AuthenticationProvider, MessageSourceAware {

    protected final Log logger = LogFactory.getLog(getClass());

    MobileDetailService mobileDetailService;

    private AccountInformationMapper accountInformationMapper;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.isInstanceOf(MobileCodeAuthenticationToken.class, authentication,
                () -> messages.getMessage(
                        "AbstractUserDetailsAuthenticationProvider.onlySupports",
                        "Only UsernamePasswordAuthenticationToken is supported"));

        //确定用户名
        String mobile = (authentication.getPrincipal() == null) ? "NONE_PROVIDED"
                : authentication.getName(); //如果是userDetail则会取userDetail的 手机号码


        // 这里的Credentials是先通过AbstractTokenGranter组装  new MobileCodeAuthenticationToken()传入的
        String code = (String) authentication.getCredentials();
        if (code == null) {
            throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "手机验证码不能为空"));
        }

        UserDetails userDetails = mobileDetailService.loadUserByMobileAndCode(mobile, code);

        //查询到了用户信息，则认证通过，就重新构建MobileToken
        QueryWrapper<AccountInformation> accountInformationQueryWrapper = new QueryWrapper<>();
        accountInformationQueryWrapper.eq("mobile",userDetails.getUsername());
         AccountInformation accountInformation = accountInformationMapper.selectOne(accountInformationQueryWrapper);
        CustomUserDetail customUserDetail = new CustomUserDetail(accountInformation, AuthorityUtils.commaSeparatedStringToAuthorityList("ADMIN"));
        MobileCodeAuthenticationToken mobileCodeAuthenticationToken = new MobileCodeAuthenticationToken(customUserDetail, null,customUserDetail.getAuthorities());
        mobileCodeAuthenticationToken.setDetails(authentication.getDetails());

        return mobileCodeAuthenticationToken;
    }

    protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();


    public void setMessageSource(MessageSource messageSource) {
        this.messages = new MessageSourceAccessor(messageSource);
    }

    public boolean supports(Class<?> authentication) {
        return (MobileCodeAuthenticationToken.class
                .isAssignableFrom(authentication));
    }


}
