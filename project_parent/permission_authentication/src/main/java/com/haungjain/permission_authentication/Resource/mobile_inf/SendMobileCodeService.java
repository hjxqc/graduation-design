package com.haungjain.permission_authentication.Resource.mobile_inf;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_common.response.ResponseBuilder;
import com.haungjain.project_interface.permission_authentication_data.AccountInformationMapper;
import com.haungjain.project_interface.permission_authentication_data.UserInformationMapper;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;
import com.haungjain.project_pojos.permission_authentication.UserInformation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SendMobileCodeService {

    @Autowired
    private RedisTemplate redisTemplate;

    public void sendCode(String mobile,String code){
        redisTemplate.opsForValue().set(mobile,code);
        log.info("发送短信成功："+code);
    }

    @Autowired
    private AccountInformationMapper accountInformationMapper;

    @Autowired
    private UserInformationMapper userInformationMapper;

    public BaseResponse getUserInfoByUserName(String UserName){
        //查询用户名信息
        QueryWrapper<AccountInformation> accountInformationQueryWrapper = new QueryWrapper<>();
        accountInformationQueryWrapper.eq("user_name", UserName);
        AccountInformation accountInformation = accountInformationMapper.selectOne(accountInformationQueryWrapper);

        if(accountInformation==null)
            return ResponseBuilder.fail(null);

        UserInformation userInformation = userInformationMapper.selectById(accountInformation.getUserId());
        return ResponseBuilder.ok(userInformation);
    }
}
