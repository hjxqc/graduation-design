package com.haungjain.im_client.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.haungjain.im_client.listener.MessageListenerMulticaster;
import com.haungjain.im_client.sender.IMSender;
import com.haungjain.im_common.contant.IMRedisKey;
import com.haungjain.im_common.enums.IMListenerType;
import com.haungjain.im_common.model.IMSendResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
public class PrivateMessageResultResultTask extends AbstractMessageResultTask{

    @Resource(name = "IMRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;

    @Value("${spring.application.name}")
    private String appName;

    @Value("${im.result.batch:100}")
    private int batchSize;

    private final MessageListenerMulticaster messageListenerMulticaster;

    @Override
    public void pullMessage() throws InterruptedException {
        List<IMSendResult> results;
        do{
            results = loadBatch();
            if (!results.isEmpty()){
                messageListenerMulticaster.multicaster(IMListenerType.PRIVATE_MESSAGE,results);
            }
        }while (results.size() >= batchSize); //如果result的大小大于或者扽与batch的大小，则表示还有消息剩余没有被拉去
    }

    /**
     * 批量拉取系统回推的消息
     * @return
     */
    List<IMSendResult> loadBatch() {
        String key = String.join(":", IMRedisKey.IM_RESULT_PRIVATE_QUEUE, appName);
        LinkedList<IMSendResult> results = new LinkedList<>();
        String jsonString = JSONObject.toJSONString(redisTemplate.opsForList().leftPop(key));
        JSONObject jsonObject = JSON.parseObject(jsonString);
        while (!Objects.isNull(jsonObject) && results.size() < batchSize){
            results.add(JSONObject.toJavaObject(jsonObject, IMSendResult.class));
            jsonString = JSONObject.toJSONString(redisTemplate.opsForList().leftPop(key));
            jsonObject = JSON.parseObject(jsonString);
        }
        return results;
    }
}
