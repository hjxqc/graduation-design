package com.haungjain.im_client.task;

import com.haungjain.im_common.util.ThreadPoolExecutorFactory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;

/**
 * 启动应用时调用run
 */
@Slf4j
public abstract class AbstractMessageResultTask implements CommandLineRunner {

    /**
     * 创建线程池
     */
    private static final ExecutorService EXECUTOR_SERVICE = ThreadPoolExecutorFactory.getThreadPoolExecutor();

    @Override
    public void run(String... args) throws Exception {
        EXECUTOR_SERVICE.execute(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                try {
                    pullMessage();
                } catch (InterruptedException e) {
                    log.error("任务调度异常",e);
                }
                if (!EXECUTOR_SERVICE.isShutdown()){
                    Thread.sleep(100);
                    EXECUTOR_SERVICE.execute(this);
                }
            }
        });
    }

    @PreDestroy
    public void destroy() {
        log.info("{}线程任务关闭",this.getClass().getSimpleName());
        //关闭线程池
        EXECUTOR_SERVICE.shutdown();
    }

    public abstract void pullMessage() throws InterruptedException;
}
