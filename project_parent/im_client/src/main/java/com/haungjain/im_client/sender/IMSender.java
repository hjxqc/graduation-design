package com.haungjain.im_client.sender;

import cn.hutool.core.collection.CollUtil;
import com.haungjain.im_client.listener.MessageListenerMulticaster;
import com.haungjain.im_common.contant.IMRedisKey;
import com.haungjain.im_common.enums.IMCmdType;
import com.haungjain.im_common.enums.IMListenerType;
import com.haungjain.im_common.enums.IMSendCode;
import com.haungjain.im_common.enums.IMTerminalType;
import com.haungjain.im_common.model.IMPrivateMessage;
import com.haungjain.im_common.model.IMRecvInfo;
import com.haungjain.im_common.model.IMSendResult;
import com.haungjain.im_common.model.IMUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 发送消息
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class IMSender {

    @Resource(name = "IMRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;

    @Value("${spring.application.name}")
    private String appName;

    private final MessageListenerMulticaster listenerMulticaster;

    public <T> void sendPrivateMessage(IMPrivateMessage<T> message) {
        LinkedList<IMSendResult> results = new LinkedList<>();
        for (Integer recvTerminal : message.getRecvTerminals()) {//默认每个总端都会发送消息
            //获取对方channelId
            String key = String.join(":", IMRedisKey.IM_USER_SERVER_ID, message.getRecvId().toString(), recvTerminal.toString());
            Integer serverId = (Integer) redisTemplate.opsForValue().get(key);
            //如果对方在线
            if (serverId != null) {
                String sendKey = String.join(":", IMRedisKey.IM_MESSAGE_PRIVATE_QUEUE, serverId.toString());
                log.info(sendKey);
                IMRecvInfo recvInfo = new IMRecvInfo();
                recvInfo.setCmd(IMCmdType.PRIVATE_MESSAGE.code());
                recvInfo.setSendResult(message.getSendResult());
                recvInfo.setServiceName(appName);
                //初始化发送者
                recvInfo.setSender(message.getSender());
                //初始化接收者
                recvInfo.setReceivers(Collections.singletonList(new IMUserInfo(message.getRecvId(), recvTerminal)));
                recvInfo.setData(message.getData());
                Long aLong = redisTemplate.opsForList().rightPush(sendKey, recvInfo);
                log.info("redis:装载消息：{}",aLong.toString());
            } else {
                IMSendResult<Object> result = new IMSendResult<>();
                result.setSender(message.getSender());
                result.setReceiver(new IMUserInfo(message.getRecvId(), recvTerminal));
                result.setCode(IMSendCode.NOT_ONLINE.code());
                result.setData(message.getData());
                //将要回推的消息添加到results的数组中
                results.add(result);
            }
        }

        //推送给其他的终端
        if (message.getSendToSelf()) {
            for (Integer terminal : IMTerminalType.codes()) {
                //如果发送的消息的终端则不回推消息
                if (message.getSender().getTerminal().equals(terminal)) {
                    continue;
                }
                //获取发送者自己的终端id终端的ChannelId
                String key = String.join(":", IMRedisKey.IM_USER_SERVER_ID, message.getSender().getId(), terminal.toString());
                Integer serverId = (Integer) redisTemplate.opsForValue().get(key);
                //如果终端在线，将数据存储至redis，等待拉取
                if (serverId != null) {
                    String sendKey = String.join(":", IMRedisKey.IM_RESULT_PRIVATE_QUEUE, serverId.toString());
                    IMRecvInfo recvInfo = new IMRecvInfo();
                    //自己的消息不需要回推消息结果
                    recvInfo.setSendResult(false);
                    recvInfo.setCmd(IMCmdType.PRIVATE_MESSAGE.code());
                    recvInfo.setSender(message.getSender());
                    recvInfo.setReceivers(Collections.singletonList(new IMUserInfo(message.getSender().getId(), terminal)));
                    recvInfo.setData(message.getData());
                    redisTemplate.opsForList().rightPush(sendKey, recvInfo);
                }
            }
        }

        //对离线用户回复消息状态
        if(message.getSendResult() && !results.isEmpty()){
            listenerMulticaster.multicaster(IMListenerType.PRIVATE_MESSAGE,results);
        }
    }

    public Map<String, List<IMTerminalType>> getOnlineTerminal(List<String> userIds){
        if (CollUtil.isEmpty(userIds)){
            return Collections.emptyMap();
        }
        //将所有用户的kye都存起来
        HashMap<String, IMUserInfo> userMap = new HashMap<>();
        for (String userId : userIds) {
            for (Integer terminal : IMTerminalType.codes()) {
                String key = String.join(":", IMRedisKey.IM_USER_SERVER_ID, userId, terminal.toString());
                userMap.put(key,new IMUserInfo(userId,terminal));
            }
        }
        //批量拉取
        List<Object> serverIds = redisTemplate.opsForValue().multiGet(userMap.keySet());
        int idx = 0;
        //用户所在线的所有终端
        HashMap<String, List<IMTerminalType>> onlineMap = new HashMap<>();
        for (Map.Entry<String, IMUserInfo> entry : userMap.entrySet()) {
            if (serverIds.get(idx++) != null){
                IMUserInfo userInfo = entry.getValue();
                List<IMTerminalType> imTerminalTypes = onlineMap.computeIfAbsent(userInfo.getId(), o -> new LinkedList<>());
                imTerminalTypes.add(IMTerminalType.fromCode(userInfo.getTerminal()));
            }
        }

        //去重并返回
        return onlineMap;
    }

    public Boolean isOnline(String userId){
        //使用keys进行模糊匹配
        String key = String.join(":", IMRedisKey.IM_USER_SERVER_ID, userId, "*");
        //如果只要有一个终端在线就算在线
        return !Objects.requireNonNull(redisTemplate.keys(key)).isEmpty();
    }

    //得到所有给定的用户所在线的用户id
    public List<String> getOnlineUser(List<String> userIds){
        return new LinkedList<>(getOnlineTerminal(userIds).keySet());
    }
}
