package com.haungjain.im_client.listener;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSONObject;
import com.haungjain.im_client.annotation.IMListener;
import com.haungjain.im_common.enums.IMListenerType;
import com.haungjain.im_common.model.IMSendResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

@Component
public class MessageListenerMulticaster {

    @Autowired
    private List<MessageListener> messageListeners =  Collections.emptyList();

    public void multicaster(IMListenerType listenerType, List<IMSendResult> results){
        if (CollUtil.isEmpty(results)){
            return;
        }
        for (MessageListener messageListener : messageListeners) {
            IMListener annotation = messageListener.getClass().getAnnotation(IMListener.class);
            if (annotation!=null && (annotation.type().equals(IMListenerType.ALL)||annotation.type().equals(listenerType))){
                results.forEach(result->{
                    //将data转向对象类型
                    if (result.getData() instanceof JSONObject){
                        //得到实现的第一个接口
                        Type superClass = messageListener.getClass().getGenericInterfaces()[0];
                        Type type = ((ParameterizedType) superClass).getActualTypeArguments()[0];
                        JSONObject data = (JSONObject) result.getData();
                        result.setData(JSONObject.toJavaObject(data,(Class)type));
                    }
                });
                //回调到处理方处理
                messageListener.process(results);
            }
        }
    }
}
