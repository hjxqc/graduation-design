package com.haungjain.im_client.listener;

import com.haungjain.im_common.model.IMSendInfo;
import com.haungjain.im_common.model.IMSendResult;

import java.util.List;

public interface MessageListener<T> {
    void process(List<IMSendResult<T>> result);
}
