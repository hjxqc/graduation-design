package com.haungjain.im_client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@ComponentScan("com.haungjain.im_client")
public class IMAutoConfiguration {
}
