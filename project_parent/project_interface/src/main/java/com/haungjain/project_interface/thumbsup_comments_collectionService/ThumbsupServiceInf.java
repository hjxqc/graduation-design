package com.haungjain.project_interface.thumbsup_comments_collectionService;

import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.thumbsup_comments_collection.Thumbsup.ThumbsupStatus;

public interface ThumbsupServiceInf {

    BaseListResponse<ApprovedPost> getUserThumbupAllPost(String userid);

    public BaseResponse thumbsupPost(String userid, String approvedPostsId);

    public BaseResponse thumbsupComment(String userid ,String commentId);

    boolean isNotThumbsup(String userid, String origin);

    public BaseResponse cancelThumbsupComment(String userid , String commentId) throws BaseException;

    public BaseResponse cancelThumbsupPost(String userid , String approvedPostsId) throws BaseException;

    public ThumbsupStatus postAllThumbsup(String postid, String userid);

    public Integer commentAllThumsup(String commentid);

    Integer AllThumThumbsup(String originid);

    BaseResponse judgeUserThumbsup(String originid, String userid);
}
