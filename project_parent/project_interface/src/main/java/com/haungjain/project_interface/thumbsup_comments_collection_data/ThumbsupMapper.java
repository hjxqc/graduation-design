package com.haungjain.project_interface.thumbsup_comments_collection_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.thumbsup_comments_collection.Thumbsup.ThumbsUp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ThumbsupMapper extends BaseMapper<ThumbsUp> {
    public boolean insertUserThumbsup(@Param("userid") String userid,@Param("thumbsupid") String thumbsUpId);

    public boolean deleteUserThumbsup(@Param("userid") String userid,@Param("thumbsupid") String thumbsUpId);

    public List<String> selectThumbsupByUseridAndOrigin(@Param("userid") String userid , @Param("originid") String originid);

    public Integer selectAllThumsupNumByOriginid(@Param("originid") String originid);

    public Integer selectUserThumbsup(@Param("originid") String originid,@Param("userid") String userid);

    public List<ApprovedPost> selectAllThumupPostByUserId(@Param("userid") String userid);
}
