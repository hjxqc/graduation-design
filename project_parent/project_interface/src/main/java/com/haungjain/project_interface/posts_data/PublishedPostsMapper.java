package com.haungjain.project_interface.posts_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.publish_manager_postImg.PublishedPosts;

public interface PublishedPostsMapper extends BaseMapper<PublishedPosts> {
}
