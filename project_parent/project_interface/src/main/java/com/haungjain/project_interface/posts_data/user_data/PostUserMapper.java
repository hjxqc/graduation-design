package com.haungjain.project_interface.posts_data.user_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.permission_authentication.UserInformation;

public interface PostUserMapper extends BaseMapper<UserInformation> {
}
