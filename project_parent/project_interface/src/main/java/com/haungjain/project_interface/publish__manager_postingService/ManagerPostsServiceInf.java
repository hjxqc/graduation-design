package com.haungjain.project_interface.publish__manager_postingService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import com.haungjain.project_pojos.publish_manager_postImg.request.PostRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ManagerPostsServiceInf extends IService<ApprovedPost> {
    public BaseListResponse<ApprovedPost> getAllApprovedPost(String userId , PostRequest postRequest) throws BaseException;

    BaseListResponse<ApprovedPost> getAllApprovedPost(List<String> userId, PostRequest postRequest) throws BaseException;

    BaseListResponse<ApprovedPost> searchAllApprovedPostByNanme(String post_name, PostRequest postRequest) throws BaseException;

    public BaseResponse UpdatePosts(String approvedPostid, ApprovedPost approvedPost, MultipartFile[] multipartFiles);

    public BaseResponse RemovePosts(String approvedPostId) throws BaseException;

    public BaseResponse<ApprovedPost> getApprovedPost(String userid,String postid) throws BaseException;
}
