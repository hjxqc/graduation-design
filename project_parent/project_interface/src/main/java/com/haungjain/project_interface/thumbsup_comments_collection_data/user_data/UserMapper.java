package com.haungjain.project_interface.thumbsup_comments_collection_data.user_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.permission_authentication.UserInformation;

public interface UserMapper extends BaseMapper<UserInformation> {
}
