package com.haungjain.project_interface.thumbsup_comments_collection_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.Collection;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectionMapper extends BaseMapper<Collection> {
    List<Collection> seletctAllCollectByUserid(@Param("userid") String userid);
}
