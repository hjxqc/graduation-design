package com.haungjain.project_interface.thumbsup_comments_collectionService;

import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.PostComment;

import java.util.List;

public interface CommentsServiceInf {

    public BaseResponse commentPost(String userid , String postid , String content) throws BaseException;

    public BaseResponse commentComment(String userid , String postid , String content) throws BaseException;

    public BaseResponse removeComment(String userid,String commentid) throws BaseException;

    List<PostComment> postAllComment(String postid,String userid);

    Integer postAllCommentNum(List<PostComment> postComments);
}
