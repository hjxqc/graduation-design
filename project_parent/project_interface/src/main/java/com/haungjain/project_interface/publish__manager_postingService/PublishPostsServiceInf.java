package com.haungjain.project_interface.publish__manager_postingService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BasePageListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.publish_manager_postImg.PublishedPosts;
import com.haungjain.project_pojos.publish_manager_postImg.pojo.SearchParam;
import org.springframework.web.multipart.MultipartFile;

public interface PublishPostsServiceInf extends IService<PublishedPosts> {
    public BaseResponse publishPosts(PublishedPosts publishedPosts , MultipartFile[] multipartFilter,MultipartFile cover) throws BaseException;

    BasePageListResponse<PublishedPosts> getAllPublishPost(SearchParam searchParam);

    BaseResponse<PublishedPosts> getPublishPostById(String postId);
}
