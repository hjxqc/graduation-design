package com.haungjain.project_interface.thumbsup_comments_collection_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.thumbsup_comments_collection.comment.Comments;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentsMapper extends BaseMapper<Comments> {
    public boolean insertUserComments(@Param("userid") String userid ,@Param("commentsid") String commentsid);

    public List<String> selectcommentsAllParentComment(@Param("commentsid") String commentsid);

    public boolean deleteUserCommentByUseridAndCommentid(@Param("userid") String userid ,@Param("commentsid") String commentsid);

    public List<Comments> selectCommentByParentCommnetId(@Param("parent_comment_id") String parent_comment_id);
}
