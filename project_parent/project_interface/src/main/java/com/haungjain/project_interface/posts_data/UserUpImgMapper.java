package com.haungjain.project_interface.posts_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.publish_manager_postImg.UserUpImg;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserUpImgMapper extends BaseMapper<UserUpImg> {
    /**
     * 删除用户上传的数据库信息
     * @param userid
     * @param imgids
     * @return
     */
    public boolean removeUserUpImg(@Param("userid") String userid,@Param("imgids") String[] imgids);

    /**
     * 获得所有的imgId通过upImgIds
     * @param upImgIds
     * @return
     */
    public List<String> getAllImgIdByUpImgIds(@Param("upImgIds") String[] upImgIds);
}
