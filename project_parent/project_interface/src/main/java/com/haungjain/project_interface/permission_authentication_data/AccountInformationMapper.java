package com.haungjain.project_interface.permission_authentication_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.permission_authentication.AccountInformation;

public interface AccountInformationMapper extends BaseMapper<AccountInformation> {
}
