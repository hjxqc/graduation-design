package com.haungjain.project_interface.posts_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haungjain.project_pojos.publish_manager_postImg.Img;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ImgMapper extends BaseMapper<Img> {
    public boolean insert_post_img(@Param("postid") String postid,@Param("imgid") String imgid);

    /**
     * 得到这个帖子的所有图片信息
     * @param postsid
     * @return
     */
    public List<Img> getAllApprovedPostImg(@Param("postsid") String postsid);

    /**
     * 删除与这个帖子关联的所有图片
     * @param postsid
     * @return
     */
    public boolean DeleteApprovedPostsImg(@Param("postsid") String postsid);
}
