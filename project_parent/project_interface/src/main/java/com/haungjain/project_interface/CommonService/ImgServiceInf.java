package com.haungjain.project_interface.CommonService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_pojos.publish_manager_postImg.Img;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImgServiceInf extends IService<Img> {

    public boolean SaveImage(String PostsId, MultipartFile[] multipartFiles) throws BaseException;

    public List<Img> getAllApprovedPostImg(String postId);

    public boolean SaveOrUpdateApprovedPostImg(String approvedId,MultipartFile[] multipartFiles);

    public boolean RemoveImg(String approvedPostsid) throws Exception ;

    public List<Img> SaveImage(MultipartFile[] multipartFiles) throws BaseException;

    public boolean RemoveImg(List<String> imgIds);
}
