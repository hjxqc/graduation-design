package com.haungjain.project_interface.publish__manager_postingService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.publish_manager_postImg.Location;
import com.haungjain.project_pojos.publish_manager_postImg.UserUpImg;
import org.springframework.web.multipart.MultipartFile;


public interface UserUpImgServiceInf extends IService<UserUpImg> {

    public BaseResponse upImg(String userId , MultipartFile[] multipartFiles , Location location , String upImgStatus) throws BaseException;

    public BaseResponse RemoveUpImg(String userid ,String[] imgIds,Location location);

    public BaseResponse UpdateUpImg(String userid,String upImgId , String upImgStatus , Location location);

    public BaseListResponse<UserUpImg> getAllUserUp(String userid);
}
