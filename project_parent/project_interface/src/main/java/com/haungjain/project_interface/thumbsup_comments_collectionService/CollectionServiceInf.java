package com.haungjain.project_interface.thumbsup_comments_collectionService;

import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.CollectionLocation;
import com.haungjain.project_pojos.thumbsup_comments_collection.collection.CollectionStatus;

public interface CollectionServiceInf {
    BaseListResponse getUserAllPostByUserId(String userid);

    public BaseResponse collectionPost(String userid, String postid, CollectionLocation collectionLocation) throws BaseException;

    public BaseResponse cancelCollectionPost(String userid,String postid,CollectionLocation collectionLocation) throws BaseException;

    CollectionStatus postAllCollection(String postid,String userid);
}
