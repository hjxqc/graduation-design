package com.haungjain.project_interface.posts_data;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haungjain.project_common.response.BaseListResponse;
import com.haungjain.project_common.response.BaseResponse;
import com.haungjain.project_pojos.publish_manager_postImg.ApprovedPost;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ApprovedPostMapper extends BaseMapper<ApprovedPost> {

    public Page<ApprovedPost> getAllApprovedPost( IPage<ApprovedPost> approvedPostIPage,@Param("userid") String userId);

    public Page<ApprovedPost> getAllApprovedPostByIds(IPage<ApprovedPost> approvedPostIPage,@Param("userids") List<String> userId);

    public Page<ApprovedPost> getAllApprovedPostByName(IPage<ApprovedPost> approvedPostIPage, @Param("post_name") String postName);
}