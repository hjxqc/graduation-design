package com.haungjain.im_common.util;

import cn.hutool.db.Session;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.haungjain.im_common.model.IMSessionInfo;

import java.util.Date;

public final class JwtUtil {

    private JwtUtil() {
    }

    /**
     * 根据token获取userId
     *
     * @param token 登录token
     * @return 用户id
     */
    public static String getUserId(String token) {
        try {
            String userId = JWT.decode(token).getClaims().get("user_id").asString();
            return userId;
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 根据token获取用户数据IMSessionInfo
     *
     * @param token 用户登录token
     * @return 用户数据
     */
    public static String getInfo(String token) {
        try {
            String userId = JWT.decode(token).getClaim("user_id").asString();
            Integer terminal = JWT.decode(token).getClaim("terminal").asInt();
            IMSessionInfo imSessionInfo = new IMSessionInfo();
            imSessionInfo.setUserId(userId);
            imSessionInfo.setTerminal(terminal);
            String jsonString = JSONObject.toJSONString(imSessionInfo);
            return jsonString;
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 校验token
     *
     * @param token  用户登录token
     * @param secret 秘钥
     * @return true/false
     */
    public static Boolean checkSign(String token, String secret) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(token);
            return true;
        } catch (JWTVerificationException e) {
            return false;
        }
    }
}
