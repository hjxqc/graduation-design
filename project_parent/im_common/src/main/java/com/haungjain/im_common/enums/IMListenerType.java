package com.haungjain.im_common.enums;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum IMListenerType {
    /**
     * 全部消息
     */
    ALL(0, "全部消息"),
    /**
     * 私聊消息
     */
    PRIVATE_MESSAGE(1, "私聊消息");

    private final Integer code;

    private final String desc;

    public Integer code() {
        return this.code;
    }
}
