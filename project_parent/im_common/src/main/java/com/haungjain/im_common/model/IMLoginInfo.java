package com.haungjain.im_common.model;

import lombok.Data;

@Data
public class IMLoginInfo {
    private String accessToken;
}
