package com.haungjain.im_common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IMUserInfo {
    /**
     *用户id
     */
    private String id;

    /**
     * 用户终端类型
     */
    private Integer terminal;

}
