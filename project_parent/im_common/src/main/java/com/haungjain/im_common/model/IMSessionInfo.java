package com.haungjain.im_common.model;

import lombok.Data;

@Data
public class IMSessionInfo {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 终端类型
     */
    private Integer terminal;
}
