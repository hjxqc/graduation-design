package com.haungjain.im_common.model;

import lombok.Data;

@Data
public class IMSendInfo<T> {
    /**
     * 命令
     */
    private Integer cmd;

    /**
     * 推送消息
     */
    private T data;
}
