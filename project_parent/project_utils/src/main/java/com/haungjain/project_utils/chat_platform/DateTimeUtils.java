package com.haungjain.project_utils.chat_platform;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期处理工具类
 *
 */
public final class DateTimeUtils extends DateUtils {

    private DateTimeUtils () {}

    public static final String FULL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String PARTDATEFORMAT = "yyyyMMdd";

    /**
     * 将日期类型转换为字符串
     * @param date 日期
     * @param xFormat 格式
     * @return
     */
    public static String getFormatDate(Date date, String xFormat){
        date = date == null? new Date() : date;
        xFormat = StringUtils.isNotEmpty(xFormat) ? xFormat :FULL_DATE_FORMAT;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(xFormat);
        return simpleDateFormat.format(date);
    }
}
