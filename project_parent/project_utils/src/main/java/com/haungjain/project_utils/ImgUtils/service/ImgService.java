package com.haungjain.project_utils.ImgUtils.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.haungjain.project_interface.CommonService.ImgServiceInf;
import com.haungjain.project_interface.posts_data.ImgMapper;
import com.haungjain.project_pojos.publish_manager_postImg.Img;
import com.haungjain.project_utils.ImgUtils.ImgServerConf;
import com.haungjain.project_utils.ImgUtils.ImgUtil;
import com.haungjain.project_utils.uuid.uuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ImgService extends ServiceImpl<ImgMapper, Img> implements ImgServiceInf {

    @Autowired
    ImgUtil imgUtil;

    @Autowired
    ImgServerConf imgServerConf;

    @Override
    @Transactional
    public boolean SaveImage(String PostsId, MultipartFile[] multipartFiles) {
        try {
            List<String> strings = imgUtil.pushImg(multipartFiles);

            for (String address : strings) {
                //将图片的信息保存到数据库中
                Img img = new Img();
                img.setImgId(uuidUtil.getUUid());
                img.setImgBaseUrl(imgServerConf.getBaseUrl());
                img.setImgAddress(address);
                img.setImgUpDate(new Date());
                saveOrUpdate(img);

                //将帖子的和图片进行关联起来
                baseMapper.insert_post_img(PostsId,img.getImgId());
            }
        } catch (BaseException e) {
            e.printStackTrace();
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public List<Img> getAllApprovedPostImg(String postId) {
        return baseMapper.getAllApprovedPostImg(postId);
    }

    //添加事物管理
    @Transactional
    @Override
    public boolean SaveOrUpdateApprovedPostImg(String approvedId,MultipartFile[] multipartFiles){
        List<Img> allApprovedPostImg = this.getAllApprovedPostImg(approvedId);
        /**
         * 如果有上传新的图片
         * 删除原先和帖子相关联的所有图片
         */
        if(allApprovedPostImg.size() > 0){
            for (int i=0 ; i<allApprovedPostImg.size() ; i++) {
                Img img = allApprovedPostImg.get(i);
                boolean flag = imgUtil.RemoveImg(img.getImgAddress());
                //如果没有删除成功
                if(!flag) {
                    return false;
                }
                //删除数据库中的图片记录
                boolean removeById = removeById(img.getImgId());
                if(!removeById)
                    return false;
            }
        }
        //删除帖子的图片内容
        baseMapper.DeleteApprovedPostsImg(approvedId);

        /**
         * 再和新的图片建立关系
         */

        boolean saved = SaveImage(approvedId, multipartFiles);
        if(!saved)
            return false;

        return true;
    }

    @Transactional
    @Override
    public boolean RemoveImg(String approvedPostsid) throws Exception {
        //查找到所有的图片信息
        List<Img> allApprovedPostImg = getAllApprovedPostImg(approvedPostsid);
        //删除图片的文件
        for (Img img : allApprovedPostImg) {
            boolean flag = imgUtil.RemoveImg(img.getImgAddress());
            //如果有一张图片不能删除则直接结束，知道用户下一次执行删除操作
            if (!flag)
                return false;
        }
        //删除数据库中的图片信息
        for (Img img : allApprovedPostImg) {
            boolean flag = removeById(img.getImgId());
            if(!flag)
                return false;
        }
        //删除和帖子的关联信息
        baseMapper.DeleteApprovedPostsImg(approvedPostsid);
        //删除成功
        return true;
    }

    /**
     * 上传所有的图片，并返回数据库中的所有图片信息
     * @param multipartFiles
     * @return
     */
    @Transactional
    @Override
    public List<Img> SaveImage(MultipartFile[] multipartFiles) throws BaseException {
        ArrayList<Img> imgs = new ArrayList<>();
        try {
            //将图片保存到服务器中
            List<String> addressPaths = imgUtil.pushImg(multipartFiles);

            //将图片信息初始化保存到数据中
            for (String addressPath : addressPaths) {
                Img img = new Img();
                img.setImgUpDate(new Date());
                img.setImgAddress(addressPath);
                img.setImgBaseUrl(imgServerConf.getBaseUrl());
                img.setImgId(uuidUtil.getUUid());
                imgs.add(img);
                saveOrUpdate(img);
            }

            return imgs;
        } catch (BaseException e) {
           e.printStackTrace();
           throw new BaseException(CommonProperties.FAIL);
        }
    }

    /**
     * 删除删除数据库中的图片信息，删除图片的存储
     * @param imgids
     * @return
     */
    @Transactional
    @Override
    public boolean RemoveImg(List<String> imgids){
        if(imgids.size() == 0)
            return false;
        //删除图片的存储
        for (String imgid : imgids) {
            Img img = getById(imgid);
            //从服务其中删除图片
            Boolean flag = imgUtil.RemoveImg(img.getImgAddress());
            //如果有一个图片删除失败则会直接短路
            if(!flag)
                return false;
        }

        //删除数据库中的数据
        for (String imgid : imgids) {
            boolean flag = removeById(imgid);
            if(!flag)
                return false;
        }

        return true;
    }
}
