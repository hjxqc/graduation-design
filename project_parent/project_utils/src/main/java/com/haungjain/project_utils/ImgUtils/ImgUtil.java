package com.haungjain.project_utils.ImgUtils;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.haungjain.project_common.exception.BaseException;
import com.haungjain.project_common.properties.exception.CommonProperties;
import com.sun.prism.Image;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component("imgUtil")
@EnableConfigurationProperties(ImgServerConf.class)
public class ImgUtil {
    private Log log = LogFactory.getLog(Image.class);

    @Autowired
    private FastFileStorageClient storageClient;

    @Autowired
    private ImgServerConf imgServerConf;

    /**
     * 图片上传
     */
    public List<String> pushImg(MultipartFile[] multipartFile) throws BaseException {

        List<String> res = new ArrayList<>();

        //校检文件上传是否为空
        for (MultipartFile file : multipartFile) {
            if(file.isEmpty()){
                log.error("有文件不存在");
                throw new BaseException(CommonProperties.FAIL);
            }
        }

        //校检文件内容
        try {
            for (MultipartFile file : multipartFile) {
                BufferedImage read = ImageIO.read(file.getInputStream());
                if(read == null || read.getWidth() == 0 || read.getHeight() == 0){
                    log.info("文件有问题");
                    throw new BaseException(CommonProperties.FAIL);
                }
            }
        } catch (IOException e) {
            log.error("校检文件内容失败...{}",e);
            e.printStackTrace();
            throw new BaseException(CommonProperties.FAIL);
        }

        for (MultipartFile file : multipartFile) {
            //上传文件
            try {
                //获取扩展名
                String extension = StringUtils.substringAfterLast(file.getOriginalFilename(), ".");
                if(!imgServerConf.getAllowType().contains(extension)){
                    log.info("文件类型不支持!");
                    throw new BaseException(CommonProperties.FAIL);
                }
                StorePath storePath = storageClient.uploadFile(file.getInputStream(), file.getSize(), extension, null);
                //返回路径
                res.add(storePath.getFullPath());

            } catch (IOException e) {
                e.printStackTrace();
                log.error("上传文件失败",e);
                throw new BaseException(CommonProperties.FAIL);
            }catch (Exception e){
                //如果其中有一个没有成功上传就删除全部已经上传成功的
                if(res.size()!=0){
                    for (String path : res) {
                        storageClient.deleteFile(path);
                    }
                }
                e.printStackTrace();
                throw new BaseException(CommonProperties.FAIL);
            }
        }

        return res;
    }

    /**
     *
     * @param ImgPath
     * @return
     */
    public Boolean RemoveImg(String ImgPath){
        try {
            storageClient.deleteFile(ImgPath);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
