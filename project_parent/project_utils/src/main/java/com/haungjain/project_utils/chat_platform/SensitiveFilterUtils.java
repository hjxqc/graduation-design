package com.haungjain.project_utils.chat_platform;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@Component
@NoArgsConstructor
@Slf4j
public class SensitiveFilterUtils {

    /**
     * 替换符
     */
    private static final String REPLACE_MENT = "***";

    /**
     * 根节点
     */
    private static final TireNode ROOT_NODE = new TireNode();
    private static class TireNode{
        //关键词结束标识
        private boolean isKeywordEnd = false;

        //子节点（key是下级字符，value是下级节点）
        //当前节点的子节点
        private final Map<Character , TireNode> subNodes = new HashMap<>();

        public boolean isKeywordEnd() {
            return isKeywordEnd;
        }

        public void setKeywordEnd(boolean keywordEnd) {
            isKeywordEnd = keywordEnd;
        }

        //添加子节点
        public void addSubNode(Character c, TireNode node){subNodes.put(c,node);}

        //获取子节点
        public TireNode getSubNode(Character c){return subNodes.get(c);}
    }

    /**
     * 初始化方法，服务启动时初始化
     */
    @PostConstruct
    public void init(){
        try(
                //类加载器
                InputStream is = this.getClass().getClassLoader().getResourceAsStream("sensitive-words.txt");
                BufferedReader reader = new BufferedReader(new InputStreamReader(is))
        ) {
            String keyword;
            while ((keyword = reader.readLine()) != null){
                log.info("脏词{}",keyword);
                //添加到前缀数中
                this.addKeyword(keyword);
            }
        }catch (Exception e){
            log.error("加载敏感词文件失败：" + e.getMessage());
        }
    }

    /**
     * 将一个敏感词加到前缀数树中
     * @param keyword
     */
    private void addKeyword(String keyword){
        TireNode tempNode = ROOT_NODE;
        for (int i=0 ; i<keyword.length() ;i++){
            char  c = keyword.charAt(i);
            TireNode subNode = tempNode.getSubNode(c);
            if (subNode == null){
                //初始化子节点
                subNode = new TireNode();
                tempNode.addSubNode(c , subNode);
            }
            tempNode = subNode;//指向子节点进入下一轮循环
            //设置结束标识
            if(i == keyword.length() -1){
                tempNode.setKeywordEnd(true);
            }
        }
    }

    /**
     * 过滤敏感词
     */
    public String filter(String text){
        if (StringUtils.isBlank(text)){
            return null;
        }

        //结果
        StringBuilder sb = new StringBuilder();
        try {
            //指针1
            TireNode tempNode = ROOT_NODE;
            //指针2
            int begin = 0;
            //指针3
            int position = 0;
            while (begin < text.length()){
                if (position < text.length()){
                    char c = text.charAt(position);
                    //跳过符号
                    if (isSymbol(c)){
                        //若指针1处于根节点，将此符号计入结果，让指针2向下走一步
                        if (tempNode == ROOT_NODE){
                            sb.append(c);
                            //因为以特殊符号开头肯定不是脏字
                            begin++;
                        }
                        //无论符号在开头或中间，指针3都向下走一步
                        position++;
                        continue;
                    }
                    //检查下级节点
                    tempNode = tempNode.getSubNode(c);
                    if(tempNode == null){
                        //以begin开头的字符串不是敏感词
                        sb.append(text.charAt(begin));
                        //进入下一个位置
                        position = ++begin;
                        //重新指向根节点
                        tempNode = ROOT_NODE;
                    } else if (tempNode.isKeywordEnd()) {
                        //发现敏感词，将begin~position字符串替换掉
                        sb.append(REPLACE_MENT);
                        //今入下一个位置
                        begin = ++position;
                    } else{
                        //检查下一个字符
                        position++;
                    }
                    //以begin开头position遍历越界仍未匹配到敏感词
                } else {
                    sb.append(text.charAt(begin));
                    position = ++begin;
                    tempNode = ROOT_NODE;
                }
            }
        }catch (Exception e){
            sb = new StringBuilder(text);
        }

        return sb.toString();
    }

    /**
     * 判断是否为符号：-特殊符号
     * @param c
     * @return
     */
    private boolean isSymbol(Character c){
        // 0x2E80~0x9FFF 是东亚文字范围
        return !CharUtils.isAsciiAlphanumeric(c) && (c < 0x2E80 || c > 0x9FFF);
    }
}
