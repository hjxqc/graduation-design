package com.haungjain.project_utils.chat;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 创建单例多线程
 */
@Slf4j
public class ThreadPoolExecutorFactory {

    /**
     * 机器的核心数：Runtime.getRuntime().availableProcessors()
     * corePoolSize: 池中所保存的线程数，包括空空闲线程
     * cpu 密集型：核心线程数：CPU核数 + 1
     * IO 密集型： 核心线程数：cpu核数数 * 2;
     */
    private static final int CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors() * 2;

    /**
     * maxmunPoolSize -池中允许的最大线程数（采用LinkBlockQueue时没有作用）
     */
    private static final int MAX_IMUM_POOL_SIZE = 100;
    /**
     * keepAliveTime - 当线程数大于核心数的时候，此为终止前的空余线程等待新任务的最长时间，线程池维护所允许的空闲时间
     */
    private static final int KEEP_ALIVE_TIME = 1000;
    /**
     * 等待队列的大小，默认是无界的，性能损耗的关键
     */
    private static final int QUEUE_SIZE = 200;
    /**
     * 线程池对象
     */
    private static  volatile ThreadPoolExecutor threadPoolExecutor = null;

    /**
     * 构造方法私有化
     */
    private ThreadPoolExecutorFactory(){
        if (null == threadPoolExecutor){
            threadPoolExecutor = ThreadPoolExecutorFactory.getThreadPoolExecutor();
        }
    }

    /**
     * 双检锁创建线程安全的单例
     * @return
     */
    public static ThreadPoolExecutor getThreadPoolExecutor(){
        //双重锁，需要加volatile，来避免内存重排的优化
        if (null == threadPoolExecutor){
            synchronized (ThreadPoolExecutorFactory.class){
                if (null == threadPoolExecutor){
                    threadPoolExecutor = new ThreadPoolExecutor(
                            //核心线程数
                            CORE_POOL_SIZE,
                            //最大线程数，包含临时线程
                            MAX_IMUM_POOL_SIZE,
                            //临时线程的存活时间
                            KEEP_ALIVE_TIME,
                            //时间单位（毫秒）
                            TimeUnit.MILLISECONDS,
                            //等待队列
                            new LinkedBlockingQueue<>(QUEUE_SIZE),
                            //拒绝策略
                            new ThreadPoolExecutor.CallerRunsPolicy()
                    );
                }
            }
        }

        return threadPoolExecutor;
    }

    /**
     * 重写readReslove方法
     * 若目标类有readResolve方法，那就通过反射的方式调用要被反序列化的类中的readResolve方法，
     * 返回一个对象，然后把这个新的对象复制给之前创建的obj（即最终返回的对象）。
     */
    private Object readResolve(){
        //重写readResolve方法，防止序列化破坏单例
        return ThreadPoolExecutorFactory.getThreadPoolExecutor();
    }

    /**
     * 关闭线程池
     */
    public void shutDown(){
        if (threadPoolExecutor != null){
            threadPoolExecutor.shutdown();
        }
    }

    public void execute(Runnable runnable){
        if (runnable == null){
            return;
        }
        threadPoolExecutor.execute(runnable);
    }
}
