package com.haungjain.project_utils.ImgUtils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "upload")
public class ImgServerConf {

    /**
     * 上传到的主机的ip地址
     */
    private String baseUrl;

    /**
     * 允许的类型
     */
    private List<String> allowType;

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setAllowType(List<String> allowType) {
        this.allowType = allowType;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public List<String> getAllowType() {
        return allowType;
    }
}
