package com.haungjain.project_utils.chat_platform;

import org.springframework.util.ReflectionUtils;

public class BeanUtils {

    private BeanUtils() {}

    //用来处理反射的异常
    private static void handleReflectionException(Exception e){
        ReflectionUtils.handleReflectionException(e);
    }

    /**
     * 属性拷贝
     * @param orig 目标对象
     * @param destClass 源对象
     * @return
     * @param <T>
     */
    public static <T> T copyProperties(Object orig,Class<T> destClass){
        try{
            T target = destClass.newInstance();
            if (orig == null){
                return null;
            }
            return target;
        } catch (Exception e){
            handleReflectionException(e);
            return null;
        }
    }

    public static void copyProperties(Object orig,Object dest){
        try {
            org.springframework.beans.BeanUtils.copyProperties(orig,dest);
        }catch (Exception e){
            handleReflectionException(e);
        }
    }
}
