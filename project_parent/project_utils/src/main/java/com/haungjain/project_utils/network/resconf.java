package com.haungjain.project_utils.network;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class resconf {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
